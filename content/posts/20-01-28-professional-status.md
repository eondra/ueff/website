---
author: "UEFF"
title: "Professional status rules for players"
date: 2020-01-28T18:00:06+01:00
draft: false
---

Due to varying life expectancy between races, the UEFF decided that the time span in which a player can play professionally should be limited.

<!--more-->

The professional leagues in every nation are soon to be started.
Complaints about certain unregulated points got louder and louder.
Therefore the UEFF took action and decided how long a player is able to play professionally.

- Players are required to absolve at least 1 year in a youth club and 3 years at most
- Players are only allowed to play 15 years as a professional. Beginning from the year they joined a club outside the youth system
- If a player was a professional before, the years after still count towards the 15 years.
- All years before the start of the first official league don't count.

This way the UEFF tries its best to make a fair ground for all races.
