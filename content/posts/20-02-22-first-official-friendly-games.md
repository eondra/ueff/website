---
author: "UEFF"
title: "First official friendly games"
date: 2020-02-22T16:00:00+01:00
tags: ["UEFF Mirror"]
draft: false
---

We are excited to report on the first official UEFF friendly games.

Audio: [ogg](/files/podcasts/ueff_mirror_ep1.ogg) [mp3](/files/podcasts/ueff_mirror_ep1.mp3)
Video: [LBRY](https://lbry.tv/@UEFF:7/ueff_mirror_ep1) [PeerTube](https://vid.lelux.fi/videos/watch/f3766e47-14c8-4b71-bbb9-80294dc0c79f)

<!--more-->


- 1. FC Gislavik - FC Kopa [1:2](/matchreports/20lc/20-02-22-1fcgislavik-fckopa-friendly)
- 1. SC Tinkerbury - BB Chiselhaven [2:2](/matchreports/20lc/20-02-22-1sctinkerbury-bbchiselhaven-friendly)
- Dim Garom Diggers - Kinbadur Rocks [2:0](/matchreports/20lc/20-02-22-dimgaromdiggers-kinbadurrocks-friendly)
- FT Kruzaz - Qvadgad Wolves [1:0](/matchreports/20lc/20-02-22-ftkruzaz-qvadgadwolves-friendly)
- MA Theveluma - Nfelin United [0:0](/matchreports/20lc/20-02-22-matheveluma-Nfelinunited)
