---
author: "UEFF"
title: "Eight clubs join the UEFF"
date: 2019-12-01T13:00:06+01:00
draft: false
---

Today, eight new clubs join the UEFF and their representative national organizations.

<!--more-->

We are proud to announce that the following teams join us and take up all free
spaces that were left.

| | |
|-|-|
| ![Jamta](/images/flags/jamta_small_md.png) | [FC Esjuberg](/teams/fcesjuberg) |
| ![Katargo](/images/flags/katargo_small_md.png) | [Cogdale United](/teams/cogdaleunited) |
| ![Katargo](/images/flags/katargo_small_md.png) | [FA Tinkerbury](/teams/fatinkerbury) |
| ![Urotha](/images/flags/urotha_small_md.png) | [FT Kruzaz](/teams/ftkruzaz) |
| ![Vongram](/images/flags/vongram_small_md.png) | [FC Vin Daral](/teams/fcvindaral) |
| ![Vongram](/images/flags/vongram_small_md.png) | [Vin Daral Fighters](/teams/vindaralfighters) |
| ![Yllin](/images/flags/yllin_small_md.png) | [FS Illa Ennore](/teams/fsillaennore) |
| ![Yllin](/images/flags/yllin_small_md.png) | [FC Iri Serin](/teams/fciriserin) |
