---
author: "UEFF"
title: "United Eondra Footorb Federation becomes reality"
date: 2017-04-02T13:00:06+01:00
draft: false
---

Today, 02.04.17 LC, the UEFF got officially founded.
After the meeting of the five national Footorb organizations in Theveluma,
the UEFF brings them together under one organization.

<!--more-->

The discussions were big over the last few months, but are finally done.
We combine the forces of the 5 national Footorb organizations and will
bring a clear structure to the upcoming professional status of Footorb.

Our headquarters will be in Theveluma, Yllin.

There are still topics to be cleared up about Eondra-wide tournaments,
even a joint league structure is on the table.

The next planned meeting will be on 21.05.17 LC.
