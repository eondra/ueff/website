---
author: "UEFF"
title: "Rules, Rules, Rules"
date: 2017-05-22T13:00:06+01:00
draft: false
---

The UEFF Board set the rules for all upcoming leagues, tournaments and teams that want to join.

<!--more-->

On 21.05.17 LC, the UEFF Board decided on the future of the structure of professional Footorb.
The overview of the rules can be read [here](/ueff/rules).