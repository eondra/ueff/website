---
author: "UEFF"
title: "Kuskyn Zinjeon joins the UEFF board"
date: 2020-01-25T16:00:06+01:00
draft: false
---

The UEFF board gets bigger. Kuskyn Zinjeon joins as official proofreader.

<!--more-->

Zinjeon has an overwhelming experience as a proofreader for local newsscrolls around Yllin.
He joins the UEFF by stepping away from his position as the proofreader for "Theveluma Daily"
