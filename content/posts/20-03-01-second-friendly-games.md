---
author: "UEFF"
title: "Second round of friendly games"
date: 2020-03-01T16:00:00+01:00
tags: ["UEFF Mirror"]
draft: false
---

The second round of friendly games played across Eondra.

Audio: [ogg](/files/podcasts/ueff_mirror_ep2.ogg) [mp3](/files/podcasts/ueff_mirror_ep2.mp3)
Video: [LBRY](https://lbry.tv/@UEFF:7/ueff_mirror_ep2) [PeerTube](https://vid.lelux.fi/videos/watch/d6be7793-93c6-47a8-a11a-efa377375288)

<!--more-->


- [BA Kyonore](/teams/bakyonore) - [Iri Serin Alchemists](/teams/iriserinalchemists) [1:0](/matchreports/20lc/20-03-01-bakyonore-iriserinalchemists-friendly)
- [FC Esjuberg](/teams/fcesjuberg) - [SC Aurioa](/teams/scaurioa) [1:0](/matchreports/20lc/20-03-01-fcesjuberg-scaurioa-friendly)
- [Gearingfort Inc.](/teams/gearingfortinc) - [Cogdale Scraps](/teams/cogdalescraps) [1:1](/matchreports/20lc/20-03-01-gearingfortinc-cogdalescraps-friendly)
- [ST Redkug](/teams/stredkug) - [FT Vaz Ilkadh](/teams/ftvazilkadh) [0:2](/matchreports/20lc/20-03-01-stredkug-ftvazilkadh-friendly)
- [Vin Daral Diamonds](/teams/vindaraldiamonds) - [SC Dim Garom](/teams/scdimgarom) [0:0](/matchreports/20lc/20-03-01-vindaraldiamonds-scdimgarom-friendly)
