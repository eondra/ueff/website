---
author: "UEFF"
title: "About Development"
date: 2020-01-25T12:00:06+01:00
draft: false
---
## The Team

| ![amolith](/images/team/amolith.png) | ![shellowl](/images/team/shellowl.png) | ![anoa](/images/team/anoa.png) | ![alea](/images/team/alea.png) |
|:-:|:-:|:-:|:-:|
| ![Urotha](/images/flags/urotha_small_md.png) | ![Jamta](/images/flags/jamta_small_md.png) | ![Yllin](/images/flags/yllin_small_md.png) | ![Yllin](/images/flags/yllin_small_md.png) |
| Krolzik Bramok | Snow Mikisson | Kuskyn Zinjeon | Alea Glynlee |
| (amolith) | (shellowl) | (anoa) | (kalaara) |
| UEFF General Director | UEFF Director | UEFF Proofreader | UEFF Writer |
| Vaz Ilkadh | Aurioa | Kyonore | Nfelin |


### World builders, Artists, Coders, Writers, Audio/Video editors, whoever wants to contribute
We are looking for people that enjoy our content and want to build the world of Eondra and the UEFF story.
In the future you might be even able to manage a team or bring yourself in as a player.

If you are interested:

- Join our Matrix Channel with Riot in your [browser](https://riot.im/app/#/room/#ueff:matrix.org) or take a look at other [clients](https://matrix.to/#/#ueff:matrix.org)
- We also have an IRC channel on freenode bridged to our Matrix channel. #ueff
- E-Mail us: ueff@nixnet.email

For more security, please use GPG. Our public key: [.asc](/files/gpg/ueff_public.asc)
