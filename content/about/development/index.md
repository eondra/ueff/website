---
author: "UEFF"
title: "About Development"
date: 2020-01-25T12:00:06+01:00
draft: false
---
## The stuff we use to make Eondra/UEFF a thing

We believe in Open Source, so we only use Open Source licensed projects to build this world.

### [ySoccer](http://ysoccer.sourceforge.net/) (GPL-2.0)
Without these guys, UEFF wouldn't even be possible.
We use ySoccer for our recordings and to provide UEFF - Footorb as a game later on.

### [Hugo](https://github.com/gohugoio/hugo) (Apache-2.0)
Our website is made with Hugo.

### [Hugo-Theme Zzo](https://github.com/zzossig/hugo-theme-zzo) (MIT)
We use the [Zzo-theme](https://github.com/zzossig/hugo-theme-zzo) for Hugo to make the website look good.

### [OBS](https://github.com/obsproject/obs-studio) (GPL-2.0)
We record our videos with OBS.

### [kdenlive](https://github.com/KDE/kdenlive) (GPL-2.0)
Of course we need to edit and render our videos. kdenlive does the trick for us.

### [GIMP](https://github.com/GNOME/gimp) & [Krita](https://github.com/KDE/krita)
Our graphics are made with GIMP and Krita respectively.
