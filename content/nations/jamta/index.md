---
author: "UEFF"
title: "Jamta Footorb Association (JFO)"
date: 2017-04-02T12:00:06+09:00
description: "Infopage about Jamta"
draft: false
type: "showcase"
---
![Jamta Flag](/images/flags/jamta_medium_md.png)

## Teams

| | | |
|-|-|-|
| [1. FC Gislavik](/teams/1fcgislavik) | [Aurioa SC](/teams/aurioasc) | [Vikur Fjall](/teams/vikurfjall) |
| [FC Odeila](/teams/fcodeila) | [FC Kopa](/teams/fckopa) | [Gnupar Unit](/teams/gnuparunit) |
| [FS Gislavik](/teams/fsgislavik) | [Strig Haell](/teams/strighaell) | [FS Dofrar](/teams/fsdofrar) |
| [FC Esjuberg](/teams/fcesjuberg) | [Barosvik Hammers](/teams/barosvikhammers) | [Vikur Gautland](/teams/vikurgautland) |
