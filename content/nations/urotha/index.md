---
author: "UEFF"
title: "Urotha Footorb Tribe (UFT)"
date: 2017-04-02T12:00:06+09:00
description: "Infopage about Urotha"
draft: false
type: "showcase"
---
![Urotha Flag](/images/flags/urotha_medium_md.png)

## Teams

| | | |
|-|-|-|
| [FT Kruzaz](/teams/ftkruzaz) | [FC Kruzaz](/teams/fckruzaz) | [Kruzaz Spears](/teams/kruzazspears) |
| [SC Redkug](/teams/scredkug) | [ST Redkug](/teams/stredkug) | [Warcry Redkug](/teams/warcryredkug) |
| [Qvadgad Wolves](/teams/qvadgadwolves) | [ST Qvadgad](/teams/stqvadgad) | [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) |
| [FT Vaz Ilkadh](/teams/ftvazilkadh) | [ST Naggar](/teams/stnaggar) | [FC Naggar](/teams/fcnaggar) |
