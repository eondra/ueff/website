---
author: "UEFF"
title: "Footorb Society Yllin (FSY)"
date: 2017-04-02T12:00:06+09:00
description: "Infopage about Yllin"
draft: false
type: "showcase"
---
![Yllin Flag](/images/flags/yllin_medium_md.png)

## Teams

| | | |
|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | [Magic Academy Theveluma](/teams/matheveluma) | [FC Iri Serin](/teams/fciriserin) |
| [Iri Serin Alchemists](/teams/iriserinalchemists) | [SC Ullh Alari](/teams/scullhalari) | [Ullh Alari Guards](/teams/ullhalariguards) |
| [Nfelin Druids](/teams/nfelindruids) | [Nfelin United](/teams/nfelinunited) | [FC Illa Ennore](/teams/fcillaennore) |
| [FS Illa Ennore](/teams/fsillaennore) | [Bard Academy Kyonore](/teams/bakyonore) | [SC Kyonore](/teams/sckyonore) |
