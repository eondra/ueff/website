---
author: "UEFF"
title: "Vikur Gautland"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vikur Gautland |
| Nation | [Jamta](/nations/jamta) |
| City | Gautland |
| Founded | 26.09.16 LC |
| Field | - |
| Head coach | [Sirnir Swiftroar](/characters/sirnir_swiftroar) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Lorkvaul Thorson](/characters/lorkvaul_thorson) | [Nordic](/races/nordic)     | 3.4.6 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Inciblick Scratchpatch](/characters/inciblick_scratchpatch) | [Gnome](/races/gnome)     | 20.5.105 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Taghahd Dragonsorrow](/characters/taghahd_dragonsorrow) | [Orc](/races/orc)     | 29.3.3 LC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Bild Boar-smith](/characters/bild_boar-smith) | [Nordic](/races/nordic)     | 21.12.11 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Ketill Enrardesson](/characters/ketill_enrardesson) | [Nordic](/races/nordic)     | 24.6.6 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Gjau Crag-light](/characters/gjau_crag-light) | [Nordic](/races/nordic)     | 6.10.9 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlorygg Hrolfrson](/characters/hlorygg_hrolfrson) | [Nordic](/races/nordic)     | 22.3.14 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hialti Grimolfrson](/characters/hialti_grimolfrson) | [Nordic](/races/nordic)     | 12.10.2 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Saemundr Klypprson](/characters/saemundr_klypprson) | [Nordic](/races/nordic)     | 31.10.15 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Vignvar Hammer-fire](/characters/vignvar_hammer-fire) | [Nordic](/races/nordic)     | 9.11.6 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlore Clanwinds](/characters/hlore_clanwinds) | [Nordic](/races/nordic)     | 29.5.2 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Nodumm Ember-smith](/characters/nodumm_ember-smith) | [Nordic](/races/nordic)     | 29.12.3 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Chrimdall Jolgeirson](/characters/chrimdall_jolgeirson) | [Nordic](/races/nordic)     | 1.5.12 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Imlik Briskcookie](/characters/imlik_briskcookie) | [Gnome](/races/gnome)     | 6.8.80 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Girgan Brondolfkin](/characters/girgan_brondolfkin) | [Nordic](/races/nordic)     | 23.6.13 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Prar Asnuk](/characters/prar_asnuk) | [Orc](/races/orc)     | 15.12.4 LC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Mag Mudchest](/characters/mag_mudchest) | [Dwarf](/races/dwarf)     | 13.5.62 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Gralfwiil Anson](/characters/gralfwiil_anson) | [Nordic](/races/nordic)     | 6.12.5 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Holmstein Boulderbrace](/characters/holmstein_boulderbrace) | [Nordic](/races/nordic)     | 27.10.2 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hjrntus Hrolleifrson](/characters/hjrntus_hrolleifrson) | [Nordic](/races/nordic)     | 17.5.12 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Oddleif Boneslayer](/characters/oddleif_boneslayer) | [Nordic](/races/nordic)     | 20.2.4 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Ragi Two-lute](/characters/ragi_two-lute) | [Nordic](/races/nordic)     | 13.3.5 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Orgnvaar Thormodson](/characters/orgnvaar_thormodson) | [Nordic](/races/nordic)     | 25.2.4 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Pibeeck Whistlescheme](/characters/pibeeck_whistlescheme) | [Gnome](/races/gnome)     | 19.11.65 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gauk Leidolfrkin](/characters/gauk_leidolfrkin) | [Nordic](/races/nordic)     | 8.3.6 BLC |

## Honours
