---
author: "UEFF"
title: "Ullh Alari Guards"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Ullh Alari Guards |
| Nation | [Yllin](/nations/yllin) |
| City | Ullh Alari |
| Founded | 15.08.06 LC |
| Field | - |
| Head coach | [Dalyor Zolmemam](/characters/dalyor_zolmemam) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Esodren Silentarrow](/characters/esodren_silentarrow) | [Elf](/races/elf)     | 19.11.173 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Celdim Evendancer](/characters/celdim_evendancer) | [Elf](/races/elf)     | 12.12.170 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Hunass Herdithas](/characters/hunass_herdithas) | [Elf](/races/elf)     | 8.9.174 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Kas Heiphine](/characters/kas_heiphine) | [Elf](/races/elf)     | 17.10.176 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Gasthige Shavaris](/characters/gasthige_shavaris) | [Elf](/races/elf)     | 14.7.163 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Ickitirn Strikespell](/characters/ickitirn_strikespell) | [Gnome](/races/gnome)     | 7.5.78 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Demarer Morvaris](/characters/demarer_morvaris) | [Elf](/races/elf)     | 30.1.162 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Thias Aewenys](/characters/thias_aewenys) | [Elf](/races/elf)     | 3.10.169 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Kozuri Soglom](/characters/kozuri_soglom) | [Dwarf](/races/dwarf)     | 11.11.93 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Knudnnir Deathforge](/characters/knudnnir_deathforge) | [Nordic](/races/nordic)     | 4.5.5 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Sanvesom Phipeiros](/characters/sanvesom_phipeiros) | [Elf](/races/elf)     | 7.3.167 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Latkadi Trickcable](/characters/latkadi_trickcable) | [Gnome](/races/gnome)     | 19.11.61 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Solthiel Zayae](/characters/solthiel_zayae) | [Elf](/races/elf)     | 13.8.164 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Lemli Swiftcrown](/characters/lemli_swiftcrown) | [Gnome](/races/gnome)     | 2.1.95 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Chalorn Giollvuhhah](/characters/chalorn_giollvuhhah) | [Elf](/races/elf)     | 8.5.176 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ruehnar Stardancer](/characters/ruehnar_stardancer) | [Elf](/races/elf)     | 31.10.165 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Feeckes Fuzzgauge](/characters/feeckes_fuzzgauge) | [Gnome](/races/gnome)     | 7.11.70 BLC |
| 18 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Suranados Shadeclouds](/characters/suranados_shadeclouds) | [Elf](/races/elf)     | 30.11.170 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Nhamashal Zan](/characters/nhamashal_zan) | [Elf](/races/elf)     | 5.6.174 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Kalman Bekankin](/characters/kalman_bekankin) | [Nordic](/races/nordic)     | 8.9.14 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mydraressian Mis](/characters/mydraressian_mis) | [Elf](/races/elf)     | 29.6.178 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Orenlais Farlen](/characters/orenlais_farlen) | [Elf](/races/elf)     | 29.3.177 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Zugar Carhorn](/characters/zugar_carhorn) | [Elf](/races/elf)     | 8.5.165 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mythanar Wan](/characters/mythanar_wan) | [Elf](/races/elf)     | 1.2.163 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Ger Ravakrana](/characters/ger_ravakrana) | [Elf](/races/elf)     | 27.11.179 BLC |

## Honours

