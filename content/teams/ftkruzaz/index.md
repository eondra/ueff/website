---
author: "UEFF"
title: "FT Kruzaz"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Tribe Kruzaz |
| Nation | [Urotha](/nations/urotha) |
| City | Kruzaz |
| Founded | 17.10.18 LC |
| Field | - |
| Head coach | [Finki Copperbit](/characters/finki_copperbit) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Ho-tuszirg Brorruuhd](/characters/ho-tuszirg_brorruuhd) | [Orc](/races/orc)     | 7.6.0 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Trivrold Lowtooth](/characters/trivrold_lowtooth) | [Orc](/races/orc)     | 24.12.1 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Tek Ghuk](/characters/tek_ghuk) | [Orc](/races/orc)     | 11.6.4 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Borm Zaam](/characters/borm_zaam) | [Orc](/races/orc)     | 27.7.3 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Kranefe Zikuk](/characters/kranefe_zikuk) | [Orc](/races/orc)     | 15.7.3 LC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Grastrec Bromgwadd](/characters/grastrec_bromgwadd) | [Dwarf](/races/dwarf)     | 8.10.69 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Azor Noruhd](/characters/azor_noruhd) | [Orc](/races/orc)     | 4.10.2 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Drash Donnin](/characters/drash_donnin) | [Orc](/races/orc)     | 14.5.1 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Brurzash Barrenslice](/characters/brurzash_barrenslice) | [Orc](/races/orc)     | 8.6.1 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Rulkurn Gar](/characters/rulkurn_gar) | [Orc](/races/orc)     | 13.6.0 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Bronk Kret](/characters/bronk_kret) | [Orc](/races/orc)     | 19.6.1 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Krosh Brightmask](/characters/krosh_brightmask) | [Orc](/races/orc)     | 28.3.3 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Adzokk Deaddrums](/characters/adzokk_deaddrums) | [Orc](/races/orc)     | 1.6.1 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drakk Chorohd](/characters/drakk_chorohd) | [Orc](/races/orc)     | 29.9.1 LC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Eeckak Gearbang](/characters/eeckak_gearbang) | [Gnome](/races/gnome)     | 4.7.84 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Abbaruuf Sumarlidason](/characters/abbaruuf_sumarlidason) | [Nordic](/races/nordic)     | 9.1.14 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Tioc Koboldblade](/characters/tioc_koboldblade) | [Dwarf](/races/dwarf)     | 7.4.70 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Cum Bittersword](/characters/cum_bittersword) | [Orc](/races/orc)     | 5.4.2 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Kuk Chikkud](/characters/kuk_chikkud) | [Orc](/races/orc)     | 21.2.3 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Kratomm Brirnem](/characters/kratomm_brirnem) | [Orc](/races/orc)     | 17.8.2 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drorntaluk Khuulketh](/characters/drorntaluk_khuulketh) | [Orc](/races/orc)     | 29.11.1 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrurk Fisttaker](/characters/vrurk_fisttaker) | [Orc](/races/orc)     | 24.11.2 LC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Ramosh Primeblade](/characters/ramosh_primeblade) | [Orc](/races/orc)     | 31.12.1 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrokk Chisreth](/characters/vrokk_chisreth) | [Orc](/races/orc)     | 30.7.1 BLC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Uklilvoz Stoutwatch](/characters/uklilvoz_stoutwatch) | [Orc](/races/orc)     | 1.1.0 LC |

## Honours
