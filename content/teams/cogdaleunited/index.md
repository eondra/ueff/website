---
author: "UEFF"
title: "Cogdale United"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Cogdale United |
| Nation | [Katargo](/nations/katargo) |
| City | Cogdale |
| Founded | 06.03.18 LC |
| Field | - |
| Head coach | [Tutlethizz Rustlight](/characters/tutlethizz_rustlight) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Ocilo Quietsignal](/characters/ocilo_quietsignal) | [Gnome](/races/gnome)     | 1.4.103 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Otlock Pulseblock](/characters/otlock_pulseblock) | [Gnome](/races/gnome)     | 7.1.102 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Parinic Fuzzstrip](/characters/parinic_fuzzstrip) | [Gnome](/races/gnome)     | 15.7.101 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Alkenk Porterdock](/characters/alkenk_porterdock) | [Gnome](/races/gnome)     | 3.12.49 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Fothkik Flickermix](/characters/fothkik_flickermix) | [Gnome](/races/gnome)     | 31.1.75 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Bumeezz Wobblestrip](/characters/bumeezz_wobblestrip) | [Gnome](/races/gnome)     | 25.5.40 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Erlick Shinebell](/characters/erlick_shinebell) | [Gnome](/races/gnome)     | 16.5.67 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Parinic Squiggleclue](/characters/parinic_squiggleclue) | [Gnome](/races/gnome)     | 22.2.42 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Uthefa Springfizz](/characters/uthefa_springfizz) | [Gnome](/races/gnome)     | 4.2.43 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Emu Wobbleballoon](/characters/emu_wobbleballoon) | [Gnome](/races/gnome)     | 26.10.89 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Ockeefok Clickcord](/characters/ockeefok_clickcord) | [Gnome](/races/gnome)     | 31.12.105 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Agmhff Veleifrson](/characters/agmhff_veleifrson) | [Nordic](/races/nordic)     | 7.7.6 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Funeethozz Steamwhistle](/characters/funeethozz_steamwhistle) | [Gnome](/races/gnome)     | 5.8.63 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Glunikirn Tinkbox](/characters/glunikirn_tinkbox) | [Gnome](/races/gnome)     | 12.9.76 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Bozrec Vundric](/characters/bozrec_vundric) | [Dwarf](/races/dwarf)     | 22.11.96 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Tinbifi Scratchpatch](/characters/tinbifi_scratchpatch) | [Gnome](/races/gnome)     | 28.10.57 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Thetlick Flukestitch](/characters/thetlick_flukestitch) | [Gnome](/races/gnome)     | 22.2.43 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Inciblick Acerfluke](/characters/inciblick_acerfluke) | [Gnome](/races/gnome)     | 7.8.62 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Lartgun Fiery-dawn](/characters/lartgun_fiery-dawn) | [Nordic](/races/nordic)     | 5.10.14 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Fickeern Bellowhammer](/characters/fickeern_bellowhammer) | [Gnome](/races/gnome)     | 10.4.95 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Imargi Wobblespring](/characters/imargi_wobblespring) | [Gnome](/races/gnome)     | 16.10.62 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnellenk Springbadge](/characters/gnellenk_springbadge) | [Gnome](/races/gnome)     | 15.8.57 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gollnach Svartkin](/characters/gollnach_svartkin) | [Nordic](/races/nordic)     | 16.1.7 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Belick Oilspring](/characters/belick_oilspring) | [Gnome](/races/gnome)     | 22.5.54 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dekdroth Soglom](/characters/dekdroth_soglom) | [Dwarf](/races/dwarf)     | 23.2.59 BLC |

## Honours

