---
author: "UEFF"
title: "Nfelin United"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Nfelin United |
| Nation | [Yllin](/nations/yllin) |
| City | Nfelin |
| Founded | 09.01.10 LC |
| Field | - |
| Head coach | [Dalongrerlig Brightbringer](/characters/dalongrerlig_brightbringer) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Krustham Grus](/characters/krustham_grus) | [Elf](/races/elf)     | 18.1.177 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Mashaldler Raloydark](/characters/mashaldler_raloydark) | [Elf](/races/elf)     | 20.3.173 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Thiler Forestleaf](/characters/thiler_forestleaf) | [Elf](/races/elf)     | 1.4.174 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Ara Boar-chucker](/characters/ara_boar-chucker) | [Nordic](/races/nordic)     | 3.3.6 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Nytadean Woodeye](/characters/nytadean_woodeye) | [Elf](/races/elf)     | 4.1.167 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Thusess Blackarrow](/characters/thusess_blackarrow) | [Elf](/races/elf)     | 30.5.168 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Iri Ravenwalker](/characters/iri_ravenwalker) | [Elf](/races/elf)     | 9.4.172 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Rhothomir Shieldforest](/characters/rhothomir_shieldforest) | [Elf](/races/elf)     | 4.12.160 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Fiwilleck Slipneedle](/characters/fiwilleck_slipneedle) | [Gnome](/races/gnome)     | 26.1.57 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Balfran Swiftgrove](/characters/balfran_swiftgrove) | [Elf](/races/elf)     | 19.7.166 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Kruglis Oriqen](/characters/kruglis_oriqen) | [Elf](/races/elf)     | 13.5.170 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Braellyn Steelwinds](/characters/braellyn_steelwinds) | [Nordic](/races/nordic)     | 2.4.14 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Bekleec Pullboss](/characters/bekleec_pullboss) | [Gnome](/races/gnome)     | 8.5.82 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Cylonian Yesrieth](/characters/cylonian_yesrieth) | [Elf](/races/elf)     | 22.1.178 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Ildoten Duskstar](/characters/ildoten_duskstar) | [Elf](/races/elf)     | 28.9.170 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Imlik Flukecog](/characters/imlik_flukecog) | [Gnome](/races/gnome)     | 13.3.99 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Evedolar Rainblade](/characters/evedolar_rainblade) | [Elf](/races/elf)     | 19.11.162 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Doylan Kubhog](/characters/doylan_kubhog) | [Dwarf](/races/dwarf)     | 10.5.93 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Kas Elhice](/characters/kas_elhice) | [Elf](/races/elf)     | 10.9.162 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Nuvian Yllapeiros](/characters/nuvian_yllapeiros) | [Elf](/races/elf)     | 28.11.165 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Renrern Zir](/characters/renrern_zir) | [Orc](/races/orc)     | 7.3.1 LC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Aydras Farcrest](/characters/aydras_farcrest) | [Elf](/races/elf)     | 1.5.168 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzirk Grandeye](/characters/kzirk_grandeye) | [Orc](/races/orc)     | 22.3.1 LC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Lydent Ravajor](/characters/lydent_ravajor) | [Elf](/races/elf)     | 13.9.170 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mavolaar Adleth](/characters/mavolaar_adleth) | [Elf](/races/elf)     | 16.6.161 BLC |

## Honours

