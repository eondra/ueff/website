---
author: "UEFF"
title: "ST Redkug"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Tribe Redkug |
| Nation | [Urotha](/nations/urotha) |
| City | Redkug |
| Founded | 25.06.04 LC |
| Field | - |
| Head coach | [Brarri Laughinghand](/characters/brarri_laughinghand) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Gish Essol](/characters/gish_essol) | [Orc](/races/orc)     | 29.9.0 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Marntuch Angersteel](/characters/marntuch_angersteel) | [Orc](/races/orc)     | 21.12.4 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Esagg Cruelmarch](/characters/esagg_cruelmarch) | [Orc](/races/orc)     | 11.8.4 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Sudorm Thoveth](/characters/sudorm_thoveth) | [Orc](/races/orc)     | 18.4.0 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Rimimm Saurdrum](/characters/rimimm_saurdrum) | [Orc](/races/orc)     | 24.5.0 LC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Vag Zoth](/characters/vag_zoth) | [Orc](/races/orc)     | 30.6.4 LC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Citald Warptwist](/characters/citald_warptwist) | [Orc](/races/orc)     | 8.7.0 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Usram Burningkill](/characters/usram_burningkill) | [Orc](/races/orc)     | 3.12.1 BLC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Muk Khuulketh](/characters/muk_khuulketh) | [Orc](/races/orc)     | 15.10.3 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Traldarg Maad](/characters/traldarg_maad) | [Orc](/races/orc)     | 10.2.3 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Gratost Asnuk](/characters/gratost_asnuk) | [Orc](/races/orc)     | 10.6.2 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzemgig Broadstriker](/characters/kzemgig_broadstriker) | [Orc](/races/orc)     | 11.6.3 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Krigust Uazot](/characters/krigust_uazot) | [Orc](/races/orc)     | 14.3.1 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drinvutzesh Kran](/characters/drinvutzesh_kran) | [Orc](/races/orc)     | 26.1.2 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrelok Kremgam](/characters/vrelok_kremgam) | [Orc](/races/orc)     | 19.2.1 LC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Leifae Olafurkin](/characters/leifae_olafurkin) | [Nordic](/races/nordic)     | 19.1.7 BLC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Muzcag Doomwatch](/characters/muzcag_doomwatch) | [Orc](/races/orc)     | 20.3.0 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Bul Thoveth](/characters/bul_thoveth) | [Orc](/races/orc)     | 2.10.1 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Oto Wickedcleaver](/characters/oto_wickedcleaver) | [Orc](/races/orc)     | 1.6.1 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Hildat Grith](/characters/hildat_grith) | [Orc](/races/orc)     | 25.11.1 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrich Graveteeth](/characters/vrich_graveteeth) | [Orc](/races/orc)     | 14.9.0 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Adzokk Donnin](/characters/adzokk_donnin) | [Orc](/races/orc)     | 20.10.3 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Crurl Primesorrow](/characters/crurl_primesorrow) | [Orc](/races/orc)     | 12.6.2 BLC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Surnzut-roz Stoutwatch](/characters/surnzut-roz_stoutwatch) | [Orc](/races/orc)     | 10.1.0 LC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Abrinlin Shiftcup](/characters/abrinlin_shiftcup) | [Gnome](/races/gnome)     | 30.11.40 BLC |

## Honours
