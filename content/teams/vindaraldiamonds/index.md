---
author: "UEFF"
title: "Vin Daral Diamonds"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vin Daral Diamonds |
| Nation | [Vongram](/nations/vongram) |
| City | Vin Daral |
| Founded | 18.09.12 LC |
| Field | - |
| Head coach | [Srod Ningid](/characters/srod_ningid) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Hamoic Coalborn](/characters/hamoic_coalborn) | [Dwarf](/races/dwarf)     | 1.7.63 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Fossael Renben](/characters/fossael_renben) | [Dwarf](/races/dwarf)     | 20.8.77 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Beyvad Flathead](/characters/beyvad_flathead) | [Dwarf](/races/dwarf)     | 24.4.66 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Gocmirg Solidhide](/characters/gocmirg_solidhide) | [Dwarf](/races/dwarf)     | 29.3.63 BLC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Huvyd Nindid](/characters/huvyd_nindid) | [Dwarf](/races/dwarf)     | 6.3.65 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Kabek Browngrip](/characters/kabek_browngrip) | [Dwarf](/races/dwarf)     | 25.11.52 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Rimgil Lightfeet](/characters/rimgil_lightfeet) | [Dwarf](/races/dwarf)     | 20.8.71 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Omnoud Flamebreath](/characters/omnoud_flamebreath) | [Dwarf](/races/dwarf)     | 3.4.62 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Fofaek Rilrin](/characters/fofaek_rilrin) | [Dwarf](/races/dwarf)     | 21.3.75 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Whughean Tresat](/characters/whughean_tresat) | [Dwarf](/races/dwarf)     | 8.5.75 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Khouvraer Coalheart](/characters/khouvraer_coalheart) | [Dwarf](/races/dwarf)     | 28.8.87 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hosglund Jurgifksson](/characters/hosglund_jurgifksson) | [Nordic](/races/nordic)     | 20.4.14 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Sradmom Flintheart](/characters/sradmom_flintheart) | [Dwarf](/races/dwarf)     | 27.6.98 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Drozmook Sulrum](/characters/drozmook_sulrum) | [Dwarf](/races/dwarf)     | 26.5.68 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Odnon Rumblepelt](/characters/odnon_rumblepelt) | [Dwarf](/races/dwarf)     | 16.8.81 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Mogoth Isnoth](/characters/mogoth_isnoth) | [Orc](/races/orc)     | 1.11.3 LC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Srak Zabras](/characters/srak_zabras) | [Dwarf](/races/dwarf)     | 13.3.80 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Pibeeck Wobblestrip](/characters/pibeeck_wobblestrip) | [Gnome](/races/gnome)     | 28.7.90 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Durikdromri Mithrilbraids](/characters/durikdromri_mithrilbraids) | [Dwarf](/races/dwarf)     | 2.11.60 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Rols Flatbraids](/characters/rols_flatbraids) | [Dwarf](/races/dwarf)     | 4.11.63 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Relgem Gravelspine](/characters/relgem_gravelspine) | [Dwarf](/races/dwarf)     | 4.11.55 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Kenved Streb](/characters/kenved_streb) | [Dwarf](/races/dwarf)     | 26.2.61 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dur Fairward](/characters/dur_fairward) | [Dwarf](/races/dwarf)     | 26.6.95 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dorargruth Fim](/characters/dorargruth_fim) | [Dwarf](/races/dwarf)     | 15.12.71 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Fosumlin Sragack](/characters/fosumlin_sragack) | [Dwarf](/races/dwarf)     | 27.3.54 BLC |

## Honours

