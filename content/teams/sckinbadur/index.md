---
author: "UEFF"
title: "SC Kinbadur"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Kinbadur |
| Nation | [Vongram](/nations/vongram) |
| City | Kinbadur |
| Founded | 05.11.08 LC |
| Field | - |
| Head coach | [Jangram Greybranch](/characters/jangram_greybranch) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Taran Goldflayer](/characters/taran_goldflayer) | [Dwarf](/races/dwarf)     | 8.4.86 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Klefilkonk Gripbrass](/characters/klefilkonk_gripbrass) | [Gnome](/races/gnome)     | 31.5.40 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Kaghaeg Mithrilforged](/characters/kaghaeg_mithrilforged) | [Dwarf](/races/dwarf)     | 18.1.58 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Bortic Pridedew](/characters/bortic_pridedew) | [Dwarf](/races/dwarf)     | 18.5.87 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Wrathful Berylbender](/characters/wrathful_berylbender) | [Dwarf](/races/dwarf)     | 23.8.52 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Glammuth Flintpelt](/characters/glammuth_flintpelt) | [Dwarf](/races/dwarf)     | 1.2.71 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Hulgamri Bitterbeard](/characters/hulgamri_bitterbeard) | [Dwarf](/races/dwarf)     | 7.8.66 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Ha1fr Hervardson](/characters/ha1fr_hervardson) | [Nordic](/races/nordic)     | 7.9.10 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Ruudd Deepview](/characters/ruudd_deepview) | [Dwarf](/races/dwarf)     | 9.10.63 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Vodur Undermaster](/characters/vodur_undermaster) | [Dwarf](/races/dwarf)     | 3.5.58 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Mar Taklat](/characters/mar_taklat) | [Dwarf](/races/dwarf)     | 5.12.70 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Kyg Sapphiremail](/characters/kyg_sapphiremail) | [Dwarf](/races/dwarf)     | 9.3.58 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Heyzor Wunnyrd](/characters/heyzor_wunnyrd) | [Dwarf](/races/dwarf)     | 4.10.79 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnonbish Wheelblock](/characters/gnonbish_wheelblock) | [Gnome](/races/gnome)     | 23.6.54 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Brerg Lightgrog](/characters/brerg_lightgrog) | [Dwarf](/races/dwarf)     | 23.1.70 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Groozumli Warsunder](/characters/groozumli_warsunder) | [Dwarf](/races/dwarf)     | 24.11.99 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Baridmur Zelred](/characters/baridmur_zelred) | [Dwarf](/races/dwarf)     | 15.1.88 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Gocmirg Bald](/characters/gocmirg_bald) | [Dwarf](/races/dwarf)     | 21.6.92 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Kusgrouth Warheart](/characters/kusgrouth_warheart) | [Dwarf](/races/dwarf)     | 23.10.60 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Brilliant Shatterheart](/characters/brilliant_shatterheart) | [Dwarf](/races/dwarf)     | 5.6.55 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Vamril Nimdid](/characters/vamril_nimdid) | [Dwarf](/races/dwarf)     | 24.9.88 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Durizmith Timbin](/characters/durizmith_timbin) | [Dwarf](/races/dwarf)     | 17.8.55 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Wicked Earthbluff](/characters/wicked_earthbluff) | [Dwarf](/races/dwarf)     | 24.9.72 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Doylan Grumblebrand](/characters/doylan_grumblebrand) | [Dwarf](/races/dwarf)     | 7.5.65 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Yaled Drakebasher](/characters/yaled_drakebasher) | [Dwarf](/races/dwarf)     | 15.7.80 BLC |

## Honours

