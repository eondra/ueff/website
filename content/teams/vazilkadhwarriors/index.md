---
author: "UEFF"
title: "Vaz Ilkadh Warriors"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vaz Ilkadh Warriors |
| Nation | [Urotha](/nations/urotha) |
| City | Vaz Ilkadh |
| Founded | 25.09.09 LC |
| Field | - |
| Head coach | [Cruld Disel](/characters/cruld_disel) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Kakk Gheggak](/characters/kakk_gheggak) | [Orc](/races/orc)     | 18.10.1 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Grilkk Halfmane](/characters/grilkk_halfmane) | [Orc](/races/orc)     | 2.11.4 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Eglak Dulker](/characters/eglak_dulker) | [Orc](/races/orc)     | 26.10.1 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Vravrach Wickedforce](/characters/vravrach_wickedforce) | [Orc](/races/orc)     | 21.4.0 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Gioc Goldmaker](/characters/gioc_goldmaker) | [Dwarf](/races/dwarf)     | 14.3.91 BLC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Dornzank Blackchewer](/characters/dornzank_blackchewer) | [Orc](/races/orc)     | 15.7.2 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hroar-hrothgar Bold-seeker](/characters/hroar-hrothgar_bold-seeker) | [Nordic](/races/nordic)     | 4.5.3 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Drilkzehd Clearcrusher](/characters/drilkzehd_clearcrusher) | [Orc](/races/orc)     | 2.12.3 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Crogi Bessis](/characters/crogi_bessis) | [Orc](/races/orc)     | 19.10.1 LC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Holgi Dawnbelly](/characters/holgi_dawnbelly) | [Nordic](/races/nordic)     | 5.12.10 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Hugehd Chomguush](/characters/hugehd_chomguush) | [Orc](/races/orc)     | 11.10.2 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drerl Brirosh](/characters/drerl_brirosh) | [Orc](/races/orc)     | 8.6.1 BLC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Prerd Zir](/characters/prerd_zir) | [Orc](/races/orc)     | 4.9.2 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzizcish Thoveth](/characters/kzizcish_thoveth) | [Orc](/races/orc)     | 9.5.1 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Vebzolk Bitterhorn](/characters/vebzolk_bitterhorn) | [Orc](/races/orc)     | 9.8.4 LC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Bargur-toth Vhur](/characters/bargur-toth_vhur) | [Orc](/races/orc)     | 30.11.4 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Krem Ehjut](/characters/krem_ehjut) | [Orc](/races/orc)     | 25.4.2 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Fadilk Starkfire](/characters/fadilk_starkfire) | [Orc](/races/orc)     | 17.4.3 LC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Teerkik Anglechin](/characters/teerkik_anglechin) | [Gnome](/races/gnome)     | 25.11.42 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Iszom Arnoth](/characters/iszom_arnoth) | [Orc](/races/orc)     | 25.1.3 LC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Imamkurg Clearcrusher](/characters/imamkurg_clearcrusher) | [Orc](/races/orc)     | 22.5.1 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Ramtkas Salt-hater](/characters/ramtkas_salt-hater) | [Nordic](/races/nordic)     | 27.11.8 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Maltsvar Hallgeirson](/characters/maltsvar_hallgeirson) | [Nordic](/races/nordic)     | 21.10.13 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kruthkik Switchhouse](/characters/kruthkik_switchhouse) | [Gnome](/races/gnome)     | 14.9.56 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Yulgraen Grandstrike](/characters/yulgraen_grandstrike) | [Dwarf](/races/dwarf)     | 5.1.98 BLC |

## Honours
