---
author: "UEFF"
title: "Gearingfort Inc"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Gearingfort Inc |
| Nation | [Katargo](/nations/katargo) |
| City | Gearingfort |
| Founded | 01.01.07 LC |
| Field | - |
| Head coach | [Ilirn Bizzpitch](/characters/ilirn_bizzpitch) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Itirn Steamtorque](/characters/itirn_steamtorque) | [Gnome](/races/gnome)     | 15.3.84 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Leldazz Oilphase](/characters/leldazz_oilphase) | [Gnome](/races/gnome)     | 12.5.52 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Klolkeesh Springcable](/characters/klolkeesh_springcable) | [Gnome](/races/gnome)     | 12.1.101 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Pinki Wobbledock](/characters/pinki_wobbledock) | [Gnome](/races/gnome)     | 3.9.100 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Lemkeeblin Quirkbonk](/characters/lemkeeblin_quirkbonk) | [Gnome](/races/gnome)     | 4.9.51 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlazz Bizzheart](/characters/itlazz_bizzheart) | [Gnome](/races/gnome)     | 11.4.78 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Eeckak Mekkablast](/characters/eeckak_mekkablast) | [Gnome](/races/gnome)     | 18.1.54 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Icisirn Luckcable](/characters/icisirn_luckcable) | [Gnome](/races/gnome)     | 10.2.74 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Peetki Shiftsteel](/characters/peetki_shiftsteel) | [Gnome](/races/gnome)     | 22.11.80 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnemkeefirn Portersignal](/characters/gnemkeefirn_portersignal) | [Gnome](/races/gnome)     | 19.6.66 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gaklotkirn Tinksteel](/characters/gaklotkirn_tinksteel) | [Gnome](/races/gnome)     | 5.7.47 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Miclon Battlelaugh](/characters/miclon_battlelaugh) | [Gnome](/races/gnome)     | 17.11.76 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hithkozz Railstrip](/characters/hithkozz_railstrip) | [Gnome](/races/gnome)     | 5.4.74 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnorleck Bizzwhistle](/characters/gnorleck_bizzwhistle) | [Gnome](/races/gnome)     | 21.7.75 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Beclimirn Buzzinkettle](/characters/beclimirn_buzzinkettle) | [Gnome](/races/gnome)     | 18.11.66 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Keendik Flukeheart](/characters/keendik_flukeheart) | [Gnome](/races/gnome)     | 14.8.83 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Krackec Stormbrick](/characters/krackec_stormbrick) | [Gnome](/races/gnome)     | 30.3.78 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Fukugus Draxlebrick](/characters/fukugus_draxlebrick) | [Gnome](/races/gnome)     | 30.7.104 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Linbikizz Quirkdish](/characters/linbikizz_quirkdish) | [Gnome](/races/gnome)     | 30.12.72 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Tenlizz Tidydrop](/characters/tenlizz_tidydrop) | [Gnome](/races/gnome)     | 26.2.85 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Klothki Switchhouse](/characters/klothki_switchhouse) | [Gnome](/races/gnome)     | 4.4.96 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Girgettlish Shinyballoon](/characters/girgettlish_shinyballoon) | [Gnome](/races/gnome)     | 2.2.93 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnellenk Oilspring](/characters/gnellenk_oilspring) | [Gnome](/races/gnome)     | 16.5.72 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Carlin Buzzinchart](/characters/carlin_buzzinchart) | [Gnome](/races/gnome)     | 24.10.90 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Thetlick Trickyblast](/characters/thetlick_trickyblast) | [Gnome](/races/gnome)     | 22.8.69 BLC |

## Honours

