---
author: "UEFF"
title: "FC Gearingfort"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Gearingfort |
| Nation | [Katargo](/nations/katargo) |
| City | Gearingfort |
| Founded | 10.11.09 LC |
| Field | - |
| Head coach | [Kliklimkan Fixcub](/characters/kliklimkan_fixcub) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Kittlick Tinkerspark](/characters/kittlick_tinkerspark) | [Gnome](/races/gnome)     | 26.10.92 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Thicurn Niftygrinder](/characters/thicurn_niftygrinder) | [Gnome](/races/gnome)     | 19.3.89 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlizz Tinkhouse](/characters/itlizz_tinkhouse) | [Gnome](/races/gnome)     | 10.12.51 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Klullink Buzzingrinder](/characters/klullink_buzzingrinder) | [Gnome](/races/gnome)     | 14.11.46 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Ariklik Overflow](/characters/ariklik_overflow) | [Gnome](/races/gnome)     | 26.7.49 BLC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Krorg Verrus](/characters/krorg_verrus) | [Orc](/races/orc)     | 19.4.2 LC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Lancish Flickerspell](/characters/lancish_flickerspell) | [Gnome](/races/gnome)     | 20.6.75 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kliciflu Porterflow](/characters/kliciflu_porterflow) | [Gnome](/races/gnome)     | 18.4.64 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Fandabrees Mekkabrick](/characters/fandabrees_mekkabrick) | [Gnome](/races/gnome)     | 16.7.99 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Glithkic Gripbrass](/characters/glithkic_gripbrass) | [Gnome](/races/gnome)     | 17.3.47 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kellok Dazzlebell](/characters/kellok_dazzlebell) | [Gnome](/races/gnome)     | 1.6.64 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Emu Acerkettle](/characters/emu_acerkettle) | [Gnome](/races/gnome)     | 23.8.59 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Ilac Castclock](/characters/ilac_castclock) | [Gnome](/races/gnome)     | 9.3.89 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Gullac Porterspinner](/characters/gullac_porterspinner) | [Gnome](/races/gnome)     | 21.4.62 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Klaki Berrychalk](/characters/klaki_berrychalk) | [Gnome](/races/gnome)     | 8.7.69 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilkees Blackfuzz](/characters/gnilkees_blackfuzz) | [Gnome](/races/gnome)     | 19.6.102 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Klekewizz Flukeballoon](/characters/klekewizz_flukeballoon) | [Gnome](/races/gnome)     | 10.2.96 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Kimkink Slipbell](/characters/kimkink_slipbell) | [Gnome](/races/gnome)     | 4.9.74 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Glivuklin Quickmaster](/characters/glivuklin_quickmaster) | [Gnome](/races/gnome)     | 14.11.56 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Imlik Buzzinkettle](/characters/imlik_buzzinkettle) | [Gnome](/races/gnome)     | 11.9.83 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Pillin Niftypitch](/characters/pillin_niftypitch) | [Gnome](/races/gnome)     | 14.1.99 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Lorongr Milk-healer](/characters/lorongr_milk-healer) | [Nordic](/races/nordic)     | 8.2.10 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Hinarn Sparklecrown](/characters/hinarn_sparklecrown) | [Gnome](/races/gnome)     | 3.10.47 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Ittlonk Stormboss](/characters/ittlonk_stormboss) | [Gnome](/races/gnome)     | 20.9.90 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Krabacleeck Luckcookie](/characters/krabacleeck_luckcookie) | [Gnome](/races/gnome)     | 8.3.94 BLC |

## Honours

