---
author: "UEFF"
title: "SC Redkug"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Redkug |
| Nation | [Urotha](/nations/urotha) |
| City | Redkug |
| Founded | 20.12.08 LC |
| Field | - |
| Head coach | [Marntuch Gedrek](/characters/marntuch_gedrek) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Tinac Dualblast](/characters/tinac_dualblast) | [Gnome](/races/gnome)     | 12.5.43 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Pillin Clickcollar](/characters/pillin_clickcollar) | [Gnome](/races/gnome)     | 4.5.100 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Drergagg Crot](/characters/drergagg_crot) | [Orc](/races/orc)     | 25.10.3 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Grarnot-rorl Ragefang](/characters/grarnot-rorl_ragefang) | [Orc](/races/orc)     | 4.5.3 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Drergagg Bitterseeker](/characters/drergagg_bitterseeker) | [Orc](/races/orc)     | 30.12.4 LC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Draldam Chaalrar](/characters/draldam_chaalrar) | [Orc](/races/orc)     | 21.1.0 LC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrird Coldsteel](/characters/vrird_coldsteel) | [Orc](/races/orc)     | 30.12.1 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Wonfit Highbrew](/characters/wonfit_highbrew) | [Dwarf](/races/dwarf)     | 20.7.53 BLC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Dorumm Dhiddes](/characters/dorumm_dhiddes) | [Orc](/races/orc)     | 26.10.1 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Bahlnoch Aridtaker](/characters/bahlnoch_aridtaker) | [Orc](/races/orc)     | 22.3.1 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Sutu-tesh Khomgoth](/characters/sutu-tesh_khomgoth) | [Orc](/races/orc)     | 23.7.4 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Mohlzorrem Zihd](/characters/mohlzorrem_zihd) | [Orc](/races/orc)     | 27.8.4 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Kan Bir](/characters/kan_bir) | [Orc](/races/orc)     | 22.6.2 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Dumkohlzo Grul](/characters/dumkohlzo_grul) | [Orc](/races/orc)     | 8.12.3 LC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Ezun Mesh](/characters/ezun_mesh) | [Orc](/races/orc)     | 18.7.0 LC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Prerd Nir](/characters/prerd_nir) | [Orc](/races/orc)     | 17.1.0 LC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Urkink Sparkcollar](/characters/urkink_sparkcollar) | [Gnome](/races/gnome)     | 7.5.103 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Crir Verrus](/characters/crir_verrus) | [Orc](/races/orc)     | 26.7.1 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Krovnis Deadwolf](/characters/krovnis_deadwolf) | [Orc](/races/orc)     | 26.10.2 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Duze Steellaugh](/characters/duze_steellaugh) | [Orc](/races/orc)     | 7.10.3 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Dusdern Ghuuvruk](/characters/dusdern_ghuuvruk) | [Orc](/races/orc)     | 10.7.0 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Gilorn Vhur](/characters/gilorn_vhur) | [Orc](/races/orc)     | 4.7.1 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Volkk Crer](/characters/volkk_crer) | [Orc](/races/orc)     | 22.12.1 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Sor Fistbone](/characters/sor_fistbone) | [Orc](/races/orc)     | 13.10.3 LC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Gruknern Deathbreath](/characters/gruknern_deathbreath) | [Orc](/races/orc)     | 3.4.1 BLC |

## Honours
