---
author: "UEFF"
title: "FC Tinkerbury"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Tinkerbury |
| Nation | [Katargo](/nations/katargo) |
| City | Tinkerbury |
| Founded | 20.02.05 LC |
| Field | - |
| Head coach | [Hori Grimeyhouse](/characters/hori_grimeyhouse) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Meenkis Stormbrick](/characters/meenkis_stormbrick) | [Gnome](/races/gnome)     | 25.11.100 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Ithern Grimeyhouse](/characters/ithern_grimeyhouse) | [Gnome](/races/gnome)     | 25.9.99 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Girgettlish Draxlebadge](/characters/girgettlish_draxlebadge) | [Gnome](/races/gnome)     | 5.8.56 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Indozz Lightwhistle](/characters/indozz_lightwhistle) | [Gnome](/races/gnome)     | 30.11.59 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Thonlesh Switchbit](/characters/thonlesh_switchbit) | [Gnome](/races/gnome)     | 4.3.52 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Bedirn Porterstrip](/characters/bedirn_porterstrip) | [Gnome](/races/gnome)     | 7.10.55 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Peetki Gearcog](/characters/peetki_gearcog) | [Gnome](/races/gnome)     | 13.6.84 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Keendik Wiggletwist](/characters/keendik_wiggletwist) | [Gnome](/races/gnome)     | 13.6.86 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Orkiwee Grimeytorque](/characters/orkiwee_grimeytorque) | [Gnome](/races/gnome)     | 28.1.69 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Mildi Castcase](/characters/mildi_castcase) | [Gnome](/races/gnome)     | 26.3.50 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnetlurn Slipneedle](/characters/gnetlurn_slipneedle) | [Gnome](/races/gnome)     | 2.11.95 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Fidic Grimeytorque](/characters/fidic_grimeytorque) | [Gnome](/races/gnome)     | 30.12.101 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Enbazz Bellowbus](/characters/enbazz_bellowbus) | [Gnome](/races/gnome)     | 27.7.60 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Fiwilleck Trickcookie](/characters/fiwilleck_trickcookie) | [Gnome](/races/gnome)     | 6.6.91 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Much Uazot](/characters/much_uazot) | [Orc](/races/orc)     | 12.6.4 LC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Ihlmush Thufrahn](/characters/ihlmush_thufrahn) | [Orc](/races/orc)     | 2.9.2 LC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnemkeefirn Clickgrinder](/characters/gnemkeefirn_clickgrinder) | [Gnome](/races/gnome)     | 18.2.95 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Ofocleen Battleflow](/characters/ofocleen_battleflow) | [Gnome](/races/gnome)     | 18.4.60 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Kicis Bizzwhistle](/characters/kicis_bizzwhistle) | [Gnome](/races/gnome)     | 23.2.100 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Terin Acercollar](/characters/terin_acercollar) | [Gnome](/races/gnome)     | 9.7.64 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Leetkarn Draxletorque](/characters/leetkarn_draxletorque) | [Gnome](/races/gnome)     | 5.5.73 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Feld Burningfight](/characters/feld_burningfight) | [Orc](/races/orc)     | 29.10.1 LC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Biwakec Shinebell](/characters/biwakec_shinebell) | [Gnome](/races/gnome)     | 24.7.63 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Aldeck Sharppipe](/characters/aldeck_sharppipe) | [Gnome](/races/gnome)     | 24.2.56 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Juryn Brownbelly](/characters/juryn_brownbelly) | [Dwarf](/races/dwarf)     | 20.4.88 BLC |

## Honours
