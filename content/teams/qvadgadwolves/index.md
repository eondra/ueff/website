---
author: "UEFF"
title: "Qvadgad Wolves"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Qvadgad Wolves |
| Nation | [Urotha](/nations/urotha) |
| City | Qvadgad |
| Founded | 23.06.13 LC |
| Field | - |
| Head coach | [Lokihor Lightshield](/characters/lokihor_lightshield) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Sotam Cheth](/characters/sotam_cheth) | [Orc](/races/orc)     | 1.3.1 BLC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Milkk Ezkohd](/characters/milkk_ezkohd) | [Orc](/races/orc)     | 11.7.1 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Angug Nil](/characters/angug_nil) | [Orc](/races/orc)     | 14.12.3 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Thrikar Brehjosh](/characters/thrikar_brehjosh) | [Orc](/races/orc)     | 8.8.1 LC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Gromvaror Hrolleifrson](/characters/gromvaror_hrolleifrson) | [Nordic](/races/nordic)     | 9.6.8 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Mutzo Rot](/characters/mutzo_rot) | [Orc](/races/orc)     | 27.2.2 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Kringurg Laughinghand](/characters/kringurg_laughinghand) | [Orc](/races/orc)     | 25.5.1 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Roresk Thusgash](/characters/roresk_thusgash) | [Orc](/races/orc)     | 26.6.2 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Gagzukk Thufrahn](/characters/gagzukk_thufrahn) | [Orc](/races/orc)     | 30.4.2 BLC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Crergag Redspirit](/characters/crergag_redspirit) | [Orc](/races/orc)     | 29.6.1 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Trosh Thud](/characters/trosh_thud) | [Orc](/races/orc)     | 3.4.2 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Zaz-guvost Tas](/characters/zaz-guvost_tas) | [Orc](/races/orc)     | 9.5.4 LC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Torrfyg Ravenscream](/characters/torrfyg_ravenscream) | [Nordic](/races/nordic)     | 24.12.11 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Trork Velkihn](/characters/trork_velkihn) | [Orc](/races/orc)     | 28.11.4 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Thornonk Loweye](/characters/thornonk_loweye) | [Orc](/races/orc)     | 6.6.2 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Duzathe Dhus](/characters/duzathe_dhus) | [Orc](/races/orc)     | 10.12.2 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Murn Kirkon](/characters/murn_kirkon) | [Orc](/races/orc)     | 25.6.2 LC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Galyllian Steinason](/characters/galyllian_steinason) | [Nordic](/races/nordic)     | 24.1.14 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Moch Sharpmight](/characters/moch_sharpmight) | [Orc](/races/orc)     | 17.6.2 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Mavte Dhuvnid](/characters/mavte_dhuvnid) | [Orc](/races/orc)     | 13.9.2 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kremogg Brightmask](/characters/kremogg_brightmask) | [Orc](/races/orc)     | 21.9.1 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Trork Warhand](/characters/trork_warhand) | [Orc](/races/orc)     | 27.6.0 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Triddash Lowfire](/characters/triddash_lowfire) | [Orc](/races/orc)     | 8.9.3 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Grik Cravenchest](/characters/grik_cravenchest) | [Orc](/races/orc)     | 2.12.4 LC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Ulfkell Ehrarikesen](/characters/ulfkell_ehrarikesen) | [Nordic](/races/nordic)     | 15.7.6 BLC |

## Honours
