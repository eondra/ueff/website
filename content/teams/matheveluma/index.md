---
author: "UEFF"
title: "MA Theveluma"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Magic Academy Theveluma |
| Nation | [Yllin](/nations/yllin) |
| City | Theveluma |
| Founded | 17.12.15 LC |
| Field | - |
| Head coach | [Cethidan Glynnelis](/characters/cethidan_glynnelis) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Thias Foreststriker](/characters/thias_foreststriker) | [Elf](/races/elf)     | 8.6.178 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Vungarth Milk-healer](/characters/vungarth_milk-healer) | [Nordic](/races/nordic)     | 21.9.11 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hontgen Ravensword](/characters/hontgen_ravensword) | [Nordic](/races/nordic)     | 30.9.9 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Saelethil Erzeiros](/characters/saelethil_erzeiros) | [Elf](/races/elf)     | 18.6.177 BLC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Tugish Kornuhn](/characters/tugish_kornuhn) | [Orc](/races/orc)     | 16.8.3 LC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Lithodroris Cor](/characters/lithodroris_cor) | [Elf](/races/elf)     | 13.11.171 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Kraenithodvioss Fensys](/characters/kraenithodvioss_fensys) | [Elf](/races/elf)     | 11.3.161 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Tiosolon Itlimum](/characters/tiosolon_itlimum) | [Elf](/races/elf)     | 21.2.173 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Ylithir Gilzumin](/characters/ylithir_gilzumin) | [Elf](/races/elf)     | 6.12.176 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Dundoc Dimtoe](/characters/dundoc_dimtoe) | [Dwarf](/races/dwarf)     | 27.6.81 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Ayduin Saryarus](/characters/ayduin_saryarus) | [Elf](/races/elf)     | 17.12.165 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Odan Carnelis](/characters/odan_carnelis) | [Elf](/races/elf)     | 26.7.163 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Ojios Voghusa](/characters/ojios_voghusa) | [Elf](/races/elf)     | 29.7.161 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Sylvar Zonvutlar](/characters/sylvar_zonvutlar) | [Elf](/races/elf)     | 30.4.170 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Gormer Magsalor](/characters/gormer_magsalor) | [Elf](/races/elf)     | 17.5.176 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Talvul Forestbreath](/characters/talvul_forestbreath) | [Elf](/races/elf)     | 15.2.163 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Zol Cailynn](/characters/zol_cailynn) | [Elf](/races/elf)     | 28.5.162 BLC |
| 18 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ovine Coshi](/characters/ovine_coshi) | [Elf](/races/elf)     | 8.3.172 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Halflar Urigolor](/characters/halflar_urigolor) | [Elf](/races/elf)     | 24.5.176 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Meves Treeshot](/characters/meves_treeshot) | [Elf](/races/elf)     | 11.12.179 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Zulim Fasatra](/characters/zulim_fasatra) | [Elf](/races/elf)     | 23.9.177 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Ven Nas](/characters/ven_nas) | [Elf](/races/elf)     | 16.3.166 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Jithae Thebella](/characters/jithae_thebella) | [Elf](/races/elf)     | 2.10.174 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Wealaeth Calvim](/characters/wealaeth_calvim) | [Elf](/races/elf)     | 20.11.178 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Ylinar Tem](/characters/ylinar_tem) | [Elf](/races/elf)     | 7.11.177 BLC |

## Honours

