---
author: "UEFF"
title: "SC Dim Garom"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Dim Garom |
| Nation | [Vongram](/nations/vongram) |
| City | Dim Garom |
| Founded | 14.04.05 LC |
| Field | - |
| Head coach | [Cazrem Kornod](/characters/cazrem_kornod) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Sighoud Beastcloak](/characters/sighoud_beastcloak) | [Dwarf](/races/dwarf)     | 12.11.66 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Brukdarn Evnath](/characters/brukdarn_evnath) | [Orc](/races/orc)     | 20.10.1 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Bomkag Tatlan](/characters/bomkag_tatlan) | [Dwarf](/races/dwarf)     | 24.8.86 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Nuznim Hammerhide](/characters/nuznim_hammerhide) | [Dwarf](/races/dwarf)     | 26.6.77 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Kud Tribhac](/characters/kud_tribhac) | [Dwarf](/races/dwarf)     | 21.10.98 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Grusgraic Kyrg](/characters/grusgraic_kyrg) | [Dwarf](/races/dwarf)     | 10.6.83 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Throthead Seck](/characters/throthead_seck) | [Dwarf](/races/dwarf)     | 5.5.61 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Khutheack Jondus](/characters/khutheack_jondus) | [Dwarf](/races/dwarf)     | 31.5.80 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bryknyn Yoglet](/characters/bryknyn_yoglet) | [Dwarf](/races/dwarf)     | 1.1.58 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Whusgrac Leadmail](/characters/whusgrac_leadmail) | [Dwarf](/races/dwarf)     | 20.4.77 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Hock Grodd](/characters/hock_grodd) | [Dwarf](/races/dwarf)     | 17.4.62 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Brodolin Honorriver](/characters/brodolin_honorriver) | [Dwarf](/races/dwarf)     | 1.5.63 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hallbiorn Hakason](/characters/hallbiorn_hakason) | [Nordic](/races/nordic)     | 25.9.13 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Comkug Tradwib](/characters/comkug_tradwib) | [Dwarf](/races/dwarf)     | 15.2.88 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Cruzmem Blackthunder](/characters/cruzmem_blackthunder) | [Orc](/races/orc)     | 10.4.1 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Drohn Forechewer](/characters/drohn_forechewer) | [Orc](/races/orc)     | 19.10.0 LC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Jocmiot Largegrog](/characters/jocmiot_largegrog) | [Dwarf](/races/dwarf)     | 20.3.53 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Wog Forgebuster](/characters/wog_forgebuster) | [Dwarf](/races/dwarf)     | 2.6.81 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Jal Thundergut](/characters/jal_thundergut) | [Dwarf](/races/dwarf)     | 9.10.58 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Ulgursk Deepcrest](/characters/ulgursk_deepcrest) | [Dwarf](/races/dwarf)     | 22.7.68 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Gurgeg Whitbraid](/characters/gurgeg_whitbraid) | [Dwarf](/races/dwarf)     | 27.9.51 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kormurn Mahd](/characters/kormurn_mahd) | [Orc](/races/orc)     | 9.7.2 LC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Iblettloc Bellowshape](/characters/iblettloc_bellowshape) | [Gnome](/races/gnome)     | 12.10.77 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Gizbul Goldbender](/characters/gizbul_goldbender) | [Dwarf](/races/dwarf)     | 10.8.73 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Throthead Amberfall](/characters/throthead_amberfall) | [Dwarf](/races/dwarf)     | 24.3.67 BLC |

## Honours

