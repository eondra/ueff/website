---
author: "UEFF"
title: "BA Kyonore"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Bard Academy Kyonore |
| Nation | [Yllin](/nations/yllin) |
| City | Kyonore |
| Founded | 01.05.11 LC |
| Field | - |
| Head coach | [Fanturas Rapidwhisper](/characters/fanturas_rapidwhisper) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Krur Darkforest](/characters/krur_darkforest) | [Elf](/races/elf)     | 5.3.166 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Fedviess Miststriker](/characters/fedviess_miststriker) | [Elf](/races/elf)     | 16.3.163 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Athaes Biwarin](/characters/athaes_biwarin) | [Elf](/races/elf)     | 26.1.169 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Aladaen Aethana](/characters/aladaen_aethana) | [Elf](/races/elf)     | 24.9.163 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Helnim Bearscribe](/characters/helnim_bearscribe) | [Elf](/races/elf)     | 1.4.164 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Sian Rainsong](/characters/sian_rainsong) | [Elf](/races/elf)     | 9.8.180 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Iliphar Glynven](/characters/iliphar_glynven) | [Elf](/races/elf)     | 8.2.172 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Thuther Presven](/characters/thuther_presven) | [Elf](/races/elf)     | 1.8.175 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Nadraen Zhirom](/characters/nadraen_zhirom) | [Elf](/races/elf)     | 6.7.171 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Fyladaen Voidbreath](/characters/fyladaen_voidbreath) | [Elf](/races/elf)     | 11.9.178 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Grurg Wrathfang](/characters/grurg_wrathfang) | [Orc](/races/orc)     | 10.5.0 LC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Elkos Quirkbonk](/characters/elkos_quirkbonk) | [Gnome](/races/gnome)     | 29.12.51 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Terendrannul Jilvinral](/characters/terendrannul_jilvinral) | [Elf](/races/elf)     | 20.8.165 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Desvelelsol Starstar](/characters/desvelelsol_starstar) | [Elf](/races/elf)     | 25.4.178 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Ostess Forestbreath](/characters/ostess_forestbreath) | [Elf](/races/elf)     | 12.5.162 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Lathlaeril Nem](/characters/lathlaeril_nem) | [Elf](/races/elf)     | 16.8.166 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Thulsom Saeyudim](/characters/thulsom_saeyudim) | [Elf](/races/elf)     | 28.8.163 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Gorluin Loragella](/characters/gorluin_loragella) | [Elf](/races/elf)     | 5.7.169 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [A-dan Lightbreeze](/characters/a-dan_lightbreeze) | [Elf](/races/elf)     | 11.9.172 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Hedos Nallmura](/characters/hedos_nallmura) | [Elf](/races/elf)     | 9.5.172 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Sunk Warhand](/characters/sunk_warhand) | [Orc](/races/orc)     | 7.9.1 LC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Lydlass Fashih](/characters/lydlass_fashih) | [Elf](/races/elf)     | 3.3.171 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Garril Yelphyra](/characters/garril_yelphyra) | [Elf](/races/elf)     | 19.12.176 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Aubron Eilwarin](/characters/aubron_eilwarin) | [Elf](/races/elf)     | 10.12.171 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Folas Fillaseh](/characters/folas_fillaseh) | [Elf](/races/elf)     | 9.1.170 BLC |

## Honours
