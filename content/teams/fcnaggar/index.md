---
author: "UEFF"
title: "FC Naggar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Naggar |
| Nation | [Urotha](/nations/urotha) |
| City | Naggar |
| Founded | 09.12.15 LC |
| Field | - |
| Head coach | [Mokk Rockrage](/characters/mokk_rockrage) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrulk Muzkoth](/characters/vrulk_muzkoth) | [Orc](/races/orc)     | 22.11.0 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Fild Brum](/characters/fild_brum) | [Orc](/races/orc)     | 17.10.2 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Azesh Dhush](/characters/azesh_dhush) | [Orc](/races/orc)     | 9.12.0 LC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Peemkos Oilphase](/characters/peemkos_oilphase) | [Gnome](/races/gnome)     | 1.10.55 BLC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Zem Darkpride](/characters/zem_darkpride) | [Orc](/races/orc)     | 3.11.0 LC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Vodzosh Warpspite](/characters/vodzosh_warpspite) | [Orc](/races/orc)     | 20.2.4 LC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Cral Shadowbattle](/characters/cral_shadowbattle) | [Orc](/races/orc)     | 20.9.1 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Rargana Bittersword](/characters/rargana_bittersword) | [Orc](/races/orc)     | 1.3.0 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Hamrot Ghovrom](/characters/hamrot_ghovrom) | [Orc](/races/orc)     | 20.12.1 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gleeklon Mintbells](/characters/gleeklon_mintbells) | [Gnome](/races/gnome)     | 13.4.70 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Muk Lowsplitter](/characters/muk_lowsplitter) | [Orc](/races/orc)     | 11.1.1 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drelor Khun](/characters/drelor_khun) | [Orc](/races/orc)     | 6.5.2 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Covrilkk Vhush](/characters/covrilkk_vhush) | [Orc](/races/orc)     | 17.4.3 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Cibom Darksong](/characters/cibom_darksong) | [Orc](/races/orc)     | 7.3.2 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drodzohm Boldhammer](/characters/drodzohm_boldhammer) | [Orc](/races/orc)     | 27.9.2 LC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Brosovekk Ironkill](/characters/brosovekk_ironkill) | [Orc](/races/orc)     | 11.5.1 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Rel Mer](/characters/rel_mer) | [Orc](/races/orc)     | 12.8.4 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Rekko Kran](/characters/rekko_kran) | [Orc](/races/orc)     | 2.7.2 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Zavo Grul](/characters/zavo_grul) | [Orc](/races/orc)     | 21.8.1 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Dom-durnder Sharpwish](/characters/dom-durnder_sharpwish) | [Orc](/races/orc)     | 15.9.1 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drokk Rilgod](/characters/drokk_rilgod) | [Orc](/races/orc)     | 17.5.3 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Hurgag-rul Ghaan](/characters/hurgag-rul_ghaan) | [Orc](/races/orc)     | 12.7.4 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drash Rilgod](/characters/drash_rilgod) | [Orc](/races/orc)     | 18.7.2 LC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlizz Quietsignal](/characters/itlizz_quietsignal) | [Gnome](/races/gnome)     | 24.10.63 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kluttlezz Teenygauge](/characters/kluttlezz_teenygauge) | [Gnome](/races/gnome)     | 6.4.88 BLC |

## Honours
