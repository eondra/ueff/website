---
author: "UEFF"
title: "FS Illa Ennore"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Society Illa Ennore |
| Nation | [Yllin](/nations/yllin) |
| City | Illa Ennore |
| Founded | 23.11.18 LC |
| Field | - |
| Head coach | [Thoglon Xilrieth](/characters/thoglon_xilrieth) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Ther Umetoris](/characters/ther_umetoris) | [Elf](/races/elf)     | 26.11.161 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Mavolin Zonvutlar](/characters/mavolin_zonvutlar) | [Elf](/races/elf)     | 14.7.169 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Ylithir Nassuviom](/characters/ylithir_nassuviom) | [Elf](/races/elf)     | 9.1.180 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Jandar Ner](/characters/jandar_ner) | [Elf](/races/elf)     | 13.3.177 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Kyrir Djensen](/characters/kyrir_djensen) | [Nordic](/races/nordic)     | 15.4.3 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Gurbed Battlehand](/characters/gurbed_battlehand) | [Dwarf](/races/dwarf)     | 28.4.74 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Deldlijiaclar Rapidstar](/characters/deldlijiaclar_rapidstar) | [Elf](/races/elf)     | 21.12.176 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Kelvhan Greenstriker](/characters/kelvhan_greenstriker) | [Elf](/races/elf)     | 8.8.180 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Kuvin Shildrilhra](/characters/kuvin_shildrilhra) | [Elf](/races/elf)     | 18.4.163 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Tusvaenvru Unaemar](/characters/tusvaenvru_unaemar) | [Elf](/races/elf)     | 20.10.160 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Rulkurn Brozmod](/characters/rulkurn_brozmod) | [Orc](/races/orc)     | 2.5.0 LC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Vass Torfina](/characters/vass_torfina) | [Elf](/races/elf)     | 27.5.179 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Visthil Yessalor](/characters/visthil_yessalor) | [Elf](/races/elf)     | 8.2.180 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Rihm Crueltwist](/characters/rihm_crueltwist) | [Orc](/races/orc)     | 11.4.1 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Iamardiss Zonvutlar](/characters/iamardiss_zonvutlar) | [Elf](/races/elf)     | 12.12.180 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Lior Fogstar](/characters/lior_fogstar) | [Elf](/races/elf)     | 14.9.169 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Tham Zaenlairo](/characters/tham_zaenlairo) | [Elf](/races/elf)     | 3.3.180 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Brolt Brokendeath](/characters/brolt_brokendeath) | [Orc](/races/orc)     | 27.2.4 LC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hring Heyjang-bjornnson](/characters/hring_heyjang-bjornnson) | [Nordic](/races/nordic)     | 2.11.15 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Vamir Zhilvonthais](/characters/vamir_zhilvonthais) | [Elf](/races/elf)     | 25.9.163 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Pirphal Daglostior](/characters/pirphal_daglostior) | [Elf](/races/elf)     | 2.9.166 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Zeno Trisgwyn](/characters/zeno_trisgwyn) | [Elf](/races/elf)     | 30.8.176 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Emagorn Sunrunner](/characters/emagorn_sunrunner) | [Elf](/races/elf)     | 25.7.174 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Iefyr Coshi](/characters/iefyr_coshi) | [Elf](/races/elf)     | 30.5.169 BLC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Dravzu Grur](/characters/dravzu_grur) | [Orc](/races/orc)     | 10.1.1 LC |

## Honours

