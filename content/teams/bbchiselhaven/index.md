---
author: "UEFF"
title: "BB Chiselhaven"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Big Boys Chiselhaven |
| Nation | [Katargo](/nations/katargo) |
| City | Chiselhaven |
| Founded | 13.05.19 LC |
| Field | - |
| Head coach | [Klefilkonk Battlelaugh](/characters/klefilkonk_battlelaugh) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Hidiblic Fixneedle](/characters/hidiblic_fixneedle) | [Gnome](/races/gnome)     | 14.1.42 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Efinuck Bizzwhistle](/characters/efinuck_bizzwhistle) | [Gnome](/races/gnome)     | 31.5.105 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Lisencec Stitchbus](/characters/lisencec_stitchbus) | [Gnome](/races/gnome)     | 27.6.82 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Kimli Sharpfizzle](/characters/kimli_sharpfizzle) | [Gnome](/races/gnome)     | 11.9.102 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Gleeklon Porterspinner](/characters/gleeklon_porterspinner) | [Gnome](/races/gnome)     | 29.1.60 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Kirlis Lockfuse](/characters/kirlis_lockfuse) | [Gnome](/races/gnome)     | 9.1.69 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Hinarn Grimeyhouse](/characters/hinarn_grimeyhouse) | [Gnome](/races/gnome)     | 6.8.90 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Muvorkek Flukeheart](/characters/muvorkek_flukeheart) | [Gnome](/races/gnome)     | 1.1.41 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Tibliklan Trickguard](/characters/tibliklan_trickguard) | [Gnome](/races/gnome)     | 9.12.78 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnirkabrik Fizzlecable](/characters/gnirkabrik_fizzlecable) | [Gnome](/races/gnome)     | 22.1.42 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlorygg Asalderson](/characters/hlorygg_asalderson) | [Nordic](/races/nordic)     | 24.12.13 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Eeklareck Pipepatch](/characters/eeklareck_pipepatch) | [Gnome](/races/gnome)     | 24.6.76 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hitli Strikebrake](/characters/hitli_strikebrake) | [Gnome](/races/gnome)     | 16.4.97 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Cunkack Berrysprocket](/characters/cunkack_berrysprocket) | [Gnome](/races/gnome)     | 24.2.67 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hilee Fuzzkettle](/characters/hilee_fuzzkettle) | [Gnome](/races/gnome)     | 19.11.65 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Cemkin Berrydock](/characters/cemkin_berrydock) | [Gnome](/races/gnome)     | 5.3.72 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Ariklik Berrysprocket](/characters/ariklik_berrysprocket) | [Gnome](/races/gnome)     | 19.10.82 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Lathkigeesh Gripclick](/characters/lathkigeesh_gripclick) | [Gnome](/races/gnome)     | 16.5.52 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Kleetoc Trickcookie](/characters/kleetoc_trickcookie) | [Gnome](/races/gnome)     | 1.9.88 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlun Dualchin](/characters/itlun_dualchin) | [Gnome](/races/gnome)     | 17.12.95 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Domraer Minefoot](/characters/domraer_minefoot) | [Dwarf](/races/dwarf)     | 12.10.83 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilkeck Thistlegear](/characters/gnilkeck_thistlegear) | [Gnome](/races/gnome)     | 22.11.94 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Krillash Flickercoil](/characters/krillash_flickercoil) | [Gnome](/races/gnome)     | 26.12.102 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Ditec Acerkettle](/characters/ditec_acerkettle) | [Gnome](/races/gnome)     | 28.11.50 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kruthkik Rustcable](/characters/kruthkik_rustcable) | [Gnome](/races/gnome)     | 13.6.105 BLC |

## Honours

