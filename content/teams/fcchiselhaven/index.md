---
author: "UEFF"
title: "FC Chiselhaven"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Chiselhaven |
| Nation | [Katargo](/nations/katargo) |
| City | Chiselhaven |
| Founded | 25.02.08 LC |
| Field | - |
| Head coach | [Klolkeesh Twistcount](/characters/klolkeesh_twistcount) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Theeckazz Quietsignal](/characters/theeckazz_quietsignal) | [Gnome](/races/gnome)     | 22.10.104 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Ificlock Sprysprocket](/characters/ificlock_sprysprocket) | [Gnome](/races/gnome)     | 11.8.77 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Atlizz Overbrick](/characters/atlizz_overbrick) | [Gnome](/races/gnome)     | 29.5.81 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Thelkin Railflow](/characters/thelkin_railflow) | [Gnome](/races/gnome)     | 16.2.51 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Svadi Gjarssen](/characters/svadi_gjarssen) | [Nordic](/races/nordic)     | 3.3.10 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Keebus Anglenozzle](/characters/keebus_anglenozzle) | [Gnome](/races/gnome)     | 31.10.41 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gace Springsteel](/characters/gace_springsteel) | [Gnome](/races/gnome)     | 22.3.66 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kirlis Mintbrass](/characters/kirlis_mintbrass) | [Gnome](/races/gnome)     | 25.3.82 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Thonlesh Oilybox](/characters/thonlesh_oilybox) | [Gnome](/races/gnome)     | 7.12.54 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Cemki Tosslepatch](/characters/cemki_tosslepatch) | [Gnome](/races/gnome)     | 10.1.44 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Peetki Heavyspell](/characters/peetki_heavyspell) | [Gnome](/races/gnome)     | 12.5.71 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Krakliwuk Grindlocket](/characters/krakliwuk_grindlocket) | [Gnome](/races/gnome)     | 17.5.50 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Girtud Leathershield](/characters/girtud_leathershield) | [Dwarf](/races/dwarf)     | 15.5.54 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Abrinlin Quietwire](/characters/abrinlin_quietwire) | [Gnome](/races/gnome)     | 13.10.84 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Krimlibronk Grindlocket](/characters/krimlibronk_grindlocket) | [Gnome](/races/gnome)     | 22.8.83 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Cunkack Luckcookie](/characters/cunkack_luckcookie) | [Gnome](/races/gnome)     | 19.5.46 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Glithkic Whistlescheme](/characters/glithkic_whistlescheme) | [Gnome](/races/gnome)     | 9.5.79 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Meenkis Stitchfizzle](/characters/meenkis_stitchfizzle) | [Gnome](/races/gnome)     | 21.6.57 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Grath Deadchewer](/characters/grath_deadchewer) | [Orc](/races/orc)     | 2.3.2 LC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Atheek Fizzlehouse](/characters/atheek_fizzlehouse) | [Gnome](/races/gnome)     | 29.11.50 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Linduzz Overflow](/characters/linduzz_overflow) | [Gnome](/races/gnome)     | 9.11.82 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Hirleck Wrenchstitch](/characters/hirleck_wrenchstitch) | [Gnome](/races/gnome)     | 26.2.58 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Deenbo Lightballoon](/characters/deenbo_lightballoon) | [Gnome](/races/gnome)     | 2.4.94 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Hencurn Tosslepatch](/characters/hencurn_tosslepatch) | [Gnome](/races/gnome)     | 20.4.104 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Darkock Anglebit](/characters/darkock_anglebit) | [Gnome](/races/gnome)     | 26.7.47 BLC |

## Honours

