---
author: "UEFF"
title: "Aurioa SC"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Aurioa Sports-Club |
| Nation | [Jamta](/nations/jamta) |
| City | Aurioa |
| Founded | 30.03.05 LC |
| Field | - |
| Head coach | [Einirod Hrolleifrson](/characters/einirod_hrolleifrson) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Trorgrorg Bristlebraids](/characters/trorgrorg_bristlebraids) | [Dwarf](/races/dwarf)     | 29.10.67 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Steinn Hellhide](/characters/steinn_hellhide) | [Nordic](/races/nordic)     | 5.12.10 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hrahar Short-hammers](/characters/hrahar_short-hammers) | [Nordic](/races/nordic)     | 12.10.4 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Lyting Smith](/characters/lyting_smith) | [Nordic](/races/nordic)     | 1.4.2 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Tindick Flickerspell](/characters/tindick_flickerspell) | [Gnome](/races/gnome)     | 18.1.47 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Gestr Free-versed](/characters/gestr_free-versed) | [Nordic](/races/nordic)     | 20.5.15 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Lodin Torberson](/characters/lodin_torberson) | [Nordic](/races/nordic)     | 12.12.15 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Holti Gusirson](/characters/holti_gusirson) | [Nordic](/races/nordic)     | 16.11.3 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Gautr Hand-sot](/characters/gautr_hand-sot) | [Nordic](/races/nordic)     | 27.7.3 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Orgjof Ingrarokson](/characters/orgjof_ingrarokson) | [Nordic](/races/nordic)     | 13.2.13 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Vract Skywind](/characters/vract_skywind) | [Nordic](/races/nordic)     | 28.7.4 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Fuknan Rockarm](/characters/fuknan_rockarm) | [Dwarf](/races/dwarf)     | 22.7.85 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hertrygg Triple-versed](/characters/hertrygg_triple-versed) | [Nordic](/races/nordic)     | 16.5.9 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hegbjorn Ember-loom](/characters/hegbjorn_ember-loom) | [Nordic](/races/nordic)     | 6.5.11 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlorygg Argahrson](/characters/hlorygg_argahrson) | [Nordic](/races/nordic)     | 11.1.11 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Istlmir Steelbrand](/characters/istlmir_steelbrand) | [Nordic](/races/nordic)     | 9.10.2 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Assuke Hard-nail](/characters/assuke_hard-nail) | [Nordic](/races/nordic)     | 30.5.5 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hisas Ulrerssen](/characters/hisas_ulrerssen) | [Nordic](/races/nordic)     | 18.8.4 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Emza Darkdrum](/characters/emza_darkdrum) | [Orc](/races/orc)     | 17.8.0 LC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hring Anwendson](/characters/hring_anwendson) | [Nordic](/races/nordic)     | 19.11.8 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Avuit Frostbreath](/characters/avuit_frostbreath) | [Nordic](/races/nordic)     | 25.2.6 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Hrorvur Herornsen](/characters/hrorvur_herornsen) | [Nordic](/races/nordic)     | 20.1.15 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Brakvild Bovason](/characters/brakvild_bovason) | [Nordic](/races/nordic)     | 10.12.11 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Irrrim Lonehelm](/characters/irrrim_lonehelm) | [Nordic](/races/nordic)     | 30.1.14 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Heimdall Riversword](/characters/heimdall_riversword) | [Nordic](/races/nordic)     | 24.4.2 BLC |

## Honours
