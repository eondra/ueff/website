---
author: "UEFF"
title: "FC Esjuberg"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Esjuberg |
| Nation | [Jamta](/nations/jamta) |
| City | Esjuberg |
| Founded | 27.12.19 LC |
| Field | - |
| Head coach | [Sniolf Two-lute](/characters/sniolf_two-lute) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Haehjolf Jorgenrsson](/characters/haehjolf_jorgenrsson) | [Nordic](/races/nordic)     | 31.1.14 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Sorran Lightningbrand](/characters/sorran_lightningbrand) | [Nordic](/races/nordic)     | 27.6.15 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Sinding Guttormrson](/characters/sinding_guttormrson) | [Nordic](/races/nordic)     | 27.1.3 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Saesim Fiery-hair](/characters/saesim_fiery-hair) | [Nordic](/races/nordic)     | 30.7.14 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Reisstr Guttormrson](/characters/reisstr_guttormrson) | [Nordic](/races/nordic)     | 2.6.14 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Bronhorgh Dawnmaul](/characters/bronhorgh_dawnmaul) | [Nordic](/races/nordic)     | 12.7.6 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Kastuth Ermon](/characters/kastuth_ermon) | [Orc](/races/orc)     | 16.1.3 LC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Eythiofr Clanrider](/characters/eythiofr_clanrider) | [Nordic](/races/nordic)     | 6.4.12 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Tat Mudbow](/characters/tat_mudbow) | [Dwarf](/races/dwarf)     | 16.1.80 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Dala-alf Round-smith](/characters/dala-alf_round-smith) | [Nordic](/races/nordic)     | 13.7.14 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hosglund Troll-sung](/characters/hosglund_troll-sung) | [Nordic](/races/nordic)     | 27.6.4 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Varin Star-scourge](/characters/varin_star-scourge) | [Nordic](/races/nordic)     | 30.12.4 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Bjntus Strongbringer](/characters/bjntus_strongbringer) | [Nordic](/races/nordic)     | 5.6.14 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Baslrir Lightningslayer](/characters/baslrir_lightningslayer) | [Nordic](/races/nordic)     | 17.6.12 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Sjafnir Engmarikssen](/characters/sjafnir_engmarikssen) | [Nordic](/races/nordic)     | 25.4.14 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Kodran Milk-hammers](/characters/kodran_milk-hammers) | [Nordic](/races/nordic)     | 24.8.5 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Douthack Nu](/characters/douthack_nu) | [Dwarf](/races/dwarf)     | 26.12.69 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hamall Pyreroar](/characters/hamall_pyreroar) | [Nordic](/races/nordic)     | 16.9.13 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Titku Sprycollar](/characters/titku_sprycollar) | [Gnome](/races/gnome)     | 9.3.72 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Rustlam Knottrson](/characters/rustlam_knottrson) | [Nordic](/races/nordic)     | 10.1.10 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Hulgkmar Swiftroar](/characters/hulgkmar_swiftroar) | [Nordic](/races/nordic)     | 10.9.13 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Eldjmoor Ahlornson](/characters/eldjmoor_ahlornson) | [Nordic](/races/nordic)     | 15.11.11 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dalondratir Axebreaker](/characters/dalondratir_axebreaker) | [Dwarf](/races/dwarf)     | 17.8.73 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Asbrand Rockbeard](/characters/asbrand_rockbeard) | [Nordic](/races/nordic)     | 5.11.13 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Thormodr Goreslayer](/characters/thormodr_goreslayer) | [Nordic](/races/nordic)     | 23.8.12 BLC |

## Honours
