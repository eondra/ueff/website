---
author: "UEFF"
title: "Vin Daral Grounders"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vin Daral Grounders |
| Nation | [Vongram](/nations/vongram) |
| City | Vin Daral |
| Founded | 02.09.07 LC |
| Field | - |
| Head coach | [Dutramri Ironshield](/characters/dutramri_ironshield) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Tinfil Ninbis](/characters/tinfil_ninbis) | [Dwarf](/races/dwarf)     | 2.9.72 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Gloringruil Coinchest](/characters/gloringruil_coinchest) | [Dwarf](/races/dwarf)     | 2.10.79 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Oridraer Fierybrand](/characters/oridraer_fierybrand) | [Dwarf](/races/dwarf)     | 6.1.82 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hunthjolf Lonehelm](/characters/hunthjolf_lonehelm) | [Nordic](/races/nordic)     | 22.6.5 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Ruudd Darkshout](/characters/ruudd_darkshout) | [Dwarf](/races/dwarf)     | 24.5.96 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Dalongrerlig Vevrels](/characters/dalongrerlig_vevrels) | [Dwarf](/races/dwarf)     | 3.1.73 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gakgod Hammerfury](/characters/gakgod_hammerfury) | [Dwarf](/races/dwarf)     | 20.10.63 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Uhlmugg Uudresh](/characters/uhlmugg_uudresh) | [Orc](/races/orc)     | 16.7.3 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Skangreag Barbedview](/characters/skangreag_barbedview) | [Dwarf](/races/dwarf)     | 17.11.90 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Loldrout Winterbrow](/characters/loldrout_winterbrow) | [Dwarf](/races/dwarf)     | 23.1.51 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Valthiolfr Alarsson](/characters/valthiolfr_alarsson) | [Nordic](/races/nordic)     | 3.9.11 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hunic Tricktorque](/characters/hunic_tricktorque) | [Gnome](/races/gnome)     | 30.6.97 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Ioldrird Runwimn](/characters/ioldrird_runwimn) | [Dwarf](/races/dwarf)     | 23.10.56 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Danen Somvos](/characters/danen_somvos) | [Dwarf](/races/dwarf)     | 5.5.94 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Gosrud Shatterback](/characters/gosrud_shatterback) | [Dwarf](/races/dwarf)     | 2.5.72 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Kakrer Kragbranch](/characters/kakrer_kragbranch) | [Dwarf](/races/dwarf)     | 17.4.83 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Eeckak Battlelaugh](/characters/eeckak_battlelaugh) | [Gnome](/races/gnome)     | 24.2.79 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Gadd Bhan](/characters/gadd_bhan) | [Dwarf](/races/dwarf)     | 11.3.77 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Cobruc Broadshield](/characters/cobruc_broadshield) | [Dwarf](/races/dwarf)     | 28.3.83 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Itkizz Fuzzyspinner](/characters/itkizz_fuzzyspinner) | [Gnome](/races/gnome)     | 12.10.59 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Raerhardt Ember-smith](/characters/raerhardt_ember-smith) | [Nordic](/races/nordic)     | 19.5.9 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Razron Narsadd](/characters/razron_narsadd) | [Dwarf](/races/dwarf)     | 12.5.85 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Snastread Sralgwack](/characters/snastread_sralgwack) | [Dwarf](/races/dwarf)     | 1.1.91 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Drolbuls Bumbum](/characters/drolbuls_bumbum) | [Dwarf](/races/dwarf)     | 17.10.88 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Strossin Stonegrip](/characters/strossin_stonegrip) | [Dwarf](/races/dwarf)     | 5.1.63 BLC |

## Honours

