---
author: "UEFF"
title: "United Dim Garom"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | United Dim Garom |
| Nation | [Vongram](/nations/vongram) |
| City | Dim Garom |
| Founded | 22.12.10 LC |
| Field | - |
| Head coach | [Dukdeg Tekkoth](/characters/dukdeg_tekkoth) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Thavin Gatnam](/characters/thavin_gatnam) | [Dwarf](/races/dwarf)     | 24.5.94 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Bugrael Solidstride](/characters/bugrael_solidstride) | [Dwarf](/races/dwarf)     | 26.6.85 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Stroratum Leathershield](/characters/stroratum_leathershield) | [Dwarf](/races/dwarf)     | 25.8.50 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Strad Sror](/characters/strad_sror) | [Dwarf](/races/dwarf)     | 7.9.66 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Vaghor Hammerchest](/characters/vaghor_hammerchest) | [Dwarf](/races/dwarf)     | 16.6.93 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Feknud Keenbrow](/characters/feknud_keenbrow) | [Dwarf](/races/dwarf)     | 20.6.100 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Zoskac Coinbuckle](/characters/zoskac_coinbuckle) | [Dwarf](/races/dwarf)     | 25.12.75 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bozrec Oreminer](/characters/bozrec_oreminer) | [Dwarf](/races/dwarf)     | 27.10.74 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Dufnic Treasurebuster](/characters/dufnic_treasurebuster) | [Dwarf](/races/dwarf)     | 28.5.79 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gurbed Kybnamn](/characters/gurbed_kybnamn) | [Dwarf](/races/dwarf)     | 21.8.78 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Uls Tingir](/characters/uls_tingir) | [Dwarf](/races/dwarf)     | 27.3.88 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Kurgin Throd](/characters/kurgin_throd) | [Dwarf](/races/dwarf)     | 18.5.85 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Thekgrom Axeblade](/characters/thekgrom_axeblade) | [Dwarf](/races/dwarf)     | 9.1.88 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Roskac Leadbraids](/characters/roskac_leadbraids) | [Dwarf](/races/dwarf)     | 15.11.93 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Borgek Warmgranite](/characters/borgek_warmgranite) | [Dwarf](/races/dwarf)     | 24.3.99 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Delbrick Goldhair](/characters/delbrick_goldhair) | [Dwarf](/races/dwarf)     | 24.2.93 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Dagumoor War-mage](/characters/dagumoor_war-mage) | [Nordic](/races/nordic)     | 19.9.6 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Hagg Barrenscream](/characters/hagg_barrenscream) | [Orc](/races/orc)     | 5.7.1 LC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Tustear Shatterback](/characters/tustear_shatterback) | [Dwarf](/races/dwarf)     | 1.9.73 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Dykkiodd Flatshot](/characters/dykkiodd_flatshot) | [Dwarf](/races/dwarf)     | 25.5.71 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Jurguls Axechest](/characters/jurguls_axechest) | [Dwarf](/races/dwarf)     | 7.2.81 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Vubrud Treasurejaw](/characters/vubrud_treasurejaw) | [Dwarf](/races/dwarf)     | 2.1.56 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dazzomli Bronzearm](/characters/dazzomli_bronzearm) | [Dwarf](/races/dwarf)     | 19.10.61 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Celbon Dersk](/characters/celbon_dersk) | [Dwarf](/races/dwarf)     | 11.12.86 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Kakrer Bloodthane](/characters/kakrer_bloodthane) | [Dwarf](/races/dwarf)     | 1.6.92 BLC |

## Honours

