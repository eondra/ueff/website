---
author: "UEFF"
title: "SC Kyonore"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Kyonore |
| Nation | [Yllin](/nations/yllin) |
| City | Kyonore |
| Founded | 31.1.06 LC |
| Field | - |
| Head coach | [Jaonos Truesong](/characters/jaonos_truesong) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Tedor Ravaxidor](/characters/tedor_ravaxidor) | [Elf](/races/elf)     | 14.9.170 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Sanev Ehallmu](/characters/sanev_ehallmu) | [Elf](/races/elf)     | 5.8.160 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Olvass Thunderbranch](/characters/olvass_thunderbranch) | [Elf](/races/elf)     | 16.4.176 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Theodemar Ravaxalim](/characters/theodemar_ravaxalim) | [Elf](/races/elf)     | 13.2.175 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Irathras Gelai](/characters/irathras_gelai) | [Elf](/races/elf)     | 19.8.178 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Kraclenk Grindfuse](/characters/kraclenk_grindfuse) | [Gnome](/races/gnome)     | 28.6.104 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Olviar Blademane](/characters/olviar_blademane) | [Elf](/races/elf)     | 29.3.164 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Jorildyn Wrannala](/characters/jorildyn_wrannala) | [Elf](/races/elf)     | 9.2.176 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Tathaln Moonstriker](/characters/tathaln_moonstriker) | [Elf](/races/elf)     | 22.10.172 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Iressian Fan](/characters/iressian_fan) | [Elf](/races/elf)     | 1.4.166 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Selaedan Oceanstalker](/characters/selaedan_oceanstalker) | [Elf](/races/elf)     | 16.12.176 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Strodaeck Giantbraid](/characters/strodaeck_giantbraid) | [Dwarf](/races/dwarf)     | 13.9.87 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Vulre Seasword](/characters/vulre_seasword) | [Elf](/races/elf)     | 12.10.174 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Tiosolon Kas](/characters/tiosolon_kas) | [Elf](/races/elf)     | 6.12.161 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Miladearn Nor](/characters/miladearn_nor) | [Elf](/races/elf)     | 4.2.179 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ralnor Ondrum](/characters/ralnor_ondrum) | [Elf](/races/elf)     | 14.7.175 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Eldar Daglostior](/characters/eldar_daglostior) | [Elf](/races/elf)     | 2.3.175 BLC |
| 18 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Saleh Strongcrest](/characters/saleh_strongcrest) | [Elf](/races/elf)     | 1.1.180 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Sydraes Thundertree](/characters/sydraes_thundertree) | [Elf](/races/elf)     | 3.9.160 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Paeral Leafrunner](/characters/paeral_leafrunner) | [Elf](/races/elf)     | 8.11.163 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Alenrenthil Grom](/characters/alenrenthil_grom) | [Elf](/races/elf)     | 17.11.178 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Alred Ler](/characters/alred_ler) | [Elf](/races/elf)     | 28.8.173 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Elean Leafflower](/characters/elean_leafflower) | [Elf](/races/elf)     | 24.6.172 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Nileath Lunahelm](/characters/nileath_lunahelm) | [Elf](/races/elf)     | 22.6.172 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Girgldr Greatblade](/characters/girgldr_greatblade) | [Nordic](/races/nordic)     | 24.12.7 BLC |

## Honours

