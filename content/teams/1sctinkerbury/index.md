---
author: "UEFF"
title: "1. SC Tinkerbury"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | 1. Sports Club Tinkerbury |
| Nation | [Katargo](/nations/katargo) |
| City | Tinkerbury |
| Founded | 19.09.06 LC |
| Field | - |
| Head coach | [Tutlethizz Switchblock](/characters/tutlethizz_switchblock) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Abrinlin Oilphase](/characters/abrinlin_oilphase) | [Gnome](/races/gnome)     | 19.5.49 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Urkink Wheelcrown](/characters/urkink_wheelcrown) | [Gnome](/races/gnome)     | 20.12.103 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Binkis Buzzinpipe](/characters/binkis_buzzinpipe) | [Gnome](/races/gnome)     | 24.2.78 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Imunkli Anglebit](/characters/imunkli_anglebit) | [Gnome](/races/gnome)     | 28.7.104 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Kollir Surtrson](/characters/kollir_surtrson) | [Nordic](/races/nordic)     | 20.5.10 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Futhkic Silverspanner](/characters/futhkic_silverspanner) | [Gnome](/races/gnome)     | 14.1.65 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Klirkos Flickermix](/characters/klirkos_flickermix) | [Gnome](/races/gnome)     | 5.10.84 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kibithi Mechacog](/characters/kibithi_mechacog) | [Gnome](/races/gnome)     | 23.6.70 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Darkock Flickerbrick](/characters/darkock_flickerbrick) | [Gnome](/races/gnome)     | 26.10.49 BLC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Crumzit Forehorn](/characters/crumzit_forehorn) | [Orc](/races/orc)     | 22.10.2 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kletlimoc Quickpickle](/characters/kletlimoc_quickpickle) | [Gnome](/races/gnome)     | 15.3.67 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Bosh Ghikal](/characters/bosh_ghikal) | [Orc](/races/orc)     | 1.6.0 LC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Beetli Heavyscheme](/characters/beetli_heavyscheme) | [Gnome](/races/gnome)     | 3.9.53 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Glitec Wheelcrown](/characters/glitec_wheelcrown) | [Gnome](/races/gnome)     | 23.3.93 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Morkizz Pitchbang](/characters/morkizz_pitchbang) | [Gnome](/races/gnome)     | 25.10.95 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Funeethozz Mintcount](/characters/funeethozz_mintcount) | [Gnome](/races/gnome)     | 1.1.44 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Deenbo Fixcub](/characters/deenbo_fixcub) | [Gnome](/races/gnome)     | 1.8.56 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Itloluk Sparkbit](/characters/itloluk_sparkbit) | [Gnome](/races/gnome)     | 9.4.62 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Kriclick Shinespell](/characters/kriclick_shinespell) | [Gnome](/races/gnome)     | 3.6.44 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Muvorkek Singlebus](/characters/muvorkek_singlebus) | [Gnome](/races/gnome)     | 21.8.83 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Deromoor Stone](/characters/deromoor_stone) | [Nordic](/races/nordic)     | 24.5.2 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Linduzz Swiftcrown](/characters/linduzz_swiftcrown) | [Gnome](/races/gnome)     | 28.10.62 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Lancish Mintguard](/characters/lancish_mintguard) | [Gnome](/races/gnome)     | 18.3.45 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Itkon Anglechin](/characters/itkon_anglechin) | [Gnome](/races/gnome)     | 3.5.84 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Kicis Grimeycrown](/characters/kicis_grimeycrown) | [Gnome](/races/gnome)     | 20.10.79 BLC |

## Honours
