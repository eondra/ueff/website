---
author: "UEFF"
title: "Kinbadur Rocks"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Kinbadur Rocks |
| Nation | [Vongram](/nations/vongram) |
| City | Kinbadur |
| Founded | 31.07.14 LC |
| Field | - |
| Head coach | [Haim Treasurejaw](/characters/haim_treasurejaw) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Thegne Mur](/characters/thegne_mur) | [Orc](/races/orc)     | 30.10.4 LC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Vyls Warmmantle](/characters/vyls_warmmantle) | [Dwarf](/races/dwarf)     | 25.8.64 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Thunderous Mudtoe](/characters/thunderous_mudtoe) | [Dwarf](/races/dwarf)     | 17.7.88 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Wefnum Bloodthane](/characters/wefnum_bloodthane) | [Dwarf](/races/dwarf)     | 5.4.52 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Lost Stormgrip](/characters/lost_stormgrip) | [Dwarf](/races/dwarf)     | 31.5.65 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Lurgrus Khas](/characters/lurgrus_khas) | [Dwarf](/races/dwarf)     | 21.3.93 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Runder Diggir](/characters/runder_diggir) | [Dwarf](/races/dwarf)     | 23.2.68 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Dor Runwimn](/characters/dor_runwimn) | [Dwarf](/races/dwarf)     | 18.12.70 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Helerlug Greatchest](/characters/helerlug_greatchest) | [Dwarf](/races/dwarf)     | 12.1.95 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Thrafid Guard](/characters/thrafid_guard) | [Dwarf](/races/dwarf)     | 23.6.50 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gadd Gearshift](/characters/gadd_gearshift) | [Dwarf](/races/dwarf)     | 7.12.94 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Dur Gobbos](/characters/dur_gobbos) | [Dwarf](/races/dwarf)     | 20.5.60 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Sneaky Gravelarm](/characters/sneaky_gravelarm) | [Dwarf](/races/dwarf)     | 30.8.87 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Ruudd Flaskshield](/characters/ruudd_flaskshield) | [Dwarf](/races/dwarf)     | 2.8.72 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Woknem Blacksmith](/characters/woknem_blacksmith) | [Dwarf](/races/dwarf)     | 21.3.75 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Glasdrut Belges](/characters/glasdrut_belges) | [Dwarf](/races/dwarf)     | 27.1.61 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Dwoldrud Kubhog](/characters/dwoldrud_kubhog) | [Dwarf](/races/dwarf)     | 5.10.93 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Zersuc Warback](/characters/zersuc_warback) | [Dwarf](/races/dwarf)     | 28.1.60 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Strul Sahudd](/characters/strul_sahudd) | [Dwarf](/races/dwarf)     | 4.4.58 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Sandrul Graybane](/characters/sandrul_graybane) | [Dwarf](/races/dwarf)     | 3.2.74 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Dagumoor Earthsong](/characters/dagumoor_earthsong) | [Nordic](/races/nordic)     | 10.10.7 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Silgor Goldhair](/characters/silgor_goldhair) | [Dwarf](/races/dwarf)     | 2.10.57 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Zubrem Retlen](/characters/zubrem_retlen) | [Dwarf](/races/dwarf)     | 12.3.77 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Zabmiodd Coalspine](/characters/zabmiodd_coalspine) | [Dwarf](/races/dwarf)     | 17.12.98 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gudfastr Svartkin](/characters/gudfastr_svartkin) | [Nordic](/races/nordic)     | 14.5.7 BLC |

## Honours

