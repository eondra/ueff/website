---
author: "UEFF"
title: "FC Kinbadur"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Clan Kinbadur |
| Nation | [Vongram](/nations/vongram) |
| City | Kinbadur |
| Founded | 24.02.11 LC |
| Field | - |
| Head coach | [Bengrorlum Grandgem](/characters/bengrorlum_grandgem) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Baze Arnoth](/characters/baze_arnoth) | [Orc](/races/orc)     | 29.9.4 LC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Daretgraeth Craftsman](/characters/daretgraeth_craftsman) | [Dwarf](/races/dwarf)     | 9.11.72 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Kun Patrol](/characters/kun_patrol) | [Dwarf](/races/dwarf)     | 18.2.97 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Throlgoic Magmagrog](/characters/throlgoic_magmagrog) | [Dwarf](/races/dwarf)     | 12.12.78 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Hestarlig Humblemaul](/characters/hestarlig_humblemaul) | [Dwarf](/races/dwarf)     | 12.10.86 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Elfeam Broadshield](/characters/elfeam_broadshield) | [Dwarf](/races/dwarf)     | 31.5.66 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Dod Alebuster](/characters/dod_alebuster) | [Dwarf](/races/dwarf)     | 29.6.71 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Ozur Longhide](/characters/ozur_longhide) | [Nordic](/races/nordic)     | 12.10.10 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Veteran Greatchest](/characters/veteran_greatchest) | [Dwarf](/races/dwarf)     | 24.8.94 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Barildrack Orebender](/characters/barildrack_orebender) | [Dwarf](/races/dwarf)     | 2.12.67 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Skosdread Somgwes](/characters/skosdread_somgwes) | [Dwarf](/races/dwarf)     | 25.1.57 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Bozrec Bocwird](/characters/bozrec_bocwird) | [Dwarf](/races/dwarf)     | 29.4.94 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Bulgruki Steelsteam](/characters/bulgruki_steelsteam) | [Dwarf](/races/dwarf)     | 1.12.87 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Nadrec Tribals](/characters/nadrec_tribals) | [Dwarf](/races/dwarf)     | 18.5.72 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Wekgem Nebbem](/characters/wekgem_nebbem) | [Dwarf](/races/dwarf)     | 1.9.70 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Skagnaes Stoneroar](/characters/skagnaes_stoneroar) | [Dwarf](/races/dwarf)     | 8.3.73 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Evom Cragcrusher](/characters/evom_cragcrusher) | [Dwarf](/races/dwarf)     | 18.4.57 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Nuratmaes Wright](/characters/nuratmaes_wright) | [Dwarf](/races/dwarf)     | 27.8.56 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Skadrim Darkridge](/characters/skadrim_darkridge) | [Dwarf](/races/dwarf)     | 16.3.64 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Dapper Flintsteam](/characters/dapper_flintsteam) | [Dwarf](/races/dwarf)     | 29.1.98 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Ergorm Bronzelash](/characters/ergorm_bronzelash) | [Orc](/races/orc)     | 28.3.4 LC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Khuggom Grunngvys](/characters/khuggom_grunngvys) | [Dwarf](/races/dwarf)     | 9.5.58 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Urteld Kul](/characters/urteld_kul) | [Orc](/races/orc)     | 1.6.2 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Broutik Gomn](/characters/broutik_gomn) | [Dwarf](/races/dwarf)     | 17.1.79 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Votged Anvilbuckle](/characters/votged_anvilbuckle) | [Dwarf](/races/dwarf)     | 12.12.83 BLC |

## Honours

