---
author: "UEFF"
title: "FC Kopa"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Kopareykir |
| Nation | [Jamta](/nations/jamta) |
| City | Kopareykir |
| Founded | 05.09.15 LC |
| Field | - |
| Head coach | [Halufi Hard-nail](/characters/halufi_hard-nail) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Hridarik Mjisssen](/characters/hridarik_mjisssen) | [Nordic](/races/nordic)     | 28.2.9 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Orlygr Geirleifrson](/characters/orlygr_geirleifrson) | [Nordic](/races/nordic)     | 24.11.4 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Einar Corundum-toe](/characters/einar_corundum-toe) | [Nordic](/races/nordic)     | 8.7.9 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Metinir Sarervonsen](/characters/metinir_sarervonsen) | [Nordic](/races/nordic)     | 15.10.2 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Kvistr Kylanson](/characters/kvistr_kylanson) | [Nordic](/races/nordic)     | 18.7.9 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Soxolfr Dawnmaul](/characters/soxolfr_dawnmaul) | [Nordic](/races/nordic)     | 7.6.14 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Oddleifr Stonebelly](/characters/oddleifr_stonebelly) | [Nordic](/races/nordic)     | 21.10.14 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hamundr Ember-thief](/characters/hamundr_ember-thief) | [Nordic](/races/nordic)     | 26.9.14 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Akaoknolf Kolson](/characters/akaoknolf_kolson) | [Nordic](/races/nordic)     | 21.4.11 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Vermundr Short-maiden](/characters/vermundr_short-maiden) | [Nordic](/races/nordic)     | 2.2.12 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gamod Jolvos](/characters/gamod_jolvos) | [Dwarf](/races/dwarf)     | 6.7.60 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Soxolfr Mojarkesson](/characters/soxolfr_mojarkesson) | [Nordic](/races/nordic)     | 3.2.9 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hjalmar Hnakakin](/characters/hjalmar_hnakakin) | [Nordic](/races/nordic)     | 28.2.7 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Vignis Bloodmouth](/characters/vignis_bloodmouth) | [Nordic](/races/nordic)     | 12.8.14 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Kvistr Thorolfkin](/characters/kvistr_thorolfkin) | [Nordic](/races/nordic)     | 6.4.10 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Loatnjolf Haukrson](/characters/loatnjolf_haukrson) | [Nordic](/races/nordic)     | 4.11.4 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hastein Fjorinssen](/characters/hastein_fjorinssen) | [Nordic](/races/nordic)     | 18.10.3 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hunthjolf Arnoddrson](/characters/hunthjolf_arnoddrson) | [Nordic](/races/nordic)     | 18.7.5 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Igmudic Ulfarkin](/characters/igmudic_ulfarkin) | [Nordic](/races/nordic)     | 24.4.3 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Nelkrad Warshield](/characters/nelkrad_warshield) | [Nordic](/races/nordic)     | 29.8.12 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Sur Viga-glumson](/characters/sur_viga-glumson) | [Nordic](/races/nordic)     | 11.3.8 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Hakon Heavyshield](/characters/hakon_heavyshield) | [Nordic](/races/nordic)     | 14.1.2 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Risssing Heyjang-bjornnson](/characters/risssing_heyjang-bjornnson) | [Nordic](/races/nordic)     | 26.9.15 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Bergthorr Flat-skinner](/characters/bergthorr_flat-skinner) | [Nordic](/races/nordic)     | 16.7.10 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gutng Erensson](/characters/gutng_erensson) | [Nordic](/races/nordic)     | 2.12.13 BLC |

## Honours
