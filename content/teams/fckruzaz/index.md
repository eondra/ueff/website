---
author: "UEFF"
title: "FC Kruzaz"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Kruzaz |
| Nation | [Urotha](/nations/urotha) |
| City | Kruzaz |
| Founded | 11.08.09 LC |
| Field | - |
| Head coach | [Visgag Vhak](/characters/visgag_vhak) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Izkat Dihd](/characters/izkat_dihd) | [Orc](/races/orc)     | 15.3.1 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Crik Guun](/characters/crik_guun) | [Orc](/races/orc)     | 23.11.4 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Krume Zom](/characters/krume_zom) | [Orc](/races/orc)     | 13.3.3 LC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Wefnum Shatterback](/characters/wefnum_shatterback) | [Dwarf](/races/dwarf)     | 18.1.82 BLC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Grum Kehd](/characters/grum_kehd) | [Orc](/races/orc)     | 1.1.0 LC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Lithkonk Sharppipe](/characters/lithkonk_sharppipe) | [Gnome](/races/gnome)     | 4.12.80 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Grahd Stoutsplitter](/characters/grahd_stoutsplitter) | [Orc](/races/orc)     | 24.8.4 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Bornorg Bik](/characters/bornorg_bik) | [Orc](/races/orc)     | 31.10.2 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Mengahd Mush](/characters/mengahd_mush) | [Orc](/races/orc)     | 11.11.4 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Malgelzu Eagertale](/characters/malgelzu_eagertale) | [Orc](/races/orc)     | 25.8.2 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Zach Foreflesh](/characters/zach_foreflesh) | [Orc](/races/orc)     | 23.10.2 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Tholturl Grimripper](/characters/tholturl_grimripper) | [Orc](/races/orc)     | 13.8.3 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Krehn Blackflame](/characters/krehn_blackflame) | [Orc](/races/orc)     | 24.6.4 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Grurnur Eskuum](/characters/grurnur_eskuum) | [Orc](/races/orc)     | 23.8.4 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Drek Dan](/characters/drek_dan) | [Orc](/races/orc)     | 4.2.2 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Arnem Frostmask](/characters/arnem_frostmask) | [Orc](/races/orc)     | 17.9.2 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzan Brightwish](/characters/kzan_brightwish) | [Orc](/races/orc)     | 29.7.1 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Uligdald Laughingmarch](/characters/uligdald_laughingmarch) | [Orc](/races/orc)     | 3.6.1 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Brard Darkrage](/characters/brard_darkrage) | [Orc](/races/orc)     | 11.12.2 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Vehk Saursword](/characters/vehk_saursword) | [Orc](/races/orc)     | 24.7.1 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drelkzilkk Kram](/characters/drelkzilkk_kram) | [Orc](/races/orc)     | 29.8.1 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Veld Bruunkon](/characters/veld_bruunkon) | [Orc](/races/orc)     | 9.7.2 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Grezvokel Grimforce](/characters/grezvokel_grimforce) | [Orc](/races/orc)     | 8.2.3 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Pridon Tit](/characters/pridon_tit) | [Orc](/races/orc)     | 23.3.1 LC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Prelkk Axehammer](/characters/prelkk_axehammer) | [Orc](/races/orc)     | 24.1.4 LC |

## Honours
