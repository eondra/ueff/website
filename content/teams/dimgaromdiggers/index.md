---
author: "UEFF"
title: "Dim Garom Diggers"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Dim Garom Diggers |
| Nation | [Vongram](/nations/vongram) |
| City | Dim Garom |
| Founded | 19.12.09 LC |
| Field | - |
| Head coach | [Loznerlug Ghuls](/characters/loznerlug_ghuls) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Cekmam Khugnath](/characters/cekmam_khugnath) | [Dwarf](/races/dwarf)     | 6.6.54 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Graadd Flintsteam](/characters/graadd_flintsteam) | [Dwarf](/races/dwarf)     | 12.12.82 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Gurgeg Bloodbrow](/characters/gurgeg_bloodbrow) | [Dwarf](/races/dwarf)     | 27.5.72 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Hylmec Flintguard](/characters/hylmec_flintguard) | [Dwarf](/races/dwarf)     | 19.8.87 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Darg Twilightstone](/characters/darg_twilightstone) | [Dwarf](/races/dwarf)     | 1.5.77 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Thufrok Blastminer](/characters/thufrok_blastminer) | [Dwarf](/races/dwarf)     | 23.9.57 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Acmurd Leatherborn](/characters/acmurd_leatherborn) | [Dwarf](/races/dwarf)     | 16.10.80 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Rakrulim Softsnow](/characters/rakrulim_softsnow) | [Dwarf](/races/dwarf)     | 23.4.96 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gamdeth Twilightshield](/characters/gamdeth_twilightshield) | [Dwarf](/races/dwarf)     | 19.3.93 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gragroc Deeparm](/characters/gragroc_deeparm) | [Dwarf](/races/dwarf)     | 17.12.60 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Ziknec Ashheart](/characters/ziknec_ashheart) | [Dwarf](/races/dwarf)     | 13.12.98 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Reivut Jadeshaper](/characters/reivut_jadeshaper) | [Dwarf](/races/dwarf)     | 1.2.86 BLC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Uulgroor Assailant](/characters/uulgroor_assailant) | [Dwarf](/races/dwarf)     | 24.2.59 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Magnuil Srorgwak](/characters/magnuil_srorgwak) | [Dwarf](/races/dwarf)     | 11.4.95 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Grozzumli Stronthild](/characters/grozzumli_stronthild) | [Dwarf](/races/dwarf)     | 13.3.60 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Guud Gravelarm](/characters/guud_gravelarm) | [Dwarf](/races/dwarf)     | 4.10.94 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Druc Cinderstride](/characters/druc_cinderstride) | [Dwarf](/races/dwarf)     | 8.5.74 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Ak Jadehelm](/characters/ak_jadehelm) | [Dwarf](/races/dwarf)     | 28.11.85 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Lustruck Gormor](/characters/lustruck_gormor) | [Dwarf](/races/dwarf)     | 21.4.83 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Skargrock Darkridge](/characters/skargrock_darkridge) | [Dwarf](/races/dwarf)     | 28.6.93 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dapper Rinbin](/characters/dapper_rinbin) | [Dwarf](/races/dwarf)     | 6.2.80 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Iorgruurg Blazecrest](/characters/iorgruurg_blazecrest) | [Dwarf](/races/dwarf)     | 17.7.93 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Braddumi Stonestrike](/characters/braddumi_stonestrike) | [Dwarf](/races/dwarf)     | 11.1.91 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Gals Shieldsword](/characters/gals_shieldsword) | [Dwarf](/races/dwarf)     | 24.7.99 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Siluli Gearshift](/characters/siluli_gearshift) | [Dwarf](/races/dwarf)     | 25.7.53 BLC |

## Honours

