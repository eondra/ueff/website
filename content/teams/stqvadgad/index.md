---
author: "UEFF"
title: "ST Qvadgad"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Tribe Qvadgad |
| Nation | [Urotha](/nations/urotha) |
| City | Qvadgad |
| Founded | 06.02.12 LC |
| Field | - |
| Head coach | [Konk Crok](/characters/konk_crok) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Trarg Ganrosh](/characters/trarg_ganrosh) | [Orc](/races/orc)     | 9.11.0 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Fosguhn Bloodguard](/characters/fosguhn_bloodguard) | [Orc](/races/orc)     | 23.6.0 LC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Svenvald Heavycleaver](/characters/svenvald_heavycleaver) | [Nordic](/races/nordic)     | 9.3.14 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Gromabunk Cravenpride](/characters/gromabunk_cravenpride) | [Orc](/races/orc)     | 18.6.1 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Ozovgem Thuuzom](/characters/ozovgem_thuuzom) | [Orc](/races/orc)     | 3.5.2 LC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Orthgarth Deathforge](/characters/orthgarth_deathforge) | [Nordic](/races/nordic)     | 27.1.15 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Krakuhlgech Tum](/characters/krakuhlgech_tum) | [Orc](/races/orc)     | 13.2.0 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Krintem Kormus](/characters/krintem_kormus) | [Orc](/races/orc)     | 5.9.4 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Nozkalk Silentblood](/characters/nozkalk_silentblood) | [Orc](/races/orc)     | 27.11.1 LC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Thor Darkdrum](/characters/thor_darkdrum) | [Orc](/races/orc)     | 25.5.1 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Prard Dreamflesh](/characters/prard_dreamflesh) | [Orc](/races/orc)     | 19.8.2 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Mudust Clearbones](/characters/mudust_clearbones) | [Orc](/races/orc)     | 17.2.2 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Bold Ragebleeder](/characters/bold_ragebleeder) | [Orc](/races/orc)     | 18.12.4 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Kozihlnu Zut](/characters/kozihlnu_zut) | [Orc](/races/orc)     | 27.10.3 LC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Tithurn Dazzlephase](/characters/tithurn_dazzlephase) | [Gnome](/races/gnome)     | 22.4.51 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Gish Kefnihn](/characters/gish_kefnihn) | [Orc](/races/orc)     | 21.8.0 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Grorvavmamm Tos](/characters/grorvavmamm_tos) | [Orc](/races/orc)     | 21.6.2 LC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Svaramor Ironwound](/characters/svaramor_ironwound) | [Nordic](/races/nordic)     | 12.12.12 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Prikde Stonewind](/characters/prikde_stonewind) | [Orc](/races/orc)     | 14.4.2 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Krorngon-digg Vuut](/characters/krorngon-digg_vuut) | [Orc](/races/orc)     | 12.4.1 LC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Nedman Battlebrewer](/characters/nedman_battlebrewer) | [Dwarf](/races/dwarf)     | 26.9.87 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrurk Bloodspite](/characters/vrurk_bloodspite) | [Orc](/races/orc)     | 20.11.2 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Rokk Dirnad](/characters/rokk_dirnad) | [Orc](/races/orc)     | 27.12.0 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Trenavtu Coldflesh](/characters/trenavtu_coldflesh) | [Orc](/races/orc)     | 4.1.2 LC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Brukk Quickbrass](/characters/brukk_quickbrass) | [Orc](/races/orc)     | 7.8.2 LC |

## Honours
