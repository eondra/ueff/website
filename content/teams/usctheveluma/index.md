---
author: "UEFF"
title: "USC Theveluma"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | United Sports Society Theveluma |
| Nation | [Yllin](/nations/yllin) |
| City | Theveluma |
| Founded | 14.07.05 LC |
| Field | - |
| Head coach | [Mlartlar Zhuh](/characters/mlartlar_zhuh) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Tuhoss Jem](/characters/tuhoss_jem) | [Elf](/races/elf)     | 17.10.168 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [E-lean Mistcrest](/characters/e-lean_mistcrest) | [Elf](/races/elf)     | 11.11.161 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Skjldr Firerider](/characters/skjldr_firerider) | [Nordic](/races/nordic)     | 24.9.10 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Casrardosol Wildwind](/characters/casrardosol_wildwind) | [Elf](/races/elf)     | 21.8.177 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Incis Coilchin](/characters/incis_coilchin) | [Gnome](/races/gnome)     | 21.8.81 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Benrius Thir](/characters/benrius_thir) | [Elf](/races/elf)     | 8.2.161 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Thonvadram Irresal](/characters/thonvadram_irresal) | [Elf](/races/elf)     | 13.11.167 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Ninrom Grisholdos](/characters/ninrom_grisholdos) | [Elf](/races/elf)     | 24.7.179 BLC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Mohloz Mush](/characters/mohloz_mush) | [Orc](/races/orc)     | 22.1.1 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Halthir Grenan](/characters/halthir_grenan) | [Elf](/races/elf)     | 31.3.171 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Cimvojaeglal Shoh](/characters/cimvojaeglal_shoh) | [Elf](/races/elf)     | 26.8.174 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Ivasaar Umefir](/characters/ivasaar_umefir) | [Elf](/races/elf)     | 31.7.160 BLC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Bhazzulim Leadmail](/characters/bhazzulim_leadmail) | [Dwarf](/races/dwarf)     | 4.7.74 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Tulum Gudilvaer](/characters/tulum_gudilvaer) | [Elf](/races/elf)     | 19.6.170 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Keggun Whitfinger](/characters/keggun_whitfinger) | [Dwarf](/races/dwarf)     | 11.7.71 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Dujes Spiritbranch](/characters/dujes_spiritbranch) | [Elf](/races/elf)     | 12.1.168 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ovrirer Ambergrove](/characters/ovrirer_ambergrove) | [Elf](/races/elf)     | 30.9.178 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Zubrem Turstot](/characters/zubrem_turstot) | [Dwarf](/races/dwarf)     | 28.1.52 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Omguk Erkuud](/characters/omguk_erkuud) | [Orc](/races/orc)     | 19.2.0 LC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Tum Heimyar](/characters/tum_heimyar) | [Elf](/races/elf)     | 12.5.178 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kugom Dhilkaahd](/characters/kugom_dhilkaahd) | [Orc](/races/orc)     | 6.5.3 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Brolk Thuuzin](/characters/brolk_thuuzin) | [Orc](/races/orc)     | 5.8.2 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Grath Graveblade](/characters/grath_graveblade) | [Orc](/races/orc)     | 1.11.3 LC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Haldamyr Yidilluh](/characters/haldamyr_yidilluh) | [Elf](/races/elf)     | 16.5.170 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Jigorn Bluecloud](/characters/jigorn_bluecloud) | [Elf](/races/elf)     | 19.3.172 BLC |

## Honours
