---
author: "UEFF"
title: "FA Tinkerbury"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Aces Tinkerbury |
| Nation | [Katargo](/nations/katargo) |
| City | Tinkerbury |
| Founded | 08.06.19 LC |
| Field | - |
| Head coach | [Krirleek Pumpbonk](/characters/krirleek_pumpbonk) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlun Bizzbeam](/characters/itlun_bizzbeam) | [Gnome](/races/gnome)     | 23.2.102 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Gatkiklirn Sparklebrass](/characters/gatkiklirn_sparklebrass) | [Gnome](/races/gnome)     | 20.8.66 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Tenlizz Wobbledock](/characters/tenlizz_wobbledock) | [Gnome](/races/gnome)     | 28.1.59 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Utka Grindcoil](/characters/utka_grindcoil) | [Gnome](/races/gnome)     | 18.5.76 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Peetki Grimeyclue](/characters/peetki_grimeyclue) | [Gnome](/races/gnome)     | 23.11.70 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Gocalazz Strikespell](/characters/gocalazz_strikespell) | [Gnome](/races/gnome)     | 18.8.45 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Binkis Wrenchgrinder](/characters/binkis_wrenchgrinder) | [Gnome](/races/gnome)     | 10.4.64 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Elkos Grimeychin](/characters/elkos_grimeychin) | [Gnome](/races/gnome)     | 26.7.70 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Irkok Trickyguard](/characters/irkok_trickyguard) | [Gnome](/races/gnome)     | 5.8.68 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Umabe Dualbrake](/characters/umabe_dualbrake) | [Gnome](/races/gnome)     | 19.6.41 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bindizz Berryspanner](/characters/bindizz_berryspanner) | [Gnome](/races/gnome)     | 19.5.86 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hetan Sparkfizz](/characters/hetan_sparkfizz) | [Gnome](/races/gnome)     | 3.7.84 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Thorer Aeskelson](/characters/thorer_aeskelson) | [Nordic](/races/nordic)     | 5.5.4 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Gankleerush Dazzlebell](/characters/gankleerush_dazzlebell) | [Gnome](/races/gnome)     | 7.11.79 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Pilkin Mintcount](/characters/pilkin_mintcount) | [Gnome](/races/gnome)     | 22.3.58 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Fiwilleck Strikebrake](/characters/fiwilleck_strikebrake) | [Gnome](/races/gnome)     | 7.1.41 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Piclis Bizzphase](/characters/piclis_bizzphase) | [Gnome](/races/gnome)     | 31.12.100 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Kulluk Spannerclick](/characters/kulluk_spannerclick) | [Gnome](/races/gnome)     | 20.5.86 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Burdo Mogmid](/characters/burdo_mogmid) | [Orc](/races/orc)     | 27.5.2 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Ceetlos Fuzzgauge](/characters/ceetlos_fuzzgauge) | [Gnome](/races/gnome)     | 11.12.85 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Ofocleen Heavyscheme](/characters/ofocleen_heavyscheme) | [Gnome](/races/gnome)     | 2.7.49 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnonbish Wobbleheart](/characters/gnonbish_wobbleheart) | [Gnome](/races/gnome)     | 31.12.81 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Urkink Luckspan](/characters/urkink_luckspan) | [Gnome](/races/gnome)     | 11.10.94 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Pisodok Overflow](/characters/pisodok_overflow) | [Gnome](/races/gnome)     | 6.5.86 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Finethizz Sprytwist](/characters/finethizz_sprytwist) | [Gnome](/races/gnome)     | 8.10.85 BLC |

## Honours

