---
author: "UEFF"
title: "Cogdale Scraps"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Cogdale Scraps |
| Nation | [Katargo](/nations/katargo) |
| City | Cogdale |
| Founded | 02.02.08 LC |
| Field | - |
| Head coach | [Milluflis Anglenozzle](/characters/milluflis_anglenozzle) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Gniles Cogsignal](/characters/gniles_cogsignal) | [Gnome](/races/gnome)     | 29.3.81 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnenlick Grimeyclue](/characters/gnenlick_grimeyclue) | [Gnome](/races/gnome)     | 13.11.64 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Cemki Tinkhouse](/characters/cemki_tinkhouse) | [Gnome](/races/gnome)     | 8.4.79 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Hathablarn Mintfield](/characters/hathablarn_mintfield) | [Gnome](/races/gnome)     | 9.1.81 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Dollik Sprytwist](/characters/dollik_sprytwist) | [Gnome](/races/gnome)     | 27.8.75 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Kletlimoc Copperspark](/characters/kletlimoc_copperspark) | [Gnome](/races/gnome)     | 31.7.63 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Tenlizz Fuzzyspinner](/characters/tenlizz_fuzzyspinner) | [Gnome](/races/gnome)     | 3.2.77 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Kalkazz Wheelpipe](/characters/kalkazz_wheelpipe) | [Gnome](/races/gnome)     | 30.3.105 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Linlu Mechacheek](/characters/linlu_mechacheek) | [Gnome](/races/gnome)     | 11.6.44 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Pendotizz Heavyspell](/characters/pendotizz_heavyspell) | [Gnome](/races/gnome)     | 2.12.69 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Pimack Wheelclick](/characters/pimack_wheelclick) | [Gnome](/races/gnome)     | 11.9.102 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Finethizz Buzzinchart](/characters/finethizz_buzzinchart) | [Gnome](/races/gnome)     | 7.1.49 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Duncosh Fizzledock](/characters/duncosh_fizzledock) | [Gnome](/races/gnome)     | 14.4.90 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Inciblick Togglescheme](/characters/inciblick_togglescheme) | [Gnome](/races/gnome)     | 26.9.40 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Kulluk Stitchgrinder](/characters/kulluk_stitchgrinder) | [Gnome](/races/gnome)     | 24.11.86 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Pilkin Twistbadge](/characters/pilkin_twistbadge) | [Gnome](/races/gnome)     | 22.11.79 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Piclis Acercollar](/characters/piclis_acercollar) | [Gnome](/races/gnome)     | 29.5.43 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Irkok Castclock](/characters/irkok_castclock) | [Gnome](/races/gnome)     | 9.4.78 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Votgan Cavebrand](/characters/votgan_cavebrand) | [Dwarf](/races/dwarf)     | 10.7.76 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Itlun Berrymix](/characters/itlun_berrymix) | [Gnome](/races/gnome)     | 28.2.71 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Ecko Scratchbrain](/characters/ecko_scratchbrain) | [Gnome](/races/gnome)     | 19.4.57 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Domzi Zahd](/characters/domzi_zahd) | [Orc](/races/orc)     | 15.2.2 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Klilkis Strikebrake](/characters/klilkis_strikebrake) | [Gnome](/races/gnome)     | 9.8.48 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Tinbifi Twistboss](/characters/tinbifi_twistboss) | [Gnome](/races/gnome)     | 20.11.94 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Cunkack Draxletorque](/characters/cunkack_draxletorque) | [Gnome](/races/gnome)     | 5.6.44 BLC |

## Honours

