---
author: "UEFF"
title: "1. FC Gislavik"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | 1. Footorb Club Gislavik |
| Nation | [Jamta](/nations/jamta) |
| City | Gislavik |
| Founded | 03.03.04 LC |
| Field | - |
| Head coach | [Thormodr Fridmundrson](/characters/thormodr_fridmundrson) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Olvir Ulrisversson](/characters/olvir_ulrisversson) | [Nordic](/races/nordic)     | 13.11.12 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Maearke Gunnulvson](/characters/maearke_gunnulvson) | [Nordic](/races/nordic)     | 10.3.14 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Lundthor Fog-lute](/characters/lundthor_fog-lute) | [Nordic](/races/nordic)     | 12.4.14 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hewmk Deadwhisper](/characters/hewmk_deadwhisper) | [Nordic](/races/nordic)     | 27.9.8 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Skalgun Alfifkssen](/characters/skalgun_alfifkssen) | [Nordic](/races/nordic)     | 8.5.12 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Koll Warlock](/characters/koll_warlock) | [Nordic](/races/nordic)     | 22.7.3 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Daguarel Throndrkin](/characters/daguarel_throndrkin) | [Nordic](/races/nordic)     | 13.9.14 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Oxen-thorir Triple-versed](/characters/oxen-thorir_triple-versed) | [Nordic](/races/nordic)     | 4.6.13 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Wedrud Brownhand](/characters/wedrud_brownhand) | [Dwarf](/races/dwarf)     | 15.5.85 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Thrainn Broken-bane](/characters/thrainn_broken-bane) | [Nordic](/races/nordic)     | 23.5.15 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Vebiorn Valgardson](/characters/vebiorn_valgardson) | [Nordic](/races/nordic)     | 19.5.15 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Jayrjar Thrallmaster](/characters/jayrjar_thrallmaster) | [Nordic](/races/nordic)     | 23.2.11 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Stenrmoor Riversword](/characters/stenrmoor_riversword) | [Nordic](/races/nordic)     | 24.9.5 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Arnsmm Triple-versed](/characters/arnsmm_triple-versed) | [Nordic](/races/nordic)     | 9.12.2 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Jeran Frosthide](/characters/jeran_frosthide) | [Nordic](/races/nordic)     | 23.7.3 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hriakr Braggart](/characters/hriakr_braggart) | [Nordic](/races/nordic)     | 4.2.8 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Tulrod Firerider](/characters/tulrod_firerider) | [Nordic](/races/nordic)     | 29.3.6 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Joremar Longpike](/characters/joremar_longpike) | [Nordic](/races/nordic)     | 22.11.8 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Hroalf Oath-nail](/characters/hroalf_oath-nail) | [Nordic](/races/nordic)     | 23.9.3 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Rhofur Haraeldsen](/characters/rhofur_haraeldsen) | [Nordic](/races/nordic)     | 21.3.6 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Ragslod Firehelm](/characters/ragslod_firehelm) | [Nordic](/races/nordic)     | 7.8.9 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Jeran Bold-seeker](/characters/jeran_bold-seeker) | [Nordic](/races/nordic)     | 3.3.5 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Borekk Ghik](/characters/borekk_ghik) | [Orc](/races/orc)     | 11.1.1 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Virvatr Round-tooth](/characters/virvatr_round-tooth) | [Nordic](/races/nordic)     | 5.7.6 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Freki Cabbage-skinner](/characters/freki_cabbage-skinner) | [Nordic](/races/nordic)     | 15.8.11 BLC |

## Honours
