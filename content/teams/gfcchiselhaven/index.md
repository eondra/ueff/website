---
author: "UEFF"
title: "GFC Chiselhaven"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Gnomish Footorb Club Chiselhaven |
| Nation | [Katargo](/nations/katargo) |
| City | Chiselhaven |
| Founded | 28.12.18 LC |
| Field | - |
| Head coach | [Kriclick Heavycog](/characters/kriclick_heavycog) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Glitec Luckbadge](/characters/glitec_luckbadge) | [Gnome](/races/gnome)     | 14.1.69 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Ickiklas Dualblast](/characters/ickiklas_dualblast) | [Gnome](/races/gnome)     | 21.2.88 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Thosdruth Vuldrag](/characters/thosdruth_vuldrag) | [Dwarf](/races/dwarf)     | 25.6.74 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Follic Fizzlepipe](/characters/follic_fizzlepipe) | [Gnome](/races/gnome)     | 22.9.74 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Glivuklin Gripneedle](/characters/glivuklin_gripneedle) | [Gnome](/races/gnome)     | 25.9.52 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Cemkin Oilspell](/characters/cemkin_oilspell) | [Gnome](/races/gnome)     | 25.4.66 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Anicki Flickercoil](/characters/anicki_flickercoil) | [Gnome](/races/gnome)     | 16.3.48 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Methin Heavyspell](/characters/methin_heavyspell) | [Gnome](/races/gnome)     | 17.7.49 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Fankikla Heavyscheme](/characters/fankikla_heavyscheme) | [Gnome](/races/gnome)     | 1.10.104 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Puti Draxledata](/characters/puti_draxledata) | [Gnome](/races/gnome)     | 25.12.47 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Etkec Grindcoil](/characters/etkec_grindcoil) | [Gnome](/races/gnome)     | 15.5.51 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Hori Anglemix](/characters/hori_anglemix) | [Gnome](/races/gnome)     | 9.9.60 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Eelkisank Slipdisk](/characters/eelkisank_slipdisk) | [Gnome](/races/gnome)     | 31.1.65 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Rareld Thorirkin](/characters/rareld_thorirkin) | [Nordic](/races/nordic)     | 15.10.2 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Abrinlin Fixneedle](/characters/abrinlin_fixneedle) | [Gnome](/races/gnome)     | 2.7.100 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gicki Quietspindle](/characters/gicki_quietspindle) | [Gnome](/races/gnome)     | 15.7.61 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gicki Trickguard](/characters/gicki_trickguard) | [Gnome](/races/gnome)     | 15.10.52 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Icisirn Sharpspan](/characters/icisirn_sharpspan) | [Gnome](/races/gnome)     | 2.10.92 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Klenkuck Oilphase](/characters/klenkuck_oilphase) | [Gnome](/races/gnome)     | 25.4.82 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Krackec Springcable](/characters/krackec_springcable) | [Gnome](/races/gnome)     | 20.5.64 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kriclick Fuzzyfizz](/characters/kriclick_fuzzyfizz) | [Gnome](/races/gnome)     | 19.11.68 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Keendik Silverspanner](/characters/keendik_silverspanner) | [Gnome](/races/gnome)     | 7.3.73 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Thocki Flickercount](/characters/thocki_flickercount) | [Gnome](/races/gnome)     | 3.9.54 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Hrani Salt-hater](/characters/hrani_salt-hater) | [Nordic](/races/nordic)     | 27.4.15 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Tindick Quietbranch](/characters/tindick_quietbranch) | [Gnome](/races/gnome)     | 14.2.42 BLC |

## Honours

