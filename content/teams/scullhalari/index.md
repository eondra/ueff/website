---
author: "UEFF"
title: "SC Ullh Alari"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Ullh Alari |
| Nation | [Yllin](/nations/yllin) |
| City | Ullh Alari |
| Founded | 02.04.15 LC |
| Field | - |
| Head coach | [Hagrel Dortumal](/characters/hagrel_dortumal) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Ilalaeath Tralee](/characters/ilalaeath_tralee) | [Elf](/races/elf)     | 9.2.179 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Naghrenun Feinuker](/characters/naghrenun_feinuker) | [Elf](/races/elf)     | 28.6.180 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Olvass Daglostior](/characters/olvass_daglostior) | [Elf](/races/elf)     | 18.2.178 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Anam Fenkrana](/characters/anam_fenkrana) | [Elf](/races/elf)     | 11.10.162 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Klekewizz Squiggleclue](/characters/klekewizz_squiggleclue) | [Gnome](/races/gnome)     | 11.10.64 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Baridant Mossleaf](/characters/baridant_mossleaf) | [Elf](/races/elf)     | 24.7.180 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Rydel Ulaxalim](/characters/rydel_ulaxalim) | [Elf](/races/elf)     | 5.11.165 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Althidan Strongrunner](/characters/althidan_strongrunner) | [Elf](/races/elf)     | 6.12.173 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Krer Greenlance](/characters/krer_greenlance) | [Elf](/races/elf)     | 2.2.175 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Cylonian Morris](/characters/cylonian_morris) | [Elf](/races/elf)     | 27.7.180 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Nelalleas Mistclouds](/characters/nelalleas_mistclouds) | [Elf](/races/elf)     | 26.6.167 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Vus Siehunna](/characters/vus_siehunna) | [Elf](/races/elf)     | 27.6.171 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Filandiir Treeshot](/characters/filandiir_treeshot) | [Elf](/races/elf)     | 22.10.171 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Ickiklas Dualhouse](/characters/ickiklas_dualhouse) | [Gnome](/races/gnome)     | 11.6.94 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Kyg Fullshade](/characters/kyg_fullshade) | [Dwarf](/races/dwarf)     | 24.5.58 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Asem Quilamin](/characters/asem_quilamin) | [Elf](/races/elf)     | 7.3.172 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Cemkin Railkettle](/characters/cemkin_railkettle) | [Gnome](/races/gnome)     | 28.10.102 BLC |
| 18 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Demir Carnelis](/characters/demir_carnelis) | [Elf](/races/elf)     | 28.10.171 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Brybmyld Kagras](/characters/brybmyld_kagras) | [Dwarf](/races/dwarf)     | 7.3.68 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ilyllean Leafflower](/characters/ilyllean_leafflower) | [Elf](/races/elf)     | 19.9.169 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Aien Nerdardes](/characters/aien_nerdardes) | [Elf](/races/elf)     | 8.3.180 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Fukugus Mechalocket](/characters/fukugus_mechalocket) | [Gnome](/races/gnome)     | 23.3.53 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Jhaeros Umewynn](/characters/jhaeros_umewynn) | [Elf](/races/elf)     | 5.9.180 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Darthoridan Yllapeiros](/characters/darthoridan_yllapeiros) | [Elf](/races/elf)     | 31.10.164 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Yelaess Fir](/characters/yelaess_fir) | [Elf](/races/elf)     | 1.9.171 BLC |

## Honours

