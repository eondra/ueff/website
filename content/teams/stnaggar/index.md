---
author: "UEFF"
title: "ST Naggar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Tribe Naggar |
| Nation | [Urotha](/nations/urotha) |
| City | Naggar |
| Founded | 06.11.06 LC |
| Field | - |
| Head coach | [Cesern Laughingtale](/characters/cesern_laughingtale) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Bisdo Deathhunter](/characters/bisdo_deathhunter) | [Orc](/races/orc)     | 20.9.1 BLC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Gremush Fireband](/characters/gremush_fireband) | [Orc](/races/orc)     | 5.9.1 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Nobdot Barrenwatch](/characters/nobdot_barrenwatch) | [Orc](/races/orc)     | 28.1.3 LC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Doutic Brickforge](/characters/doutic_brickforge) | [Dwarf](/races/dwarf)     | 16.5.100 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Cazrod Dimhide](/characters/cazrod_dimhide) | [Dwarf](/races/dwarf)     | 3.1.58 BLC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Busto-romm Terved](/characters/busto-romm_terved) | [Orc](/races/orc)     | 11.10.1 LC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Huvurngerm Faggosh](/characters/huvurngerm_faggosh) | [Orc](/races/orc)     | 21.10.3 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Crihm Frostcleaver](/characters/crihm_frostcleaver) | [Orc](/races/orc)     | 20.10.0 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Cragralkk Rapidsnarl](/characters/cragralkk_rapidsnarl) | [Orc](/races/orc)     | 24.10.2 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bulvud Darkrock](/characters/bulvud_darkrock) | [Dwarf](/races/dwarf)     | 16.5.89 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Trivrold Steelbeast](/characters/trivrold_steelbeast) | [Orc](/races/orc)     | 5.10.2 BLC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Bord Strongslice](/characters/bord_strongslice) | [Orc](/races/orc)     | 17.8.1 BLC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Cruzmem Clearrunner](/characters/cruzmem_clearrunner) | [Orc](/races/orc)     | 20.4.3 LC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Trizcohn Khaasnot](/characters/trizcohn_khaasnot) | [Orc](/races/orc)     | 7.10.1 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrovran Favrul](/characters/vrovran_favrul) | [Orc](/races/orc)     | 3.2.1 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Umrird Fullsword](/characters/umrird_fullsword) | [Orc](/races/orc)     | 15.9.4 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Cuzcom Halfmane](/characters/cuzcom_halfmane) | [Orc](/races/orc)     | 2.9.1 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Urntu Deadbreath](/characters/urntu_deadbreath) | [Orc](/races/orc)     | 31.5.1 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Vumahk Tusktaker](/characters/vumahk_tusktaker) | [Orc](/races/orc)     | 31.8.2 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzukk Cruelbeast](/characters/kzukk_cruelbeast) | [Orc](/races/orc)     | 23.6.2 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Dumkohlzo Ezam](/characters/dumkohlzo_ezam) | [Orc](/races/orc)     | 26.10.3 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kror Bittersmoke](/characters/kror_bittersmoke) | [Orc](/races/orc)     | 22.8.2 LC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Thorormr Stonegrip](/characters/thorormr_stonegrip) | [Nordic](/races/nordic)     | 25.8.8 BLC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzunosh Rapidbringer](/characters/kzunosh_rapidbringer) | [Orc](/races/orc)     | 28.3.4 LC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Gan Zoth](/characters/gan_zoth) | [Orc](/races/orc)     | 16.7.4 LC |

## Honours
