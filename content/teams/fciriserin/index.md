---
author: "UEFF"
title: "FC Iri Serin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Iri Serin |
| Nation | [Yllin](/nations/yllin) |
| City | Iri Serin |
| Founded | 12.04.19 LC |
| Field | - |
| Head coach | [Heardred Warflayer](/characters/heardred_warflayer) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Finki Trickyguard](/characters/finki_trickyguard) | [Gnome](/races/gnome)     | 7.2.47 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Mereas Shus](/characters/mereas_shus) | [Elf](/races/elf)     | 25.7.171 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Klilkis Mintcount](/characters/klilkis_mintcount) | [Gnome](/races/gnome)     | 9.1.91 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Visthil Darkshot](/characters/visthil_darkshot) | [Elf](/races/elf)     | 11.2.180 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Deimar Glynzorwyn](/characters/deimar_glynzorwyn) | [Elf](/races/elf)     | 15.5.172 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Aien Miamyar](/characters/aien_miamyar) | [Elf](/races/elf)     | 6.1.167 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Soss Eilmumvim](/characters/soss_eilmumvim) | [Elf](/races/elf)     | 17.6.172 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Jandar Zhinsolvim](/characters/jandar_zhinsolvim) | [Elf](/races/elf)     | 1.5.180 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Hunreirdlul Andrurdla](/characters/hunreirdlul_andrurdla) | [Elf](/races/elf)     | 3.5.180 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Ilyllian Dewcaller](/characters/ilyllian_dewcaller) | [Elf](/races/elf)     | 3.2.168 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Alennul Ravenwalker](/characters/alennul_ravenwalker) | [Elf](/races/elf)     | 29.7.174 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Myries Norro](/characters/myries_norro) | [Elf](/races/elf)     | 14.6.168 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Bornoli Steelfall](/characters/bornoli_steelfall) | [Dwarf](/races/dwarf)     | 8.8.74 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Chathanglas Ionil](/characters/chathanglas_ionil) | [Elf](/races/elf)     | 24.4.170 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Nyrieth Fogleaf](/characters/nyrieth_fogleaf) | [Elf](/races/elf)     | 26.6.180 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Aias Zhantheis](/characters/aias_zhantheis) | [Elf](/races/elf)     | 27.1.180 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Varuvas Iergiolvom](/characters/varuvas_iergiolvom) | [Elf](/races/elf)     | 27.12.171 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Sildulf Hakakin](/characters/sildulf_hakakin) | [Nordic](/races/nordic)     | 17.7.8 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Haemir Staglight](/characters/haemir_staglight) | [Elf](/races/elf)     | 26.3.173 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Cosul Lorawenys](/characters/cosul_lorawenys) | [Elf](/races/elf)     | 19.12.175 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Kriolar Sunrunner](/characters/kriolar_sunrunner) | [Elf](/races/elf)     | 14.12.172 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Miladearn Vislemmiah](/characters/miladearn_vislemmiah) | [Elf](/races/elf)     | 14.9.165 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Irengyl Trueleaf](/characters/irengyl_trueleaf) | [Elf](/races/elf)     | 13.4.170 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gutgi Flat-shield](/characters/gutgi_flat-shield) | [Nordic](/races/nordic)     | 4.3.10 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Sazmotir Birnin](/characters/sazmotir_birnin) | [Dwarf](/races/dwarf)     | 28.7.62 BLC |

## Honours
