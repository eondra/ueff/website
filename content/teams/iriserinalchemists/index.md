---
author: "UEFF"
title: "Iri Serin Alchemists"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Iri Serin Alchemists |
| Nation | [Yllin](/nations/yllin) |
| City | Iri Serin |
| Founded | 21.03.10 LC |
| Field | - |
| Head coach | [Baridant Diolliadu](/characters/baridant_diolliadu) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Sel Wem](/characters/sel_wem) | [Elf](/races/elf)     | 5.11.164 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Chathanglas Elluker](/characters/chathanglas_elluker) | [Elf](/races/elf)     | 31.1.165 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Aladaen Wysaberos](/characters/aladaen_wysaberos) | [Elf](/races/elf)     | 7.10.172 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Krar Shadelight](/characters/krar_shadelight) | [Elf](/races/elf)     | 5.3.176 BLC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Vilt Axehammer](/characters/vilt_axehammer) | [Orc](/races/orc)     | 6.6.2 LC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Leleath Shadeshot](/characters/leleath_shadeshot) | [Elf](/races/elf)     | 10.2.167 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Orrian Starblower](/characters/orrian_starblower) | [Elf](/races/elf)     | 8.10.180 BLC |
| 8 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Nelsil Gastamras](/characters/nelsil_gastamras) | [Elf](/races/elf)     | 9.10.160 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Onund Blooddream](/characters/onund_blooddream) | [Nordic](/races/nordic)     | 29.7.11 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Dutluk Teenygauge](/characters/dutluk_teenygauge) | [Gnome](/races/gnome)     | 22.3.96 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Nirn Deadrage](/characters/nirn_deadrage) | [Orc](/races/orc)     | 1.2.1 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Khiiral Jelier](/characters/khiiral_jelier) | [Elf](/races/elf)     | 12.9.163 BLC |
| 13 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Madaan Gelai](/characters/madaan_gelai) | [Elf](/races/elf)     | 11.9.167 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Kilthil Zeeyilhri](/characters/kilthil_zeeyilhri) | [Elf](/races/elf)     | 18.8.176 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Zarengyl Silosteh](/characters/zarengyl_silosteh) | [Elf](/races/elf)     | 24.2.172 BLC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Sonielu Yeslee](/characters/sonielu_yeslee) | [Elf](/races/elf)     | 30.7.171 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Rylaeth Forestflower](/characters/rylaeth_forestflower) | [Elf](/races/elf)     | 13.11.176 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Dan Drakespine](/characters/dan_drakespine) | [Dwarf](/races/dwarf)     | 10.6.84 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Ugus Glynven](/characters/ugus_glynven) | [Elf](/races/elf)     | 16.2.175 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Elyon Windstalker](/characters/elyon_windstalker) | [Elf](/races/elf)     | 13.1.166 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Luvon Phipeiros](/characters/luvon_phipeiros) | [Elf](/races/elf)     | 14.6.171 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Thuther Moldlon](/characters/thuther_moldlon) | [Elf](/races/elf)     | 28.3.178 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Elren Farieth](/characters/elren_farieth) | [Elf](/races/elf)     | 29.8.177 BLC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kerntoch Dhith](/characters/kerntoch_dhith) | [Orc](/races/orc)     | 7.12.2 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlarmm Ingmikssen](/characters/hlarmm_ingmikssen) | [Nordic](/races/nordic)     | 11.2.2 BLC |

## Honours

