---
author: "UEFF"
title: "Warcry Redkug"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Warcry Redkug |
| Nation | [Urotha](/nations/urotha) |
| City | Redkug |
| Founded | 13.01.09 LC |
| Field | - |
| Head coach | [Sun Rigmuak](/characters/sun_rigmuak) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Umrird Brokenspite](/characters/umrird_brokenspite) | [Orc](/races/orc)     | 6.6.3 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Kavme Olrel](/characters/kavme_olrel) | [Orc](/races/orc)     | 16.8.3 LC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Tizcolk Steelbasher](/characters/tizcolk_steelbasher) | [Orc](/races/orc)     | 9.5.2 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Theres Dhimgud](/characters/theres_dhimgud) | [Orc](/races/orc)     | 30.3.0 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Zargum Brokendeath](/characters/zargum_brokendeath) | [Orc](/races/orc)     | 17.2.1 LC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Sun Vheknas](/characters/sun_vheknas) | [Orc](/races/orc)     | 28.5.1 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Mirgin Rockbasher](/characters/mirgin_rockbasher) | [Orc](/races/orc)     | 22.8.4 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Dumkohlzo Thes](/characters/dumkohlzo_thes) | [Orc](/races/orc)     | 21.5.3 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Het Battleblade](/characters/het_battleblade) | [Orc](/races/orc)     | 1.11.4 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Ramukk Bloodwish](/characters/ramukk_bloodwish) | [Orc](/races/orc)     | 19.6.2 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Hirg Thikdin](/characters/hirg_thikdin) | [Orc](/races/orc)     | 9.4.2 LC |
| 12 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Tralt Tuanuth](/characters/tralt_tuanuth) | [Orc](/races/orc)     | 16.9.1 LC |
| 13 |    Left Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Tizcolk Deaddrums](/characters/tizcolk_deaddrums) | [Orc](/races/orc)     | 7.4.2 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Vuhn Deadstorm](/characters/vuhn_deadstorm) | [Orc](/races/orc)     | 23.4.0 LC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Ihlos Vhak](/characters/ihlos_vhak) | [Orc](/races/orc)     | 29.5.2 LC |
| 16 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Farvarn Quickwind](/characters/farvarn_quickwind) | [Orc](/races/orc)     | 22.6.2 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Gearshift Commonhair](/characters/gearshift_commonhair) | [Dwarf](/races/dwarf)     | 13.9.87 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Krihn Gorearm](/characters/krihn_gorearm) | [Orc](/races/orc)     | 18.6.2 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Cruttash Thunderarm](/characters/cruttash_thunderarm) | [Orc](/races/orc)     | 8.4.2 LC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Nuhm Mut](/characters/nuhm_mut) | [Orc](/races/orc)     | 13.5.0 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Huld Angerchewer](/characters/huld_angerchewer) | [Orc](/races/orc)     | 7.9.2 LC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Erikjar Fork-knee](/characters/erikjar_fork-knee) | [Nordic](/races/nordic)     | 7.7.11 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzorge Brokenteeth](/characters/kzorge_brokenteeth) | [Orc](/races/orc)     | 22.2.0 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Crandezi Thuuzin](/characters/crandezi_thuuzin) | [Orc](/races/orc)     | 4.11.3 LC |
| 25 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Tedrikk Warblade](/characters/tedrikk_warblade) | [Orc](/races/orc)     | 28.7.1 LC |

## Honours

