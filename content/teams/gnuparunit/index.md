---
author: "UEFF"
title: "Gnupar Unit"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Gnupar Unit |
| Nation | [Jamta](/nations/jamta) |
| City | Gnupar |
| Founded | 13.03.09 LC |
| Field | - |
| Head coach | [Rakthjolf Lute-fur](/characters/rakthjolf_lute-fur) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Wulfch Heimdalson](/characters/wulfch_heimdalson) | [Nordic](/races/nordic)     | 27.10.14 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Gudrodr Agnason](/characters/gudrodr_agnason) | [Nordic](/races/nordic)     | 23.7.8 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Sorli Hrolfrson](/characters/sorli_hrolfrson) | [Nordic](/races/nordic)     | 17.2.15 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Adils Lonecaller](/characters/adils_lonecaller) | [Nordic](/races/nordic)     | 8.6.9 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Jokulf Fenarissson](/characters/jokulf_fenarissson) | [Nordic](/races/nordic)     | 13.11.12 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Melkolf Rich-helm](/characters/melkolf_rich-helm) | [Nordic](/races/nordic)     | 5.10.15 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bugrith Fairroar](/characters/bugrith_fairroar) | [Dwarf](/races/dwarf)     | 30.4.75 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hialti Modolfrkin](/characters/hialti_modolfrkin) | [Nordic](/races/nordic)     | 29.8.12 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Holmot Star-eater](/characters/holmot_star-eater) | [Nordic](/races/nordic)     | 21.8.13 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Jayrim Golden-hammers](/characters/jayrim_golden-hammers) | [Nordic](/races/nordic)     | 11.2.11 BLC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Drehd Dhiddes](/characters/drehd_dhiddes) | [Orc](/races/orc)     | 18.10.2 LC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hallkatla Storm-jumper](/characters/hallkatla_storm-jumper) | [Nordic](/races/nordic)     | 24.2.13 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Joridall Chnobson](/characters/joridall_chnobson) | [Nordic](/races/nordic)     | 28.3.9 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Gnupa Hemmingson](/characters/gnupa_hemmingson) | [Nordic](/races/nordic)     | 15.3.13 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Trinhild Erarnssen](/characters/trinhild_erarnssen) | [Nordic](/races/nordic)     | 6.9.12 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Thordar Fridmundrson](/characters/thordar_fridmundrson) | [Nordic](/races/nordic)     | 15.7.6 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Bulenor Boulderbend](/characters/bulenor_boulderbend) | [Nordic](/races/nordic)     | 10.7.7 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Orm Ulrerssen](/characters/orm_ulrerssen) | [Nordic](/races/nordic)     | 23.8.9 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Heidae Hererson](/characters/heidae_hererson) | [Nordic](/races/nordic)     | 27.7.14 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Snow Scar-torn](/characters/snow_scar-torn) | [Nordic](/races/nordic)     | 28.7.8 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Balbberth Cairn-wrecker](/characters/balbberth_cairn-wrecker) | [Nordic](/races/nordic)     | 5.1.11 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drilt Steeltale](/characters/drilt_steeltale) | [Orc](/races/orc)     | 24.12.4 LC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Lyncgvor Steelbrand](/characters/lyncgvor_steelbrand) | [Nordic](/races/nordic)     | 31.7.12 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Knudnnir Torfakin](/characters/knudnnir_torfakin) | [Nordic](/races/nordic)     | 28.4.15 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Otlock Quickbrain](/characters/otlock_quickbrain) | [Gnome](/races/gnome)     | 8.10.91 BLC |

## Honours
