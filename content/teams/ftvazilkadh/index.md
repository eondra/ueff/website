---
author: "UEFF"
title: "FT Vaz Ilkadh"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Tribe Vaz Ilkadh |
| Nation | [Urotha](/nations/urotha) |
| City | Vaz Ilkadh |
| Founded | 13.10.04 LC |
| Field | - |
| Head coach | [Cralzech Ket](/characters/cralzech_ket) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Rulkurn Okihd](/characters/rulkurn_okihd) | [Orc](/races/orc)     | 15.4.3 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Dald Silentcrusher](/characters/dald_silentcrusher) | [Orc](/races/orc)     | 9.11.1 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Bish Axekill](/characters/bish_axekill) | [Orc](/races/orc)     | 2.9.1 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzangosk Brightdrums](/characters/kzangosk_brightdrums) | [Orc](/races/orc)     | 23.5.4 LC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Krume Crik](/characters/krume_crik) | [Orc](/races/orc)     | 20.8.1 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Beclimirn Coilcrown](/characters/beclimirn_coilcrown) | [Gnome](/races/gnome)     | 8.2.62 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Oto Cheznuus](/characters/oto_cheznuus) | [Orc](/races/orc)     | 9.11.2 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Broguk Proudmane](/characters/broguk_proudmane) | [Orc](/races/orc)     | 16.9.4 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Kohltuldez Gumkush](/characters/kohltuldez_gumkush) | [Orc](/races/orc)     | 17.8.1 LC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Cok Gravespirit](/characters/cok_gravespirit) | [Orc](/races/orc)     | 9.10.0 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Grahd Blindspite](/characters/grahd_blindspite) | [Orc](/races/orc)     | 22.3.2 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Orkink Temperscheme](/characters/orkink_temperscheme) | [Gnome](/races/gnome)     | 2.1.103 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Deebi Mekkabrick](/characters/deebi_mekkabrick) | [Gnome](/races/gnome)     | 1.10.79 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Boz Mos](/characters/boz_mos) | [Orc](/races/orc)     | 23.6.0 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Muld Uazot](/characters/muld_uazot) | [Orc](/races/orc)     | 12.2.4 LC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Morkizz Dazzlebell](/characters/morkizz_dazzlebell) | [Gnome](/races/gnome)     | 24.3.51 BLC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Rihk Ret](/characters/rihk_ret) | [Orc](/races/orc)     | 17.10.2 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Grarn Hollowstriker](/characters/grarn_hollowstriker) | [Orc](/races/orc)     | 20.4.4 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Gremrahn Laughingnight](/characters/gremrahn_laughingnight) | [Orc](/races/orc)     | 3.9.2 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Bonvoltal Saurthunder](/characters/bonvoltal_saurthunder) | [Orc](/races/orc)     | 8.4.1 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Thukk Blindchewer](/characters/thukk_blindchewer) | [Orc](/races/orc)     | 8.8.1 BLC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Draldam Roth](/characters/draldam_roth) | [Orc](/races/orc)     | 4.8.1 LC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Kold Bad](/characters/kold_bad) | [Orc](/races/orc)     | 18.4.4 LC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Rulzern Vhusnak](/characters/rulzern_vhusnak) | [Orc](/races/orc)     | 19.4.3 LC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Humdaek Gamdad](/characters/humdaek_gamdad) | [Dwarf](/races/dwarf)     | 8.2.96 BLC |

## Honours

