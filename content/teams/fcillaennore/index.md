---
author: "UEFF"
title: "FC Illa Ennore"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Illa Ennore |
| Nation | [Yllin](/nations/yllin) |
| City | Illa Ennore |
| Founded | 14.02.05 LC |
| Field | - |
| Head coach | [Elephon Phixina](/characters/elephon_phixina) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Deldrach Dawnsinger](/characters/deldrach_dawnsinger) | [Elf](/races/elf)     | 16.4.160 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Wiogmold Beastgut](/characters/wiogmold_beastgut) | [Dwarf](/races/dwarf)     | 2.1.95 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Luvon Shalamin](/characters/luvon_shalamin) | [Elf](/races/elf)     | 22.6.164 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Zolvegar Umetoris](/characters/zolvegar_umetoris) | [Elf](/races/elf)     | 18.1.176 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnemkeefirn Mekkaballoon](/characters/gnemkeefirn_mekkaballoon) | [Gnome](/races/gnome)     | 20.12.41 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Zujinrionar Faenorin](/characters/zujinrionar_faenorin) | [Elf](/races/elf)     | 11.4.175 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Grerni Grimil](/characters/grerni_grimil) | [Orc](/races/orc)     | 2.2.3 LC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Othjodan Vigbiodrson](/characters/othjodan_vigbiodrson) | [Nordic](/races/nordic)     | 22.11.10 BLC |
| 9 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Aumrauth Ilmiollvol](/characters/aumrauth_ilmiollvol) | [Elf](/races/elf)     | 12.4.168 BLC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Malon Julvur](/characters/malon_julvur) | [Elf](/races/elf)     | 25.6.165 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Fagorn Tonlolhru](/characters/fagorn_tonlolhru) | [Elf](/races/elf)     | 24.5.165 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Garril Uriqen](/characters/garril_uriqen) | [Elf](/races/elf)     | 9.2.162 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Krakliwuk Pullboss](/characters/krakliwuk_pullboss) | [Gnome](/races/gnome)     | 23.7.99 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Zadel Maglana](/characters/zadel_maglana) | [Elf](/races/elf)     | 18.5.162 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Myriil Yeslana](/characters/myriil_yeslana) | [Elf](/races/elf)     | 2.4.170 BLC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Zach Cruhd](/characters/zach_cruhd) | [Orc](/races/orc)     | 5.1.3 LC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Krustham Bearblade](/characters/krustham_bearblade) | [Elf](/races/elf)     | 25.6.170 BLC |
| 18 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Horith Sel](/characters/horith_sel) | [Elf](/races/elf)     | 26.7.179 BLC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Tedreas Edes](/characters/tedreas_edes) | [Elf](/races/elf)     | 18.11.172 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Aseam Heimoira](/characters/aseam_heimoira) | [Elf](/races/elf)     | 23.12.162 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Magos Essinno](/characters/magos_essinno) | [Elf](/races/elf)     | 29.7.168 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Krustham Torbalar](/characters/krustham_torbalar) | [Elf](/races/elf)     | 27.1.178 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mathil Stonesnow](/characters/mathil_stonesnow) | [Elf](/races/elf)     | 22.1.166 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mythaleath Xiltoris](/characters/mythaleath_xiltoris) | [Elf](/races/elf)     | 30.1.180 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kleemko Porterstrip](/characters/kleemko_porterstrip) | [Gnome](/races/gnome)     | 14.8.81 BLC |

## Honours

