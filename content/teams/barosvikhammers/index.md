---
author: "UEFF"
title: "Barosvik Hammers"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Barosvik Hammers |
| Nation | [Jamta](/nations/jamta) |
| City | Barosvik |
| Founded | 05.10.05 LC |
| Field | - |
| Head coach | [Skearel Ulf-krakuson](/characters/skearel_ulf-krakuson) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Loaes Stonepike](/characters/loaes_stonepike) | [Nordic](/races/nordic)     | 10.10.3 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Rigelf Short-eater](/characters/rigelf_short-eater) | [Nordic](/races/nordic)     | 6.7.8 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Irlolf Fire-knee](/characters/irlolf_fire-knee) | [Nordic](/races/nordic)     | 28.2.14 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Svogferth Ehrarikesen](/characters/svogferth_ehrarikesen) | [Nordic](/races/nordic)     | 29.11.7 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Verlidi Bear-toe](/characters/verlidi_bear-toe) | [Nordic](/races/nordic)     | 25.4.14 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Dag Thorliotrson](/characters/dag_thorliotrson) | [Nordic](/races/nordic)     | 13.5.5 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilavish Switchbit](/characters/gnilavish_switchbit) | [Gnome](/races/gnome)     | 28.7.59 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Svavarr Earthbend](/characters/svavarr_earthbend) | [Nordic](/races/nordic)     | 7.5.10 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Svaramor Warwatcher](/characters/svaramor_warwatcher) | [Nordic](/races/nordic)     | 12.1.4 BLC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Tit Redstriker](/characters/tit_redstriker) | [Orc](/races/orc)     | 14.7.1 LC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Eymundr Warwatcher](/characters/eymundr_warwatcher) | [Nordic](/races/nordic)     | 1.9.3 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Galmr Soul-bane](/characters/galmr_soul-bane) | [Nordic](/races/nordic)     | 21.10.4 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Thengill Holmsteinson](/characters/thengill_holmsteinson) | [Nordic](/races/nordic)     | 25.2.13 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Akaoknolf Fjorinssen](/characters/akaoknolf_fjorinssen) | [Nordic](/races/nordic)     | 6.5.11 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Kzarg Hellshift](/characters/kzarg_hellshift) | [Orc](/races/orc)     | 26.1.2 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Gogrkir Lightshout](/characters/gogrkir_lightshout) | [Nordic](/races/nordic)     | 2.11.6 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Kollsveinn Earthfist](/characters/kollsveinn_earthfist) | [Nordic](/races/nordic)     | 20.11.3 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Sifrad Palson](/characters/sifrad_palson) | [Nordic](/races/nordic)     | 23.1.4 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Jeran Halladrson](/characters/jeran_halladrson) | [Nordic](/races/nordic)     | 15.6.12 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Jorinvar Bulwark](/characters/jorinvar_bulwark) | [Nordic](/races/nordic)     | 1.1.8 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Bindizz Oilychart](/characters/bindizz_oilychart) | [Gnome](/races/gnome)     | 11.2.95 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Snorri Stormbash](/characters/snorri_stormbash) | [Nordic](/races/nordic)     | 19.1.11 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Ozur Thorlakrkin](/characters/ozur_thorlakrkin) | [Nordic](/races/nordic)     | 13.1.5 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Thodrekr Sword-maiden](/characters/thodrekr_sword-maiden) | [Nordic](/races/nordic)     | 5.11.4 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Kvistr Mjorkesson](/characters/kvistr_mjorkesson) | [Nordic](/races/nordic)     | 19.9.13 BLC |

## Honours
