---
author: "UEFF"
title: "FC Vin Daral"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Clan Vin Daral |
| Nation | [Vongram](/nations/vongram) |
| City | Vin Daral |
| Founded | 14.10.18 LC |
| Field | - |
| Head coach | [Oznead Koboldbelt](/characters/oznead_koboldbelt) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Rutrun Steelheart](/characters/rutrun_steelheart) | [Dwarf](/races/dwarf)     | 26.10.57 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Nuratmaes Stonebrew](/characters/nuratmaes_stonebrew) | [Dwarf](/races/dwarf)     | 26.2.74 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Skargrock Redthane](/characters/skargrock_redthane) | [Dwarf](/races/dwarf)     | 1.10.99 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Gerngagbukk Vonnuash](/characters/gerngagbukk_vonnuash) | [Orc](/races/orc)     | 3.12.1 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Teerkik Grimeyclue](/characters/teerkik_grimeyclue) | [Gnome](/races/gnome)     | 5.7.55 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Hoggarlum Runwimn](/characters/hoggarlum_runwimn) | [Dwarf](/races/dwarf)     | 14.2.94 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Grum Broadmace](/characters/grum_broadmace) | [Dwarf](/races/dwarf)     | 18.4.55 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Garsk Kendem](/characters/garsk_kendem) | [Dwarf](/races/dwarf)     | 27.10.78 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Cocleergirn Thistlecrown](/characters/cocleergirn_thistlecrown) | [Gnome](/races/gnome)     | 4.4.48 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Bulgur Kimbir](/characters/bulgur_kimbir) | [Dwarf](/races/dwarf)     | 5.5.55 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Fotmot Ingotdigger](/characters/fotmot_ingotdigger) | [Dwarf](/races/dwarf)     | 5.8.60 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Elkgrout Chaingrip](/characters/elkgrout_chaingrip) | [Dwarf](/races/dwarf)     | 12.12.54 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Dracnog Greatspark](/characters/dracnog_greatspark) | [Dwarf](/races/dwarf)     | 8.6.95 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Rold Steelsteam](/characters/rold_steelsteam) | [Dwarf](/races/dwarf)     | 1.4.98 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Krasdrunli Stormgrip](/characters/krasdrunli_stormgrip) | [Dwarf](/races/dwarf)     | 30.7.88 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Brognut Woldtoe](/characters/brognut_woldtoe) | [Dwarf](/races/dwarf)     | 1.9.65 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Duncosh Flickerbrake](/characters/duncosh_flickerbrake) | [Gnome](/races/gnome)     | 2.8.92 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Sitgrus Geknem](/characters/sitgrus_geknem) | [Dwarf](/races/dwarf)     | 28.2.57 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Kollsveinn Goreseeker](/characters/kollsveinn_goreseeker) | [Nordic](/races/nordic)     | 23.4.10 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Bedgrok Rockcrest](/characters/bedgrok_rockcrest) | [Dwarf](/races/dwarf)     | 29.8.86 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Relmak Giantforged](/characters/relmak_giantforged) | [Dwarf](/races/dwarf)     | 15.10.58 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Thukhuli Pyrepunch](/characters/thukhuli_pyrepunch) | [Dwarf](/races/dwarf)     | 11.9.75 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Snarhit Alesunder](/characters/snarhit_alesunder) | [Dwarf](/races/dwarf)     | 21.12.99 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Noraruri Getnes](/characters/noraruri_getnes) | [Dwarf](/races/dwarf)     | 7.1.91 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Vid Graybringer](/characters/vid_graybringer) | [Dwarf](/races/dwarf)     | 9.3.99 BLC |

## Honours

