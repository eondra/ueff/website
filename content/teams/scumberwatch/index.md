---
author: "UEFF"
title: "SC Umberwatch"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Sports Club Umberwatch |
| Nation | [Katargo](/nations/katargo) |
| City | Umberwatch |
| Founded | 17.06.04 LC |
| Field | - |
| Head coach | [Samm Thul](/characters/samm_thul) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Pillen Dazzlesteel](/characters/pillen_dazzlesteel) | [Gnome](/races/gnome)     | 12.6.105 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Thamu Wobbledock](/characters/thamu_wobbledock) | [Gnome](/races/gnome)     | 26.4.69 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Ilirn Steamtorque](/characters/ilirn_steamtorque) | [Gnome](/races/gnome)     | 15.10.83 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Bevdes Dhem](/characters/bevdes_dhem) | [Orc](/races/orc)     | 8.12.2 LC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Imlik Wheelfield](/characters/imlik_wheelfield) | [Gnome](/races/gnome)     | 19.6.63 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Urkink Squiggleclue](/characters/urkink_squiggleclue) | [Gnome](/races/gnome)     | 21.3.76 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Beclimirn Mintbells](/characters/beclimirn_mintbells) | [Gnome](/races/gnome)     | 9.6.56 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Dild Quickpack](/characters/dild_quickpack) | [Orc](/races/orc)     | 28.4.0 LC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Ronfon Cavemaster](/characters/ronfon_cavemaster) | [Dwarf](/races/dwarf)     | 31.1.84 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Ikun Overscheme](/characters/ikun_overscheme) | [Gnome](/races/gnome)     | 7.1.84 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Eenbe Stitchspark](/characters/eenbe_stitchspark) | [Gnome](/races/gnome)     | 1.9.64 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Kjyr Stonegrip](/characters/kjyr_stonegrip) | [Nordic](/races/nordic)     | 12.11.14 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Linduzz Trickcookie](/characters/linduzz_trickcookie) | [Gnome](/races/gnome)     | 26.2.64 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Enklurlick Pullboss](/characters/enklurlick_pullboss) | [Gnome](/races/gnome)     | 13.9.93 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Bergthorr Sun-sayer](/characters/bergthorr_sun-sayer) | [Nordic](/races/nordic)     | 15.7.13 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Thumkes Switchwizzle](/characters/thumkes_switchwizzle) | [Gnome](/races/gnome)     | 22.6.53 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Alkenk Flukecog](/characters/alkenk_flukecog) | [Gnome](/races/gnome)     | 16.9.79 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gniles Dualbrake](/characters/gniles_dualbrake) | [Gnome](/races/gnome)     | 19.3.105 BLC |
| 19 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilkeck Wobbleguard](/characters/gnilkeck_wobbleguard) | [Gnome](/races/gnome)     | 10.10.80 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Ditish Squigglecord](/characters/ditish_squigglecord) | [Gnome](/races/gnome)     | 12.10.81 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kruthunboc Swiftcog](/characters/kruthunboc_swiftcog) | [Gnome](/races/gnome)     | 5.6.64 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Mildi Gearpocket](/characters/mildi_gearpocket) | [Gnome](/races/gnome)     | 19.6.54 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilkees Sparkflow](/characters/gnilkees_sparkflow) | [Gnome](/races/gnome)     | 20.6.51 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Pankloldi Quirkdish](/characters/pankloldi_quirkdish) | [Gnome](/races/gnome)     | 4.1.40 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Gini Squigglecord](/characters/gini_squigglecord) | [Gnome](/races/gnome)     | 21.10.80 BLC |

## Honours

