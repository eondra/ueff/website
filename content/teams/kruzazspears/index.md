---
author: "UEFF"
title: "Kruzaz Spears"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Kruzaz Spears |
| Nation | [Urotha](/nations/urotha) |
| City | Kruzaz |
| Founded | 22.05.04 LC |
| Field | - |
| Head coach | [Akkold Coldheart](/characters/akkold_coldheart) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Rurgoth Halfspirit](/characters/rurgoth_halfspirit) | [Orc](/races/orc)     | 28.8.0 LC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Muk Blindblade](/characters/muk_blindblade) | [Orc](/races/orc)     | 31.8.2 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Thovgas Bonemane](/characters/thovgas_bonemane) | [Orc](/races/orc)     | 13.6.3 LC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Hevru Muvred](/characters/hevru_muvred) | [Orc](/races/orc)     | 10.11.4 LC |
| 5 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Treldot Stoutwatch](/characters/treldot_stoutwatch) | [Orc](/races/orc)     | 3.4.3 LC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Cazrem Gravelbringer](/characters/cazrem_gravelbringer) | [Dwarf](/races/dwarf)     | 16.3.88 BLC |
| 7 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Batoz Laughingtale](/characters/batoz_laughingtale) | [Orc](/races/orc)     | 12.2.3 LC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Trorzez Graveblade](/characters/trorzez_graveblade) | [Orc](/races/orc)     | 22.11.2 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Urntu Issuk](/characters/urntu_issuk) | [Orc](/races/orc)     | 22.6.2 BLC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Krintem Irondrums](/characters/krintem_irondrums) | [Orc](/races/orc)     | 5.5.1 LC |
| 11 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Fiknarg Dish](/characters/fiknarg_dish) | [Orc](/races/orc)     | 25.7.1 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Prime Mudbuckle](/characters/prime_mudbuckle) | [Dwarf](/races/dwarf)     | 13.5.74 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Viggnen Glumkin](/characters/viggnen_glumkin) | [Nordic](/races/nordic)     | 4.1.3 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Untez Graagir](/characters/untez_graagir) | [Orc](/races/orc)     | 5.11.2 LC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Throstekk Lowtask](/characters/throstekk_lowtask) | [Orc](/races/orc)     | 18.4.2 LC |
| 16 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Grust Mezohd](/characters/grust_mezohd) | [Orc](/races/orc)     | 23.11.1 LC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Praverg Halfdeath](/characters/praverg_halfdeath) | [Orc](/races/orc)     | 2.9.3 LC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Carg Bronzebleeder](/characters/carg_bronzebleeder) | [Orc](/races/orc)     | 26.10.1 LC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Vrird Zesnith](/characters/vrird_zesnith) | [Orc](/races/orc)     | 3.4.2 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Alkzard Wildmight](/characters/alkzard_wildmight) | [Orc](/races/orc)     | 7.12.2 LC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Drezru Geth](/characters/drezru_geth) | [Orc](/races/orc)     | 16.8.0 LC |
| 22 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Cach Dhimgud](/characters/cach_dhimgud) | [Orc](/races/orc)     | 20.7.3 LC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Falt Darkpride](/characters/falt_darkpride) | [Orc](/races/orc)     | 22.10.1 BLC |
| 24 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Crohm Kaadduhn](/characters/crohm_kaadduhn) | [Orc](/races/orc)     | 7.3.1 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Dinkok Gearwhistle](/characters/dinkok_gearwhistle) | [Gnome](/races/gnome)     | 23.10.43 BLC |

## Honours

