---
author: "UEFF"
title: "FC Odeila"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Odeila |
| Nation | [Jamta](/nations/jamta) |
| City | Odeila |
| Founded | 09.06.14 LC |
| Field | - |
| Head coach | [Halors Ragewound](/characters/halors_ragewound) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Sirkrlak Flat-shield](/characters/sirkrlak_flat-shield) | [Nordic](/races/nordic)     | 20.12.2 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Miclon Pulseblock](/characters/miclon_pulseblock) | [Gnome](/races/gnome)     | 22.4.81 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hamall Fullstride](/characters/hamall_fullstride) | [Nordic](/races/nordic)     | 22.6.13 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hjrntus Hammer-fire](/characters/hjrntus_hammer-fire) | [Nordic](/races/nordic)     | 18.12.14 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Sel Skullwind](/characters/sel_skullwind) | [Nordic](/races/nordic)     | 11.6.6 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Finngeirr Tarbaldersen](/characters/finngeirr_tarbaldersen) | [Nordic](/races/nordic)     | 12.3.8 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Ofocleen Briskbang](/characters/ofocleen_briskbang) | [Gnome](/races/gnome)     | 10.3.40 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Sneng Wildsteel](/characters/sneng_wildsteel) | [Nordic](/races/nordic)     | 17.4.7 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Nereidr Spring-hilt](/characters/nereidr_spring-hilt) | [Nordic](/races/nordic)     | 11.6.10 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Fjolmod Wide-smasher](/characters/fjolmod_wide-smasher) | [Nordic](/races/nordic)     | 5.2.15 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Hlarmm Firehelm](/characters/hlarmm_firehelm) | [Nordic](/races/nordic)     | 31.12.12 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Rold Somvos](/characters/rold_somvos) | [Dwarf](/races/dwarf)     | 18.8.61 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Thorri Geode-victim](/characters/thorri_geode-victim) | [Nordic](/races/nordic)     | 31.3.3 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Steinvith Bollason](/characters/steinvith_bollason) | [Nordic](/races/nordic)     | 21.3.15 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Helerlug Plateheart](/characters/helerlug_plateheart) | [Dwarf](/races/dwarf)     | 4.1.54 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Otkel Warlock](/characters/otkel_warlock) | [Nordic](/races/nordic)     | 20.4.8 BLC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Sur Alrahrikson](/characters/sur_alrahrikson) | [Nordic](/races/nordic)     | 26.5.6 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Sigfus Crag-mouth](/characters/sigfus_crag-mouth) | [Nordic](/races/nordic)     | 16.8.7 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Rognavald Clanrider](/characters/rognavald_clanrider) | [Nordic](/races/nordic)     | 6.1.14 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Agmaild Tiorvakin](/characters/agmaild_tiorvakin) | [Nordic](/races/nordic)     | 26.3.5 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Floki Long](/characters/floki_long) | [Nordic](/races/nordic)     | 1.8.10 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Jolf Lute-fur](/characters/jolf_lute-fur) | [Nordic](/races/nordic)     | 21.2.6 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Horkbrir Fine-shaper](/characters/horkbrir_fine-shaper) | [Nordic](/races/nordic)     | 19.7.14 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Sigvaldi Friccoson](/characters/sigvaldi_friccoson) | [Nordic](/races/nordic)     | 6.1.12 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Frotor Hallgeirson](/characters/frotor_hallgeirson) | [Nordic](/races/nordic)     | 18.2.13 BLC |

## Honours
