---
author: "UEFF"
title: "Strig Haell"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Strig Haell |
| Nation | [Jamta](/nations/jamta) |
| City | Haell |
| Founded | 20.10.16 LC |
| Field | - |
| Head coach | [Hrodpr Geirsteinson](/characters/hrodpr_geirsteinson) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Amundi Hererson](/characters/amundi_hererson) | [Nordic](/races/nordic)     | 10.10.3 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Melkolf Proudshield](/characters/melkolf_proudshield) | [Nordic](/races/nordic)     | 14.11.7 BLC |
| 3 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Thre-duvarg Darkchain](/characters/thre-duvarg_darkchain) | [Orc](/races/orc)     | 1.4.2 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Jolondir Halfdanson](/characters/jolondir_halfdanson) | [Nordic](/races/nordic)     | 4.4.9 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Hisas Halson](/characters/hisas_halson) | [Nordic](/races/nordic)     | 25.4.9 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Ulfliotr Greycloak](/characters/ulfliotr_greycloak) | [Nordic](/races/nordic)     | 30.9.12 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Rogearel Kjornesen](/characters/rogearel_kjornesen) | [Nordic](/races/nordic)     | 18.11.8 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Eldiarn Goreseeker](/characters/eldiarn_goreseeker) | [Nordic](/races/nordic)     | 25.6.8 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Yngvar Horse-toes](/characters/yngvar_horse-toes) | [Nordic](/races/nordic)     | 31.12.8 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Grand Sentry](/characters/grand_sentry) | [Dwarf](/races/dwarf)     | 5.4.53 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Thorgestr Engmarikssen](/characters/thorgestr_engmarikssen) | [Nordic](/races/nordic)     | 18.8.12 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Vebnud Digger](/characters/vebnud_digger) | [Dwarf](/races/dwarf)     | 8.2.64 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Bodmodr Saemingkin](/characters/bodmodr_saemingkin) | [Nordic](/races/nordic)     | 20.3.15 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hraerekr Mountainsteel](/characters/hraerekr_mountainsteel) | [Nordic](/races/nordic)     | 6.9.12 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Loftr War-gobler](/characters/loftr_war-gobler) | [Nordic](/races/nordic)     | 10.1.11 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Filettle Sparklewrench](/characters/filettle_sparklewrench) | [Gnome](/races/gnome)     | 13.9.95 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Ull Hakason](/characters/ull_hakason) | [Nordic](/races/nordic)     | 16.7.13 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Gudbrand Viga-glumson](/characters/gudbrand_viga-glumson) | [Nordic](/races/nordic)     | 10.4.11 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Grosalan Mahd](/characters/grosalan_mahd) | [Orc](/races/orc)     | 8.4.0 LC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Vigsterkr Seven-nail](/characters/vigsterkr_seven-nail) | [Nordic](/races/nordic)     | 2.10.13 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Arnvid Thorolfrkin](/characters/arnvid_thorolfrkin) | [Nordic](/races/nordic)     | 5.5.3 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Borgrald Gjarssen](/characters/borgrald_gjarssen) | [Nordic](/races/nordic)     | 5.1.5 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Wilmgr Boar-chucker](/characters/wilmgr_boar-chucker) | [Nordic](/races/nordic)     | 19.7.7 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Bjadric Alfisorsson](/characters/bjadric_alfisorsson) | [Nordic](/races/nordic)     | 7.2.2 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Pall Nialson](/characters/pall_nialson) | [Nordic](/races/nordic)     | 19.12.7 BLC |

## Honours
