---
author: "UEFF"
title: "Umberwatch Guards"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Umberwatch Guards |
| Nation | [Katargo](/nations/katargo) |
| City | Umberwatch |
| Founded | 19.01.09 LC |
| Field | - |
| Head coach | [Fithak Tinkerspark](/characters/fithak_tinkerspark) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Inciblick Anglemix](/characters/inciblick_anglemix) | [Gnome](/races/gnome)     | 8.11.99 BLC |
| 2 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Henlees Fuzzyfuzz](/characters/henlees_fuzzyfuzz) | [Gnome](/races/gnome)     | 14.2.70 BLC |
| 3 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Girgettlish Oilyspinner](/characters/girgettlish_oilyspinner) | [Gnome](/races/gnome)     | 26.6.92 BLC |
| 4 |    Left Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Incis Pullboss](/characters/incis_pullboss) | [Gnome](/races/gnome)     | 16.1.40 BLC |
| 5 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Tutlethizz Cogwizzle](/characters/tutlethizz_cogwizzle) | [Gnome](/races/gnome)     | 2.5.103 BLC |
| 6 |    Right Back    | ![Katargo](/images/flags/katargo_small_md.png) | [Dollik Sparklewrench](/characters/dollik_sparklewrench) | [Gnome](/races/gnome)     | 9.6.69 BLC |
| 7 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Mildi Dualchin](/characters/mildi_dualchin) | [Gnome](/races/gnome)     | 15.8.48 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Leeckek Blackfuzz](/characters/leeckek_blackfuzz) | [Gnome](/races/gnome)     | 11.4.71 BLC |
| 9 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Feeckes Quickpickle](/characters/feeckes_quickpickle) | [Gnome](/races/gnome)     | 22.3.92 BLC |
| 10 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Rima Kem](/characters/rima_kem) | [Orc](/races/orc)     | 21.5.1 LC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gnilis Wireguard](/characters/gnilis_wireguard) | [Gnome](/races/gnome)     | 13.1.74 BLC |
| 12 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Theeckazz Silverspanner](/characters/theeckazz_silverspanner) | [Gnome](/races/gnome)     | 11.2.71 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Meedish Squigglecord](/characters/meedish_squigglecord) | [Gnome](/races/gnome)     | 13.10.58 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Skamuth Stefnirkin](/characters/skamuth_stefnirkin) | [Nordic](/races/nordic)     | 11.9.4 BLC |
| 15 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Kramkack Billowdata](/characters/kramkack_billowdata) | [Gnome](/races/gnome)     | 3.2.83 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Cokon Grimeycrown](/characters/cokon_grimeycrown) | [Gnome](/races/gnome)     | 20.6.96 BLC |
| 17 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Thamu Fuzzyheart](/characters/thamu_fuzzyheart) | [Gnome](/races/gnome)     | 4.12.85 BLC |
| 18 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Gankleerush Flukeballoon](/characters/gankleerush_flukeballoon) | [Gnome](/races/gnome)     | 11.11.71 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Krigust Shadowheart](/characters/krigust_shadowheart) | [Orc](/races/orc)     | 2.4.2 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Pimack Pipepatch](/characters/pimack_pipepatch) | [Gnome](/races/gnome)     | 2.1.68 BLC |
| 21 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Filis Sparkgear](/characters/filis_sparkgear) | [Gnome](/races/gnome)     | 31.7.92 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Miclon Niftypitch](/characters/miclon_niftypitch) | [Gnome](/races/gnome)     | 14.12.58 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Itlazz Sharpballoon](/characters/itlazz_sharpballoon) | [Gnome](/races/gnome)     | 13.2.67 BLC |
| 24 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Belick Grimeychart](/characters/belick_grimeychart) | [Gnome](/races/gnome)     | 24.11.61 BLC |
| 25 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Theeckazz Trickyguard](/characters/theeckazz_trickyguard) | [Gnome](/races/gnome)     | 24.8.40 BLC |

## Honours

