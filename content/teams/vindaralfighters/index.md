---
author: "UEFF"
title: "Vin Daral Fighters"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vin Daral Fighters |
| Nation | [Vongram](/nations/vongram) |
| City | Vin Daral |
| Founded | 16.08.10 LC |
| Field | - |
| Head coach | [Garsk Warspine](/characters/garsk_warspine) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Bomdon Moltenforge](/characters/bomdon_moltenforge) | [Dwarf](/races/dwarf)     | 21.1.82 BLC |
| 2 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Kernzuld Halfbones](/characters/kernzuld_halfbones) | [Orc](/races/orc)     | 5.3.3 LC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Bratmec Flaskarm](/characters/bratmec_flaskarm) | [Dwarf](/races/dwarf)     | 28.8.98 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Adept Ghodd](/characters/adept_ghodd) | [Dwarf](/races/dwarf)     | 7.9.65 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Mag Bluntsunder](/characters/mag_bluntsunder) | [Dwarf](/races/dwarf)     | 1.2.67 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Jur Rilrir](/characters/jur_rilrir) | [Dwarf](/races/dwarf)     | 21.11.53 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Losteck Sorgod](/characters/losteck_sorgod) | [Dwarf](/races/dwarf)     | 2.1.94 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Zomdan Luh](/characters/zomdan_luh) | [Dwarf](/races/dwarf)     | 24.11.63 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Uucruc Flatbraids](/characters/uucruc_flatbraids) | [Dwarf](/races/dwarf)     | 20.1.53 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Merra Oddrkin](/characters/merra_oddrkin) | [Nordic](/races/nordic)     | 18.5.10 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Jozaet Vebbem](/characters/jozaet_vebbem) | [Dwarf](/races/dwarf)     | 6.5.52 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Vunnrod Battlechewer](/characters/vunnrod_battlechewer) | [Nordic](/races/nordic)     | 20.11.11 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Kusgrouth Ingotbraid](/characters/kusgrouth_ingotbraid) | [Dwarf](/races/dwarf)     | 17.11.95 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Iolic Longgranite](/characters/iolic_longgranite) | [Dwarf](/races/dwarf)     | 28.6.87 BLC |
| 15 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Brast Graverest](/characters/brast_graverest) | [Orc](/races/orc)     | 29.3.3 LC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Durikhik Rumblegem](/characters/durikhik_rumblegem) | [Dwarf](/races/dwarf)     | 29.6.53 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Nomnoli Frostborn](/characters/nomnoli_frostborn) | [Dwarf](/races/dwarf)     | 1.6.58 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Reingromi Rubyarmour](/characters/reingromi_rubyarmour) | [Dwarf](/races/dwarf)     | 11.12.94 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Snastread Terrasteam](/characters/snastread_terrasteam) | [Dwarf](/races/dwarf)     | 9.4.51 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Thrassom Coppergrip](/characters/thrassom_coppergrip) | [Dwarf](/races/dwarf)     | 29.4.90 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Widder Patrol](/characters/widder_patrol) | [Dwarf](/races/dwarf)     | 14.12.54 BLC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Zot Vanbad](/characters/zot_vanbad) | [Dwarf](/races/dwarf)     | 22.1.82 BLC |
| 23 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Sardik Darkchain](/characters/sardik_darkchain) | [Orc](/races/orc)     | 23.10.2 LC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Veteran Dremn](/characters/veteran_dremn) | [Dwarf](/races/dwarf)     | 29.6.89 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Rekdac Bonemail](/characters/rekdac_bonemail) | [Dwarf](/races/dwarf)     | 4.2.64 BLC |

## Honours

