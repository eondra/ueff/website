---
author: "UEFF"
title: "Mount Kinbadur"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Mount Kinbadur |
| Nation | [Vongram](/nations/vongram) |
| City | Kinbadur |
| Founded | 06.10.15 LC |
| Field | - |
| Head coach | [Sitgrus Nugmadd](/characters/sitgrus_nugmadd) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Traariold Coinbuckle](/characters/traariold_coinbuckle) | [Dwarf](/races/dwarf)     | 11.5.75 BLC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Druugrac Excavator](/characters/druugrac_excavator) | [Dwarf](/races/dwarf)     | 20.9.66 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Brybmyld Darkbash](/characters/brybmyld_darkbash) | [Dwarf](/races/dwarf)     | 19.1.70 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Kurgin Bricktoe](/characters/kurgin_bricktoe) | [Dwarf](/races/dwarf)     | 14.5.82 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Rarherlum Hen](/characters/rarherlum_hen) | [Dwarf](/races/dwarf)     | 24.3.71 BLC |
| 6 |    Right Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Groozumli Rubyarmour](/characters/groozumli_rubyarmour) | [Dwarf](/races/dwarf)     | 24.11.60 BLC |
| 7 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Nossolin Pyregleam](/characters/nossolin_pyregleam) | [Dwarf](/races/dwarf)     | 26.8.59 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Vemdud Bristlearm](/characters/vemdud_bristlearm) | [Dwarf](/races/dwarf)     | 16.4.89 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Gaggock Hammerglide](/characters/gaggock_hammerglide) | [Dwarf](/races/dwarf)     | 14.8.78 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Steinfidr Thordson](/characters/steinfidr_thordson) | [Nordic](/races/nordic)     | 27.11.4 BLC |
| 11 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Itkizz Springsteel](/characters/itkizz_springsteel) | [Gnome](/races/gnome)     | 11.9.104 BLC |
| 12 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Mumoth Earthbraids](/characters/mumoth_earthbraids) | [Dwarf](/races/dwarf)     | 18.6.74 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Lorratir Battleglide](/characters/lorratir_battleglide) | [Dwarf](/races/dwarf)     | 19.6.58 BLC |
| 14 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Sryck Thunderforge](/characters/sryck_thunderforge) | [Dwarf](/races/dwarf)     | 1.7.67 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Strud Hardglow](/characters/strud_hardglow) | [Dwarf](/races/dwarf)     | 24.1.91 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Broukdreas Thrum](/characters/broukdreas_thrum) | [Dwarf](/races/dwarf)     | 3.9.71 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Finerlun Icegranite](/characters/finerlun_icegranite) | [Dwarf](/races/dwarf)     | 27.1.100 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Haknmoor Freehand](/characters/haknmoor_freehand) | [Nordic](/races/nordic)     | 29.4.15 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Humzan Oakhead](/characters/humzan_oakhead) | [Dwarf](/races/dwarf)     | 19.4.65 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Fiknig Bimlim](/characters/fiknig_bimlim) | [Dwarf](/races/dwarf)     | 12.5.61 BLC |
| 21 |    Attacker    | ![Urotha](/images/flags/urotha_small_md.png) | [Ozmusorm Burningspite](/characters/ozmusorm_burningspite) | [Orc](/races/orc)     | 1.3.0 LC |
| 22 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Dundromri Gondon](/characters/dundromri_gondon) | [Dwarf](/races/dwarf)     | 30.1.71 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gutgi Asgrimrson](/characters/gutgi_asgrimrson) | [Nordic](/races/nordic)     | 13.7.7 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Faldud Ranbam](/characters/faldud_ranbam) | [Dwarf](/races/dwarf)     | 14.6.97 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Grozzumli Darkarmour](/characters/grozzumli_darkarmour) | [Dwarf](/races/dwarf)     | 8.9.78 BLC |

## Honours

