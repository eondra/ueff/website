---
author: "UEFF"
title: "FS Dofrar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Society Dofrar |
| Nation | [Jamta](/nations/jamta) |
| City | Dofrar |
| Founded | 04.06.09 LC |
| Field | - |
| Head coach | [Haraldr Broken-lute](/characters/haraldr_broken-lute) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Katargo](/images/flags/katargo_small_md.png) | [Indozz Tidysprocket](/characters/indozz_tidysprocket) | [Gnome](/races/gnome)     | 26.6.49 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Rargthjar Svarthofdason](/characters/rargthjar_svarthofdason) | [Nordic](/races/nordic)     | 19.11.3 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Sjarrgal Alrornson](/characters/sjarrgal_alrornson) | [Nordic](/races/nordic)     | 23.6.14 BLC |
| 4 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Kolskeggr Godmundson](/characters/kolskeggr_godmundson) | [Nordic](/races/nordic)     | 30.6.3 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Revatar Bold-wand](/characters/revatar_bold-wand) | [Nordic](/races/nordic)     | 11.6.8 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Sorran Adilson](/characters/sorran_adilson) | [Nordic](/races/nordic)     | 7.12.6 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Jokul Vandilson](/characters/jokul_vandilson) | [Nordic](/races/nordic)     | 3.2.8 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Angvjorn Djaldson](/characters/angvjorn_djaldson) | [Nordic](/races/nordic)     | 24.9.7 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Majstag Haraeldsen](/characters/majstag_haraeldsen) | [Nordic](/races/nordic)     | 21.10.10 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Ramtkas Darkchewer](/characters/ramtkas_darkchewer) | [Nordic](/races/nordic)     | 14.6.7 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Brukred Heavyblaze](/characters/brukred_heavyblaze) | [Dwarf](/races/dwarf)     | 22.10.84 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Throct Finnbogason](/characters/throct_finnbogason) | [Nordic](/races/nordic)     | 26.7.11 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Braellyn Shadowsword](/characters/braellyn_shadowsword) | [Nordic](/races/nordic)     | 2.7.14 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Klefilkonk Blackcollar](/characters/klefilkonk_blackcollar) | [Gnome](/races/gnome)     | 30.10.83 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Gauk Jargornson](/characters/gauk_jargornson) | [Nordic](/races/nordic)     | 20.7.6 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Torvnar Asgrimrson](/characters/torvnar_asgrimrson) | [Nordic](/races/nordic)     | 9.2.7 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Menerd Steinason](/characters/menerd_steinason) | [Nordic](/races/nordic)     | 2.3.4 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Kylan Longbasher](/characters/kylan_longbasher) | [Nordic](/races/nordic)     | 16.6.10 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Erilar Wildbeard](/characters/erilar_wildbeard) | [Nordic](/races/nordic)     | 5.7.9 BLC |
| 20 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Urlaalf Erarokson](/characters/urlaalf_erarokson) | [Nordic](/races/nordic)     | 20.4.13 BLC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Babror Hornmane](/characters/babror_hornmane) | [Dwarf](/races/dwarf)     | 4.5.98 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Falelf Bjoehrsson](/characters/falelf_bjoehrsson) | [Nordic](/races/nordic)     | 10.3.3 BLC |
| 23 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Terin Fastclock](/characters/terin_fastclock) | [Gnome](/races/gnome)     | 22.1.92 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Herigar Biornolfrson](/characters/herigar_biornolfrson) | [Nordic](/races/nordic)     | 27.9.8 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Soldsim Heavyshield](/characters/soldsim_heavyshield) | [Nordic](/races/nordic)     | 19.11.12 BLC |

## Honours
