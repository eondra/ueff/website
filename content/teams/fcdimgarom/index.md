---
author: "UEFF"
title: "FC Dim Garom"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Club Dim Garom |
| Nation | [Vongram](/nations/vongram) |
| City | Dim Garom |
| Founded | 18.03.14 LC |
| Field | - |
| Head coach | [Jozaet Mithrilrock](/characters/jozaet_mithrilrock) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Urotha](/images/flags/urotha_small_md.png) | [Rohk Krofrod](/characters/rohk_krofrod) | [Orc](/races/orc)     | 12.8.0 LC |
| 2 |    Goalkeeper    | ![Vongram](/images/flags/vongram_small_md.png) | [Dhotgrous Bloodhide](/characters/dhotgrous_bloodhide) | [Dwarf](/races/dwarf)     | 17.2.53 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Alfognath Kybnamn](/characters/alfognath_kybnamn) | [Dwarf](/races/dwarf)     | 8.7.90 BLC |
| 4 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Dokmag Heavymane](/characters/dokmag_heavymane) | [Dwarf](/races/dwarf)     | 13.1.100 BLC |
| 5 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Urg Solidshout](/characters/urg_solidshout) | [Dwarf](/races/dwarf)     | 18.11.93 BLC |
| 6 |    Right Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Bakger Shatterheart](/characters/bakger_shatterheart) | [Dwarf](/races/dwarf)     | 15.6.73 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Ozar-toti Flamebeard](/characters/ozar-toti_flamebeard) | [Nordic](/races/nordic)     | 6.6.7 BLC |
| 8 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Drobryl Mithrilrock](/characters/drobryl_mithrilrock) | [Dwarf](/races/dwarf)     | 12.9.56 BLC |
| 9 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Thosdruth Ninbis](/characters/thosdruth_ninbis) | [Dwarf](/races/dwarf)     | 11.1.93 BLC |
| 10 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Mudmols Pebblefinger](/characters/mudmols_pebblefinger) | [Dwarf](/races/dwarf)     | 26.5.54 BLC |
| 11 |    Defender    | ![Vongram](/images/flags/vongram_small_md.png) | [Doyren Vobbor](/characters/doyren_vobbor) | [Dwarf](/races/dwarf)     | 6.1.61 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Loftr Soranson](/characters/loftr_soranson) | [Nordic](/races/nordic)     | 1.1.9 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Sral Blazecrusher](/characters/sral_blazecrusher) | [Dwarf](/races/dwarf)     | 26.10.80 BLC |
| 14 |    Right Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Grorraet Merrymail](/characters/grorraet_merrymail) | [Dwarf](/races/dwarf)     | 19.8.75 BLC |
| 15 |    Right Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Erirsam Woh](/characters/erirsam_woh) | [Dwarf](/races/dwarf)     | 11.12.72 BLC |
| 16 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Jagnig Steelfinger](/characters/jagnig_steelfinger) | [Dwarf](/races/dwarf)     | 3.3.81 BLC |
| 17 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Cekmam Hardminer](/characters/cekmam_hardminer) | [Dwarf](/races/dwarf)     | 25.6.89 BLC |
| 18 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Ralul Nokron](/characters/ralul_nokron) | [Dwarf](/races/dwarf)     | 4.6.57 BLC |
| 19 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Huzmorlum Scout](/characters/huzmorlum_scout) | [Dwarf](/races/dwarf)     | 27.6.83 BLC |
| 20 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Zadank Ariddrums](/characters/zadank_ariddrums) | [Orc](/races/orc)     | 21.10.1 LC |
| 21 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Exalted Flaskgranite](/characters/exalted_flaskgranite) | [Dwarf](/races/dwarf)     | 6.7.55 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Whukdrith Solvon](/characters/whukdrith_solvon) | [Dwarf](/races/dwarf)     | 27.7.99 BLC |
| 23 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Hidorg Pridedew](/characters/hidorg_pridedew) | [Dwarf](/races/dwarf)     | 25.8.89 BLC |
| 24 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Zatkon Sorgod](/characters/zatkon_sorgod) | [Dwarf](/races/dwarf)     | 26.7.92 BLC |
| 25 |    Attacker    | ![Vongram](/images/flags/vongram_small_md.png) | [Grorsk Medd](/characters/grorsk_medd) | [Dwarf](/races/dwarf)     | 14.1.52 BLC |

## Honours

