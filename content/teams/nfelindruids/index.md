---
author: "UEFF"
title: "Nfelin Druids"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Nfelin Druids |
| Nation | [Yllin](/nations/yllin) |
| City | Nfelin |
| Founded | 25.12.09 LC |
| Field | - |
| Head coach | [Cal Tus](/characters/cal_tus) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Afamrail Oridove](/characters/afamrail_oridove) | [Elf](/races/elf)     | 19.5.172 BLC |
| 2 |    Goalkeeper    | ![Yllin](/images/flags/yllin_small_md.png) | [Cess Silverstriker](/characters/cess_silverstriker) | [Elf](/races/elf)     | 26.9.160 BLC |
| 3 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Vurgolvrolsam Enfaren](/characters/vurgolvrolsam_enfaren) | [Elf](/races/elf)     | 5.2.162 BLC |
| 4 |    Left Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Sur Blackdancer](/characters/sur_blackdancer) | [Elf](/races/elf)     | 27.4.173 BLC |
| 5 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Ameanel Ziemnelmiol](/characters/ameanel_ziemnelmiol) | [Elf](/races/elf)     | 26.11.173 BLC |
| 6 |    Right Back    | ![Yllin](/images/flags/yllin_small_md.png) | [Haltho Lightmight](/characters/haltho_lightmight) | [Elf](/races/elf)     | 8.1.170 BLC |
| 7 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Sur Forestsinger](/characters/sur_forestsinger) | [Elf](/races/elf)     | 9.3.172 BLC |
| 8 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Ogtern Dhuvnid](/characters/ogtern_dhuvnid) | [Orc](/races/orc)     | 7.7.2 LC |
| 9 |    Defender    | ![Urotha](/images/flags/urotha_small_md.png) | [Datzes Gik](/characters/datzes_gik) | [Orc](/races/orc)     | 20.12.1 LC |
| 10 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Helnim Norqen](/characters/helnim_norqen) | [Elf](/races/elf)     | 24.7.170 BLC |
| 11 |    Defender    | ![Yllin](/images/flags/yllin_small_md.png) | [Thylallaeth Trueleaf](/characters/thylallaeth_trueleaf) | [Elf](/races/elf)     | 23.6.169 BLC |
| 12 |    Left Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Cam Zim](/characters/cam_zim) | [Elf](/races/elf)     | 6.2.163 BLC |
| 13 |    Left Winger    | ![Vongram](/images/flags/vongram_small_md.png) | [Grooneac Flaskdigger](/characters/grooneac_flaskdigger) | [Dwarf](/races/dwarf)     | 1.4.77 BLC |
| 14 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Nythaelath Skysinger](/characters/nythaelath_skysinger) | [Elf](/races/elf)     | 8.9.175 BLC |
| 15 |    Right Winger    | ![Yllin](/images/flags/yllin_small_md.png) | [Felaern Joliglel](/characters/felaern_joliglel) | [Elf](/races/elf)     | 22.5.161 BLC |
| 16 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Botluck Anglenozzle](/characters/botluck_anglenozzle) | [Gnome](/races/gnome)     | 5.2.85 BLC |
| 17 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Dadrannul Genrona](/characters/dadrannul_genrona) | [Elf](/races/elf)     | 8.12.179 BLC |
| 18 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Detis Zeth](/characters/detis_zeth) | [Orc](/races/orc)     | 25.8.3 LC |
| 19 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Eanthigrullom Wamalhnus](/characters/eanthigrullom_wamalhnus) | [Elf](/races/elf)     | 8.5.163 BLC |
| 20 |    Midfielder    | ![Yllin](/images/flags/yllin_small_md.png) | [Fenduyal Mistclouds](/characters/fenduyal_mistclouds) | [Elf](/races/elf)     | 21.3.173 BLC |
| 21 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Erarion Imis](/characters/erarion_imis) | [Elf](/races/elf)     | 24.2.172 BLC |
| 22 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Esadon Stagsnow](/characters/esadon_stagsnow) | [Elf](/races/elf)     | 22.2.177 BLC |
| 23 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Wirenth Sunbow](/characters/wirenth_sunbow) | [Elf](/races/elf)     | 20.12.172 BLC |
| 24 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Khidell Grom](/characters/khidell_grom) | [Elf](/races/elf)     | 22.2.168 BLC |
| 25 |    Attacker    | ![Yllin](/images/flags/yllin_small_md.png) | [Tum Tilsisail](/characters/tum_tilsisail) | [Elf](/races/elf)     | 7.1.176 BLC |

## Honours

