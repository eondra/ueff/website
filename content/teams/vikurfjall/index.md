---
author: "UEFF"
title: "Vikur Fjall"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Vikur Fjall |
| Nation | [Jamta](/nations/jamta) |
| City | Fjall |
| Founded | 07.05.05 LC |
| Field | - |
| Head coach | [Hawker-hedin Sohrerssen](/characters/hawker-hedin_sohrerssen) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Aevar Skalprson](/characters/aevar_skalprson) | [Nordic](/races/nordic)     | 4.12.14 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Ketilbjorn Deeproar](/characters/ketilbjorn_deeproar) | [Nordic](/races/nordic)     | 4.4.13 BLC |
| 3 |    Left Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Gutgi Heavyshield](/characters/gutgi_heavyshield) | [Nordic](/races/nordic)     | 24.6.10 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Helgard Sharpstrike](/characters/helgard_sharpstrike) | [Orc](/races/orc)     | 1.2.1 LC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Vermundr Sararoksson](/characters/vermundr_sararoksson) | [Nordic](/races/nordic)     | 25.2.2 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Bedleid Helmbolg](/characters/bedleid_helmbolg) | [Nordic](/races/nordic)     | 25.8.3 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Skjorgal Eylaugrson](/characters/skjorgal_eylaugrson) | [Nordic](/races/nordic)     | 3.11.13 BLC |
| 8 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Gnilavish Oilphase](/characters/gnilavish_oilphase) | [Gnome](/races/gnome)     | 19.6.87 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Jolgeirr Hersteinson](/characters/jolgeirr_hersteinson) | [Nordic](/races/nordic)     | 5.9.5 BLC |
| 10 |    Defender    | ![Katargo](/images/flags/katargo_small_md.png) | [Bumeezz Battlecount](/characters/bumeezz_battlecount) | [Gnome](/races/gnome)     | 7.8.75 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Joridall Early-gobler](/characters/joridall_early-gobler) | [Nordic](/races/nordic)     | 19.2.9 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Oxen-thorir Round-tooth](/characters/oxen-thorir_round-tooth) | [Nordic](/races/nordic)     | 25.12.13 BLC |
| 13 |    Left Winger    | ![Katargo](/images/flags/katargo_small_md.png) | [Vungarth Boulderchewer](/characters/vungarth_boulderchewer) | [Nordic](/races/nordic)     | 4.10.7 BLC |
| 14 |    Right Winger    | ![Urotha](/images/flags/urotha_small_md.png) | [Curkan Boldblade](/characters/curkan_boldblade) | [Orc](/races/orc)     | 25.1.3 LC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Hakang Skystalker](/characters/hakang_skystalker) | [Nordic](/races/nordic)     | 4.4.11 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Wilnolf Hrapprkin](/characters/wilnolf_hrapprkin) | [Nordic](/races/nordic)     | 6.9.8 BLC |
| 17 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Sigvellyn Throndrkin](/characters/sigvellyn_throndrkin) | [Nordic](/races/nordic)     | 2.6.14 BLC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Ollrdrir Night-mug](/characters/ollrdrir_night-mug) | [Nordic](/races/nordic)     | 25.1.7 BLC |
| 19 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Zigman Thim](/characters/zigman_thim) | [Orc](/races/orc)     | 3.7.1 BLC |
| 20 |    Midfielder    | ![Vongram](/images/flags/vongram_small_md.png) | [Srak Longmail](/characters/srak_longmail) | [Dwarf](/races/dwarf)     | 21.8.94 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Eldjmoor Fleetfoot](/characters/eldjmoor_fleetfoot) | [Nordic](/races/nordic)     | 13.2.12 BLC |
| 22 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Gonnthjar Easterner](/characters/gonnthjar_easterner) | [Nordic](/races/nordic)     | 30.7.15 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Vekoslod Gorefist](/characters/vekoslod_gorefist) | [Nordic](/races/nordic)     | 17.8.10 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Bronhorgh Thorgeirson](/characters/bronhorgh_thorgeirson) | [Nordic](/races/nordic)     | 11.11.13 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Asbrand Hjorleifson](/characters/asbrand_hjorleifson) | [Nordic](/races/nordic)     | 14.12.9 BLC |

## Honours
