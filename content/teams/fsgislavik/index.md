---
author: "UEFF"
title: "FS Gislavik"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

INSERT_BADGE

INSERT_HISTORY

## General info

| | |
|-|-|
| Name | Footorb Society Gislavik |
| Nation | [Jamta](/nations/jamta) |
| City | Gislavik |
| Founded | 14.06.10 LC |
| Field | - |
| Head coach | [Anicki Twistboss](/characters/anicki_twistboss) |


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
| 1 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Revatar Scar-cloak](/characters/revatar_scar-cloak) | [Nordic](/races/nordic)     | 18.6.4 BLC |
| 2 |    Goalkeeper    | ![Jamta](/images/flags/jamta_small_md.png) | [Brynof Ingialdrson](/characters/brynof_ingialdrson) | [Nordic](/races/nordic)     | 18.7.5 BLC |
| 3 |    Left Back    | ![Vongram](/images/flags/vongram_small_md.png) | [Haldrec Coppersunder](/characters/haldrec_coppersunder) | [Dwarf](/races/dwarf)     | 7.5.81 BLC |
| 4 |    Left Back    | ![Urotha](/images/flags/urotha_small_md.png) | [Stigngrim Heyjang-bjornnson](/characters/stigngrim_heyjang-bjornnson) | [Nordic](/races/nordic)     | 30.11.8 BLC |
| 5 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Osfrid Long-victim](/characters/osfrid_long-victim) | [Nordic](/races/nordic)     | 25.7.12 BLC |
| 6 |    Right Back    | ![Jamta](/images/flags/jamta_small_md.png) | [Klemuth Drunk-blood](/characters/klemuth_drunk-blood) | [Nordic](/races/nordic)     | 9.5.9 BLC |
| 7 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Gurd Hammer-fire](/characters/gurd_hammer-fire) | [Nordic](/races/nordic)     | 20.1.7 BLC |
| 8 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Rogearel Longbasher](/characters/rogearel_longbasher) | [Nordic](/races/nordic)     | 6.10.14 BLC |
| 9 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Loredar Thorgilson](/characters/loredar_thorgilson) | [Nordic](/races/nordic)     | 13.10.14 BLC |
| 10 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Skorageirr Herraudson](/characters/skorageirr_herraudson) | [Nordic](/races/nordic)     | 28.5.9 BLC |
| 11 |    Defender    | ![Jamta](/images/flags/jamta_small_md.png) | [Skapti Engmarikssen](/characters/skapti_engmarikssen) | [Nordic](/races/nordic)     | 9.6.7 BLC |
| 12 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Kolskeggr Salty-giver](/characters/kolskeggr_salty-giver) | [Nordic](/races/nordic)     | 22.1.15 BLC |
| 13 |    Left Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Ozur Bollason](/characters/ozur_bollason) | [Nordic](/races/nordic)     | 3.1.7 BLC |
| 14 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Kluwarinn Hlodverson](/characters/kluwarinn_hlodverson) | [Nordic](/races/nordic)     | 8.5.14 BLC |
| 15 |    Right Winger    | ![Jamta](/images/flags/jamta_small_md.png) | [Asbjorn Hjorleifson](/characters/asbjorn_hjorleifson) | [Nordic](/races/nordic)     | 19.5.6 BLC |
| 16 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Hosvir Seven-nail](/characters/hosvir_seven-nail) | [Nordic](/races/nordic)     | 16.4.7 BLC |
| 17 |    Midfielder    | ![Urotha](/images/flags/urotha_small_md.png) | [Ozkelt Velkihn](/characters/ozkelt_velkihn) | [Orc](/races/orc)     | 11.9.4 LC |
| 18 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Skarom Torradson](/characters/skarom_torradson) | [Nordic](/races/nordic)     | 19.1.9 BLC |
| 19 |    Midfielder    | ![Jamta](/images/flags/jamta_small_md.png) | [Sigvellyn Bonerage](/characters/sigvellyn_bonerage) | [Nordic](/races/nordic)     | 8.2.6 BLC |
| 20 |    Midfielder    | ![Katargo](/images/flags/katargo_small_md.png) | [Pinki Clickgrinder](/characters/pinki_clickgrinder) | [Gnome](/races/gnome)     | 26.9.96 BLC |
| 21 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Alldak Hrutson](/characters/alldak_hrutson) | [Nordic](/races/nordic)     | 3.7.12 BLC |
| 22 |    Attacker    | ![Katargo](/images/flags/katargo_small_md.png) | [Kluttlezz Strikepatch](/characters/kluttlezz_strikepatch) | [Gnome](/races/gnome)     | 22.4.78 BLC |
| 23 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Igmudic Stout-nail](/characters/igmudic_stout-nail) | [Nordic](/races/nordic)     | 22.7.11 BLC |
| 24 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Pall Halfdanson](/characters/pall_halfdanson) | [Nordic](/races/nordic)     | 15.9.4 BLC |
| 25 |    Attacker    | ![Jamta](/images/flags/jamta_small_md.png) | [Istlal Firehelm](/characters/istlal_firehelm) | [Nordic](/races/nordic)     | 9.7.4 BLC |

## Honours
