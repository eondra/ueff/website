---
author: "UEFF"
title: "Agmhff Veleifrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Agmhff Veleifrson |
| Date of birth | 7.7.6 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [Cogdale United](/teams/cogdaleunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale United](/teams/cogdaleunited) | 20 | | |

## Honours

