---
author: "UEFF"
title: "Douthack Nu"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Douthack Nu |
| Date of birth | 26.12.69 BLC |
| City of birth | [Esjuberg](/cities/esjuberg) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [FC Esjuberg](/teams/fcesjuberg) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Esjuberg](/teams/fcesjuberg) | 20 | | |

## Honours

