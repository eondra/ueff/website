---
author: "UEFF"
title: "Latkadi Trickcable"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Latkadi Trickcable |
| Date of birth | 19.11.61 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Gnome](/races/gnome) |
| Position | Left Winger |
| Current club | [Ullh Alari Guards](/teams/ullhalariguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Ullh Alari Guards](/teams/ullhalariguards) | 20 | | |

## Honours

