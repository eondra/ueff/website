---
author: "UEFF"
title: "Durizmith Timbin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Durizmith Timbin |
| Date of birth | 17.8.55 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [SC Kinbadur](/teams/sckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kinbadur](/teams/sckinbadur) | 20 | | |

## Honours

