---
author: "UEFF"
title: "Skadrim Darkridge"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Skadrim Darkridge |
| Date of birth | 16.3.64 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [FC Kinbadur](/teams/fckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kinbadur](/teams/fckinbadur) | 20 | | |

## Honours

