---
author: "UEFF"
title: "Gnilkees Blackfuzz"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gnilkees Blackfuzz |
| Date of birth | 19.6.102 BLC |
| City of birth | [Gearingfort](/cities/gearingfort) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [FC Gearingfort](/teams/fcgearingfort) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Gearingfort](/teams/fcgearingfort) | 20 | | |

## Honours

