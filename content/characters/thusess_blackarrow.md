---
author: "UEFF"
title: "Thusess Blackarrow"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thusess Blackarrow |
| Date of birth | 30.5.168 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Right Back |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

