---
author: "UEFF"
title: "Trosh Thud"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Trosh Thud |
| Date of birth | 3.4.2 LC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Qvadgad Wolves](/teams/qvadgadwolves) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Qvadgad Wolves](/teams/qvadgadwolves) | 20 | | |

## Honours

