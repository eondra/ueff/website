---
author: "UEFF"
title: "Enbazz Bellowbus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Enbazz Bellowbus |
| Date of birth | 27.7.60 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Winger |
| Current club | [FC Tinkerbury](/teams/fctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Tinkerbury](/teams/fctinkerbury) | 20 | | |

## Honours

