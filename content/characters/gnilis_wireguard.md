---
author: "UEFF"
title: "Gnilis Wireguard"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gnilis Wireguard |
| Date of birth | 13.1.74 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [Umberwatch Guards](/teams/umberwatchguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Umberwatch Guards](/teams/umberwatchguards) | 20 | | |

## Honours

