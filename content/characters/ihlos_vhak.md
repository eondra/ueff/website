---
author: "UEFF"
title: "Ihlos Vhak"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ihlos Vhak |
| Date of birth | 29.5.2 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [Warcry Redkug](/teams/warcryredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Warcry Redkug](/teams/warcryredkug) | 20 | | |

## Honours

