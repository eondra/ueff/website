---
author: "UEFF"
title: "Mirgin Rockbasher"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Mirgin Rockbasher |
| Date of birth | 22.8.4 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Warcry Redkug](/teams/warcryredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Warcry Redkug](/teams/warcryredkug) | 20 | | |

## Honours

