---
author: "UEFF"
title: "Gutgi Asgrimrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gutgi Asgrimrson |
| Date of birth | 13.7.7 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [Mount Kinbadur](/teams/mountkinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Mount Kinbadur](/teams/mountkinbadur) | 20 | | |

## Honours

