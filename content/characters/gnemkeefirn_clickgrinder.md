---
author: "UEFF"
title: "Gnemkeefirn Clickgrinder"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gnemkeefirn Clickgrinder |
| Date of birth | 18.2.95 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [FC Tinkerbury](/teams/fctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Tinkerbury](/teams/fctinkerbury) | 20 | | |

## Honours

