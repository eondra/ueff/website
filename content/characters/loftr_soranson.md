---
author: "UEFF"
title: "Loftr Soranson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Loftr Soranson |
| Date of birth | 1.1.9 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

