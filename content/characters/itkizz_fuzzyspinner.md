---
author: "UEFF"
title: "Itkizz Fuzzyspinner"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Itkizz Fuzzyspinner |
| Date of birth | 12.10.59 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [Vin Daral Grounders](/teams/vindaralgrounders) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Grounders](/teams/vindaralgrounders) | 20 | | |

## Honours

