---
author: "UEFF"
title: "Votgan Cavebrand"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Votgan Cavebrand |
| Date of birth | 10.7.76 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [Cogdale Scraps](/teams/cogdalescraps) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale Scraps](/teams/cogdalescraps) | 20 | | |

## Honours

