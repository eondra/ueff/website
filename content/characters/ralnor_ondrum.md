---
author: "UEFF"
title: "Ralnor Ondrum"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ralnor Ondrum |
| Date of birth | 14.7.175 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [SC Kyonore](/teams/sckyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kyonore](/teams/sckyonore) | 20 | | |

## Honours

