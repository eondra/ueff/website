---
author: "UEFF"
title: "Stigngrim Heyjang-bjornnson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Stigngrim Heyjang-bjornnson |
| Date of birth | 30.11.8 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Nordic](/races/nordic) |
| Position | Left Back |
| Current club | [FS Gislavik](/teams/fsgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Gislavik](/teams/fsgislavik) | 20 | | |

## Honours

