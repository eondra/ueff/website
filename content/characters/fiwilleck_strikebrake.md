---
author: "UEFF"
title: "Fiwilleck Strikebrake"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fiwilleck Strikebrake |
| Date of birth | 7.1.41 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [FA Tinkerbury](/teams/fatinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FA Tinkerbury](/teams/fatinkerbury) | 20 | | |

## Honours

