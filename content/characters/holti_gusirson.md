---
author: "UEFF"
title: "Holti Gusirson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Holti Gusirson |
| Date of birth | 16.11.3 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Aurioa SC](/teams/arioasc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Aurioa SC](/teams/arioasc) | 20 | | |

## Honours

