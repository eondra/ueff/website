---
author: "UEFF"
title: "Skearel Ulf-krakuson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Skearel Ulf-krakuson |
| Date of birth | 30.12.3 BLC |
| City of birth | [Barosvik](/cities/barosvik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

