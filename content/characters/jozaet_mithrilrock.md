---
author: "UEFF"
title: "Jozaet Mithrilrock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jozaet Mithrilrock |
| Date of birth | 24.3.54 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

