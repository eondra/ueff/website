---
author: "UEFF"
title: "Sildulf Hakakin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sildulf Hakakin |
| Date of birth | 17.7.8 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours
