---
author: "UEFF"
title: "Dalyor Zolmemam"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dalyor Zolmemam |
| Date of birth | 26.7.170 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [Ullh Alari Guards](/teams/ullhalariguards) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Ullh Alari Guards](/teams/ullhalariguards) | 20 | | |

## Honours

