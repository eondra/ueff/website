---
author: "UEFF"
title: "Cekmam Hardminer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cekmam Hardminer |
| Date of birth | 25.6.89 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

