---
author: "UEFF"
title: "Sur Forestsinger"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sur Forestsinger |
| Date of birth | 9.3.172 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

