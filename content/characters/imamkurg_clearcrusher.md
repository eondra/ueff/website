---
author: "UEFF"
title: "Imamkurg Clearcrusher"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Imamkurg Clearcrusher |
| Date of birth | 22.5.1 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) | 20 | | |

## Honours

