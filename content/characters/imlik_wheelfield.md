---
author: "UEFF"
title: "Imlik Wheelfield"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Imlik Wheelfield |
| Date of birth | 19.6.63 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Back |
| Current club | [SC Umberwatch](/teams/scumberwatch) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Umberwatch](/teams/scumberwatch) | 20 | | |

## Honours

