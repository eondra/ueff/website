---
author: "UEFF"
title: "Skamuth Stefnirkin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Skamuth Stefnirkin |
| Date of birth | 11.9.4 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Right Winger |
| Current club | [Umberwatch Guards](/teams/umberwatchguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Umberwatch Guards](/teams/umberwatchguards) | 20 | | |

## Honours

