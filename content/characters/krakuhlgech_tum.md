---
author: "UEFF"
title: "Krakuhlgech Tum"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krakuhlgech Tum |
| Date of birth | 13.2.0 LC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [ST Qvadgad](/teams/stqvadgad) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Qvadgad](/teams/stqvadgad) | 20 | | |

## Honours

