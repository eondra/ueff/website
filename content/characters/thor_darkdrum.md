---
author: "UEFF"
title: "Thor Darkdrum"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thor Darkdrum |
| Date of birth | 25.5.1 LC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [ST Qvadgad](/teams/stqvadgad) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Qvadgad](/teams/stqvadgad) | 20 | | |

## Honours

