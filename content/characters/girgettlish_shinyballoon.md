---
author: "UEFF"
title: "Girgettlish Shinyballoon"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Girgettlish Shinyballoon |
| Date of birth | 2.2.93 BLC |
| City of birth | [Gearingfort](/cities/gearingfort) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [Gearingfort Inc](/teams/gearingfortinc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gearingfort Inc](/teams/gearingfortinc) | 20 | | |

## Honours

