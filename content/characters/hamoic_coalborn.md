---
author: "UEFF"
title: "Hamoic Coalborn"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hamoic Coalborn |
| Date of birth | 1.7.63 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Goalkeeper |
| Current club | [Vin Daral Diamonds](/teams/vindaraldiamonds) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Diamonds](/teams/vindaraldiamonds) | 20 | | |

## Honours

