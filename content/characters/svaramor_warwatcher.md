---
author: "UEFF"
title: "Svaramor Warwatcher"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Svaramor Warwatcher |
| Date of birth | 12.1.4 BLC |
| City of birth | [Barosvik](/cities/barosvik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

