---
author: "UEFF"
title: "Krur Darkforest"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krur Darkforest |
| Date of birth | 5.3.166 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [BA Kyonore](/teams/bakyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BA Kyonore](/teams/bakyonore) | 20 | | |

## Honours

