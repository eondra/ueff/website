---
author: "UEFF"
title: "Sigfus Crag-mouth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sigfus Crag-mouth |
| Date of birth | 16.8.7 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [FC Odeila](/teams/fcodeila) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Odeila](/teams/fcodeila) | 20 | | |

## Honours

