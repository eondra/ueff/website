---
author: "UEFF"
title: "Itlizz Quietsignal"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Itlizz Quietsignal |
| Date of birth | 24.10.63 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [FC Naggar](/teams/fcnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Naggar](/teams/fcnaggar) | 20 | | |

## Honours

