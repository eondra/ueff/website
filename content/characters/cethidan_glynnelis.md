---
author: "UEFF"
title: "Cethidan Glynnelis"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cethidan Glynnelis |
| Date of birth | 12.7.179 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | League position |
|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

