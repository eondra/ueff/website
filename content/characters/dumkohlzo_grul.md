---
author: "UEFF"
title: "Dumkohlzo Grul"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dumkohlzo Grul |
| Date of birth | 8.12.3 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [SC Redkug](/teams/scredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Redkug](/teams/scredkug) | 20 | | |

## Honours

