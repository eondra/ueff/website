---
author: "UEFF"
title: "Cazrem Gravelbringer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cazrem Gravelbringer |
| Date of birth | 16.3.88 BLC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Back |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

