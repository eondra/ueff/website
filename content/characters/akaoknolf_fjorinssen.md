---
author: "UEFF"
title: "Akaoknolf Fjorinssen"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Akaoknolf Fjorinssen |
| Date of birth | 6.5.11 BLC |
| City of birth | [Barosvik](/cities/barosvik) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Nordic](/races/nordic) |
| Position | Right Winger |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

