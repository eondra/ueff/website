---
author: "UEFF"
title: "Brolt Brokendeath"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Brolt Brokendeath |
| Date of birth | 27.2.4 LC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

