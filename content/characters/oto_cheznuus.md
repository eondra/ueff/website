---
author: "UEFF"
title: "Oto Cheznuus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Oto Cheznuus |
| Date of birth | 9.11.2 BLC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [FT Vaz Ilkadh](/teams/ftvazilkadh) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Vaz Ilkadh](/teams/ftvazilkadh) | 20 | | |

## Honours

