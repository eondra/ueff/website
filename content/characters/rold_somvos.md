---
author: "UEFF"
title: "Rold Somvos"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rold Somvos |
| Date of birth | 18.8.61 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [FC Odeila](/teams/fcodeila) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Odeila](/teams/fcodeila) | 20 | | |

## Honours

