---
author: "UEFF"
title: "Babror Hornmane"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Babror Hornmane |
| Date of birth | 4.5.98 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [FS Dofrar](/teams/fsdofrar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Dofrar](/teams/fsdofrar) | 20 | | |

## Honours

