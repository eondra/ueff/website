---
author: "UEFF"
title: "Glitec Luckbadge"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Glitec Luckbadge |
| Date of birth | 14.1.69 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Goalkeeper |
| Current club | [GFC Chiselhaven](/teams/gfcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [GFC Chiselhaven](/teams/gfcchiselhaven) | 20 | | |

## Honours

