---
author: "UEFF"
title: "Kollsveinn Earthfist"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kollsveinn Earthfist |
| Date of birth | 20.11.3 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

