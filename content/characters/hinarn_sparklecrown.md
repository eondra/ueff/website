---
author: "UEFF"
title: "Hinarn Sparklecrown"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hinarn Sparklecrown |
| Date of birth | 3.10.47 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [FC Gearingfort](/teams/fcgearingfort) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Gearingfort](/teams/fcgearingfort) | 20 | | |

## Honours

