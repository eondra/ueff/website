---
author: "UEFF"
title: "Sradmom Flintheart"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sradmom Flintheart |
| Date of birth | 27.6.98 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [Vin Daral Diamonds](/teams/vindaraldiamonds) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Diamonds](/teams/vindaraldiamonds) | 20 | | |

## Honours

