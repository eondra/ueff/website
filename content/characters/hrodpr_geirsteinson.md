---
author: "UEFF"
title: "Hrodpr Geirsteinson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hrodpr Geirsteinson |
| Date of birth | 26.1.3 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

