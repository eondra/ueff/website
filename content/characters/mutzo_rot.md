---
author: "UEFF"
title: "Mutzo Rot"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Mutzo Rot |
| Date of birth | 27.2.2 BLC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Orc](/races/orc) |
| Position | Right Back |
| Current club | [Qvadgad Wolves](/teams/qvadgadwolves) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Qvadgad Wolves](/teams/qvadgadwolves) | 20 | | |

## Honours

