---
author: "UEFF"
title: "Woknem Blacksmith"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Woknem Blacksmith |
| Date of birth | 21.3.75 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [Kinbadur Rocks](/teams/kinbadurrocks) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kinbadur Rocks](/teams/kinbadurrocks) | 20 | | |

## Honours

