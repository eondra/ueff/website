---
author: "UEFF"
title: "Braellyn Steelwinds"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Braellyn Steelwinds |
| Date of birth | 2.4.14 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

