---
author: "UEFF"
title: "Tustear Shatterback"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tustear Shatterback |
| Date of birth | 1.9.73 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [United Dim Garom](/teams/uniteddimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [United Dim Garom](/teams/uniteddimgarom) | 20 | | |

## Honours

