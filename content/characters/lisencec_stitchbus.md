---
author: "UEFF"
title: "Lisencec Stitchbus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Lisencec Stitchbus |
| Date of birth | 27.6.82 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Back |
| Current club | [BB Chiselhaven](/teams/bbchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BB Chiselhaven](/teams/bbchiselhaven) | 20 | | |

## Honours

