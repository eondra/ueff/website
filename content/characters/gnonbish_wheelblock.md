---
author: "UEFF"
title: "Gnonbish Wheelblock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gnonbish Wheelblock |
| Date of birth | 23.6.54 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Winger |
| Current club | [SC Kinbadur](/teams/sckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kinbadur](/teams/sckinbadur) | 20 | | |

## Honours

