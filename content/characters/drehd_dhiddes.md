---
author: "UEFF"
title: "Drehd Dhiddes"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Drehd Dhiddes |
| Date of birth | 18.10.2 LC |
| City of birth | [Gnupar](/cities/gnupar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Gnupar Unit](/teams/gnuparunit) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gnupar Unit](/teams/gnuparunit) | 20 | | |

## Honours

