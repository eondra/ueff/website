---
author: "UEFF"
title: "Onund Blooddream"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Onund Blooddream |
| Date of birth | 29.7.11 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Iri Serin Alchemists](/teams/iriserinalchemists) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Iri Serin Alchemists](/teams/iriserinalchemists) | 20 | | |

## Honours

