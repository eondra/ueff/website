---
author: "UEFF"
title: "Svogferth Ehrarikesen"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Svogferth Ehrarikesen |
| Date of birth | 29.11.7 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Back |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

