---
author: "UEFF"
title: "Kraenithodvioss Fensys"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kraenithodvioss Fensys |
| Date of birth | 11.3.161 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

