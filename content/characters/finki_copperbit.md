---
author: "UEFF"
title: "Finki Copperbit"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Finki Copperbit |
| Date of birth | 6.6.55 BLC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FT Kruzaz](/teams/ftkruzaz) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FT Kruzaz](/teams/ftkruzaz) | 20 | | |

## Honours

