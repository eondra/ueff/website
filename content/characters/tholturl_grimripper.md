---
author: "UEFF"
title: "Tholturl Grimripper"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tholturl Grimripper |
| Date of birth | 13.8.3 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Orc](/races/orc) |
| Position | Left Winger |
| Current club | [FC Kruzaz](/teams/fckruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kruzaz](/teams/fckruzaz) | 20 | | |

## Honours

