---
author: "UEFF"
title: "Bomdon Moltenforge"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Bomdon Moltenforge |
| Date of birth | 21.1.82 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Goalkeeper |
| Current club | [Vin Daral Fighters](/teams/vindaralfighters) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Fighters](/teams/vindaralfighters) | 20 | | |

## Honours

