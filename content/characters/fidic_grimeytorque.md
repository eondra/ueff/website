---
author: "UEFF"
title: "Fidic Grimeytorque"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fidic Grimeytorque |
| Date of birth | 30.12.101 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Winger |
| Current club | [FC Tinkerbury](/teams/fctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Tinkerbury](/teams/fctinkerbury) | 20 | | |

## Honours

