---
author: "UEFF"
title: "Olvass Daglostior"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Olvass Daglostior |
| Date of birth | 18.2.178 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [SC Ullh Alari](/teams/scullhalari) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Ullh Alari](/teams/scullhalari) | 20 | | |

## Honours

