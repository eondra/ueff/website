---
author: "UEFF"
title: "Marntuch Gedrek"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Marntuch Gedrek |
| Date of birth | 15.3.4 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [SC Redkug](/teams/scredkug) |

## Club career

| Club | Season | League position |
|-|-|-|
| [SC Redkug](/teams/scredkug) | 20 | | |

## Honours

