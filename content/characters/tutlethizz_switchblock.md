---
author: "UEFF"
title: "Tutlethizz Switchblock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tutlethizz Switchblock |
| Date of birth | 12.8.62 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [1. SC Tinkerbury](/teams/1.sctinkerbury) |

## Club career

| Club | Season | League position |
|-|-|-|
| [1. SC Tinkerbury](/teams/1.sctinkerbury) | 20 | | |

## Honours

