---
author: "UEFF"
title: "Aladaen Wysaberos"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Aladaen Wysaberos |
| Date of birth | 7.10.172 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [Iri Serin Alchemists](/teams/iriserinalchemists) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Iri Serin Alchemists](/teams/iriserinalchemists) | 20 | | |

## Honours

