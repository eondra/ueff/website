---
author: "UEFF"
title: "Gizbul Goldbender"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gizbul Goldbender |
| Date of birth | 10.8.73 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

