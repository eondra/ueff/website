---
author: "UEFF"
title: "Bumeezz Battlecount"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Bumeezz Battlecount |
| Date of birth | 7.8.75 BLC |
| City of birth | [Fjall](/cities/fjall) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [Vikur Fjall](/teams/vikurfjall) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Fjall](/teams/vikurfjall) | 20 | | |

## Honours

