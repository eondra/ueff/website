---
author: "UEFF"
title: "Akaoknolf Kolson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Akaoknolf Kolson |
| Date of birth | 21.4.11 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [FC Kopa](/teams/fckopa) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kopa](/teams/fckopa) | 20 | | |

## Honours

