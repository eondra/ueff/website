---
author: "UEFF"
title: "Itlun Berrymix"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Itlun Berrymix |
| Date of birth | 28.2.71 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [Cogdale Scraps](/teams/cogdalescraps) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale Scraps](/teams/cogdalescraps) | 20 | | |

## Honours

