---
author: "UEFF"
title: "Ockeefok Clickcord"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ockeefok Clickcord |
| Date of birth | 31.12.105 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [Cogdale United](/teams/cogdaleunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale United](/teams/cogdaleunited) | 20 | | |

## Honours

