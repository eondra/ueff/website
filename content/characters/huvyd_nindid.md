---
author: "UEFF"
title: "Huvyd Nindid"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Huvyd Nindid |
| Date of birth | 6.3.65 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Back |
| Current club | [Vin Daral Diamonds](/teams/vindaraldiamonds) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Diamonds](/teams/vindaraldiamonds) | 20 | | |

## Honours

