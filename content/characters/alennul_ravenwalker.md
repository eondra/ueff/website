---
author: "UEFF"
title: "Alennul Ravenwalker"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Alennul Ravenwalker |
| Date of birth | 29.7.174 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours

