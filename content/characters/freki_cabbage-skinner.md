---
author: "UEFF"
title: "Freki Cabbage-skinner"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Freki Cabbage-skinner |
| Date of birth | 15.8.11 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [1. FC Gislavik](/teams/1.fcgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. FC Gislavik](/teams/1.fcgislavik) | 20 | | |

## Honours

