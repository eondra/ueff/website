---
author: "UEFF"
title: "Falt Darkpride"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Falt Darkpride |
| Date of birth | 22.10.1 BLC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

