---
author: "UEFF"
title: "Sardik Darkchain"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sardik Darkchain |
| Date of birth | 23.10.2 LC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [Vin Daral Fighters](/teams/vindaralfighters) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Fighters](/teams/vindaralfighters) | 20 | | |

## Honours

