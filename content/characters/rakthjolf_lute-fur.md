---
author: "UEFF"
title: "Rakthjolf Lute-fur"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rakthjolf Lute-fur |
| Date of birth | 17.4.6 BLC |
| City of birth | [Gnupar](/cities/gnupar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Gnupar Unit](/teams/gnuparunit) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Gnupar Unit](/teams/gnuparunit) | 20 | | |

## Honours

