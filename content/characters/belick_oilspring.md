---
author: "UEFF"
title: "Belick Oilspring"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Belick Oilspring |
| Date of birth | 22.5.54 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [Cogdale United](/teams/cogdaleunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale United](/teams/cogdaleunited) | 20 | | |

## Honours

