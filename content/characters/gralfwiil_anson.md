---
author: "UEFF"
title: "Gralfwiil Anson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gralfwiil Anson |
| Date of birth | 6.12.5 BLC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

