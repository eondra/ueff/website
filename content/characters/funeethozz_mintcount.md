---
author: "UEFF"
title: "Funeethozz Mintcount"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Funeethozz Mintcount |
| Date of birth | 1.1.44 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [1. SC Tinkerbury](/teams/1.sctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. SC Tinkerbury](/teams/1.sctinkerbury) | 20 | | |

## Honours

