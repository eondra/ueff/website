---
author: "UEFF"
title: "Kliklimkan Fixcub"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kliklimkan Fixcub |
| Date of birth | 19.10.95 BLC |
| City of birth | [Gearingfort](/cities/gearingfort) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FC Gearingfort](/teams/fcgearingfort) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Gearingfort](/teams/fcgearingfort) | 20 | | |

## Honours

