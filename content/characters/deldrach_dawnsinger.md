---
author: "UEFF"
title: "Deldrach Dawnsinger"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Deldrach Dawnsinger |
| Date of birth | 16.4.160 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

