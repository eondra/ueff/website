---
author: "UEFF"
title: "Gniles Dualbrake"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gniles Dualbrake |
| Date of birth | 19.3.105 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [SC Umberwatch](/teams/scumberwatch) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Umberwatch](/teams/scumberwatch) | 20 | | |

## Honours

