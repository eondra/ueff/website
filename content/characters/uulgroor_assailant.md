---
author: "UEFF"
title: "Uulgroor Assailant"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Uulgroor Assailant |
| Date of birth | 24.2.59 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [Dim Garom Diggers](/teams/dimgaromdiggers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Dim Garom Diggers](/teams/dimgaromdiggers) | 20 | | |

## Honours

