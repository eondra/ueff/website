---
author: "UEFF"
title: "Adept Ghodd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Adept Ghodd |
| Date of birth | 7.9.65 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [Vin Daral Fighters](/teams/vindaralfighters) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Fighters](/teams/vindaralfighters) | 20 | | |

## Honours

