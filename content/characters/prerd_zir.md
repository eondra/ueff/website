---
author: "UEFF"
title: "Prerd Zir"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Prerd Zir |
| Date of birth | 4.9.2 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Left Winger |
| Current club | [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) | 20 | | |

## Honours

