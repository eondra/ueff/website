---
author: "UEFF"
title: "Thordar Fridmundrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thordar Fridmundrson |
| Date of birth | 15.7.6 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Gnupar Unit](/teams/gnuparunit) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gnupar Unit](/teams/gnuparunit) | 20 | | |

## Honours

