---
author: "UEFF"
title: "Thorormr Stonegrip"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thorormr Stonegrip |
| Date of birth | 25.8.8 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours

