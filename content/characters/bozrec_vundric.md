---
author: "UEFF"
title: "Bozrec Vundric"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Bozrec Vundric |
| Date of birth | 22.11.96 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [Cogdale United](/teams/cogdaleunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale United](/teams/cogdaleunited) | 20 | | |

## Honours

