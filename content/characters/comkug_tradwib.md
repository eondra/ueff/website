---
author: "UEFF"
title: "Comkug Tradwib"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Comkug Tradwib |
| Date of birth | 15.2.88 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

