---
author: "UEFF"
title: "Jaonos Truesong"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jaonos Truesong |
| Date of birth | 1.5.170 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [SC Kyonore](/teams/sckyonore) |

## Club career

| Club | Season | League position |
|-|-|-|
| [SC Kyonore](/teams/sckyonore) | 20 | | |

## Honours

