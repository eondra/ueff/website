---
author: "UEFF"
title: "Linduzz Overflow"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Linduzz Overflow |
| Date of birth | 9.11.82 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [FC Chiselhaven](/teams/fcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Chiselhaven](/teams/fcchiselhaven) | 20 | | |

## Honours

