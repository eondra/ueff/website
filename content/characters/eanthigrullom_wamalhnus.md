---
author: "UEFF"
title: "Eanthigrullom Wamalhnus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Eanthigrullom Wamalhnus |
| Date of birth | 8.5.163 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

