---
author: "UEFF"
title: "Krirleek Pumpbonk"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krirleek Pumpbonk |
| Date of birth | 14.11.68 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FA Tinkerbury](/teams/fatinkerbury) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FA Tinkerbury](/teams/fatinkerbury) | 20 | | |

## Honours

