---
author: "UEFF"
title: "Krimlibronk Grindlocket"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krimlibronk Grindlocket |
| Date of birth | 22.8.83 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Winger |
| Current club | [FC Chiselhaven](/teams/fcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Chiselhaven](/teams/fcchiselhaven) | 20 | | |

## Honours

