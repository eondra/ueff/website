---
author: "UEFF"
title: "Naghrenun Feinuker"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Naghrenun Feinuker |
| Date of birth | 28.6.180 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [SC Ullh Alari](/teams/scullhalari) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Ullh Alari](/teams/scullhalari) | 20 | | |

## Honours

