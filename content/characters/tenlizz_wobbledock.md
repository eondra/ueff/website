---
author: "UEFF"
title: "Tenlizz Wobbledock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tenlizz Wobbledock |
| Date of birth | 28.1.59 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Back |
| Current club | [FA Tinkerbury](/teams/fatinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FA Tinkerbury](/teams/fatinkerbury) | 20 | | |

## Honours

