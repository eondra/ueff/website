---
author: "UEFF"
title: "Kastuth Ermon"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kastuth Ermon |
| Date of birth | 16.1.3 LC |
| City of birth | [Esjuberg](/cities/esjuberg) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [FC Esjuberg](/teams/fcesjuberg) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Esjuberg](/teams/fcesjuberg) | 20 | | |

## Honours

