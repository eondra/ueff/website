---
author: "UEFF"
title: "Evedolar Rainblade"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Evedolar Rainblade |
| Date of birth | 19.11.162 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

