---
author: "UEFF"
title: "Bulvud Darkrock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Bulvud Darkrock |
| Date of birth | 16.5.89 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours

