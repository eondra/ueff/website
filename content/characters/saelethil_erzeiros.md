---
author: "UEFF"
title: "Saelethil Erzeiros"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Saelethil Erzeiros |
| Date of birth | 18.6.177 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

