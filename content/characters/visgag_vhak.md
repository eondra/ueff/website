---
author: "UEFF"
title: "Visgag Vhak"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Visgag Vhak |
| Date of birth | 1.12.1 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [FC Kruzaz](/teams/fckruzaz) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Kruzaz](/teams/fckruzaz) | 20 | | |

## Honours

