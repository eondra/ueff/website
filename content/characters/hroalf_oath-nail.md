---
author: "UEFF"
title: "Hroalf Oath-nail"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hroalf Oath-nail |
| Date of birth | 23.9.3 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [1. FC Gislavik](/teams/1.fcgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. FC Gislavik](/teams/1.fcgislavik) | 20 | | |

## Honours

