---
author: "UEFF"
title: "Klekewizz Squiggleclue"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Klekewizz Squiggleclue |
| Date of birth | 11.10.64 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Back |
| Current club | [SC Ullh Alari](/teams/scullhalari) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Ullh Alari](/teams/scullhalari) | 20 | | |

## Honours

