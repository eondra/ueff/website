---
author: "UEFF"
title: "Helnim Norqen"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Helnim Norqen |
| Date of birth | 24.7.170 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

