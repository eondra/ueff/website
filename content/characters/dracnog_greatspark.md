---
author: "UEFF"
title: "Dracnog Greatspark"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dracnog Greatspark |
| Date of birth | 8.6.95 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [FC Vin Daral](/teams/fcvindaral) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Vin Daral](/teams/fcvindaral) | 20 | | |

## Honours

