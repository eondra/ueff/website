---
author: "UEFF"
title: "Ramosh Primeblade"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ramosh Primeblade |
| Date of birth | 31.12.1 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [FT Kruzaz](/teams/ftkruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Kruzaz](/teams/ftkruzaz) | 20 | | |

## Honours

