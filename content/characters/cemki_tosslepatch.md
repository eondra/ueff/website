---
author: "UEFF"
title: "Cemki Tosslepatch"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cemki Tosslepatch |
| Date of birth | 10.1.44 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [FC Chiselhaven](/teams/fcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Chiselhaven](/teams/fcchiselhaven) | 20 | | |

## Honours

