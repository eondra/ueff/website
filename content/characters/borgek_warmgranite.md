---
author: "UEFF"
title: "Borgek Warmgranite"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Borgek Warmgranite |
| Date of birth | 24.3.99 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [United Dim Garom](/teams/uniteddimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [United Dim Garom](/teams/uniteddimgarom) | 20 | | |

## Honours

