---
author: "UEFF"
title: "Anicki Twistboss"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Anicki Twistboss |
| Date of birth | 5.8.51 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FS Gislavik](/teams/fsgislavik) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FS Gislavik](/teams/fsgislavik) | 20 | | |

## Honours

