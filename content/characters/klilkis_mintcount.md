---
author: "UEFF"
title: "Klilkis Mintcount"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Klilkis Mintcount |
| Date of birth | 9.1.91 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Back |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours

