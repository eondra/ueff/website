---
author: "UEFF"
title: "Taghahd Dragonsorrow"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Taghahd Dragonsorrow |
| Date of birth | 29.3.3 LC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Orc](/races/orc) |
| Position | Left Back |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

