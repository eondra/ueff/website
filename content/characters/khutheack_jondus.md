---
author: "UEFF"
title: "Khutheack Jondus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Khutheack Jondus |
| Date of birth | 31.5.80 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

