---
author: "UEFF"
title: "Ioldrird Runwimn"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ioldrird Runwimn |
| Date of birth | 23.10.56 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [Vin Daral Grounders](/teams/vindaralgrounders) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Grounders](/teams/vindaralgrounders) | 20 | | |

## Honours

