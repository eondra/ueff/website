---
author: "UEFF"
title: "Klolkeesh Twistcount"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Klolkeesh Twistcount |
| Date of birth | 7.6.64 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FC Chiselhaven](/teams/fcchiselhaven) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Chiselhaven](/teams/fcchiselhaven) | 20 | | |

## Honours

