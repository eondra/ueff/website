---
author: "UEFF"
title: "Tiosolon Kas"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tiosolon Kas |
| Date of birth | 6.12.161 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Right Winger |
| Current club | [SC Kyonore](/teams/sckyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kyonore](/teams/sckyonore) | 20 | | |

## Honours

