---
author: "UEFF"
title: "Grum Kehd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Grum Kehd |
| Date of birth | 1.1.0 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Back |
| Current club | [FC Kruzaz](/teams/fckruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kruzaz](/teams/fckruzaz) | 20 | | |

## Honours

