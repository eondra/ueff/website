---
author: "UEFF"
title: "Hawker-hedin Sohrerssen"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hawker-hedin Sohrerssen |
| Date of birth | 17.8.9 BLC |
| City of birth | [Fjall](/cities/fjall) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Vikur Fjall](/teams/vikurfjall) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Vikur Fjall](/teams/vikurfjall) | 20 | | |

## Honours

