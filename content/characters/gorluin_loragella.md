---
author: "UEFF"
title: "Gorluin Loragella"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gorluin Loragella |
| Date of birth | 5.7.169 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [BA Kyonore](/teams/bakyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BA Kyonore](/teams/bakyonore) | 20 | | |

## Honours

