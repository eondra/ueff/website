---
author: "UEFF"
title: "Zolvegar Umetoris"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Zolvegar Umetoris |
| Date of birth | 18.1.176 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

