---
author: "UEFF"
title: "Noraruri Getnes"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Noraruri Getnes |
| Date of birth | 7.1.91 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [FC Vin Daral](/teams/fcvindaral) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Vin Daral](/teams/fcvindaral) | 20 | | |

## Honours

