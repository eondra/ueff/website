---
author: "UEFF"
title: "Soxolfr Mojarkesson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Soxolfr Mojarkesson |
| Date of birth | 3.2.9 BLC |
| City of birth | [Gnupar](/cities/gnupar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [FC Kopa](/teams/fckopa) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kopa](/teams/fckopa) | 20 | | |

## Honours

