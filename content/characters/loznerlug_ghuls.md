---
author: "UEFF"
title: "Loznerlug Ghuls"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Loznerlug Ghuls |
| Date of birth | 19.5.68 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [Dim Garom Diggers](/teams/dimgaromdiggers) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Dim Garom Diggers](/teams/dimgaromdiggers) | 20 | | |

## Honours

