---
author: "UEFF"
title: "Jolgeirr Hersteinson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jolgeirr Hersteinson |
| Date of birth | 5.9.5 BLC |
| City of birth | [Fjall](/cities/fjall) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Vikur Fjall](/teams/vikurfjall) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Fjall](/teams/vikurfjall) | 20 | | |

## Honours

