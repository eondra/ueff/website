---
author: "UEFF"
title: "Sniolf Two-lute"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sniolf Two-lute |
| Date of birth | 28.3.11 BLC |
| City of birth | [Barosvik](/cities/barosvik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [FC Esjuberg](/teams/fcesjuberg) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Esjuberg](/teams/fcesjuberg) | 20 | | |

## Honours

