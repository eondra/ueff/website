---
author: "UEFF"
title: "Galyllian Steinason"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Galyllian Steinason |
| Date of birth | 24.1.14 BLC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Qvadgad Wolves](/teams/qvadgadwolves) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Qvadgad Wolves](/teams/qvadgadwolves) | 20 | | |

## Honours

