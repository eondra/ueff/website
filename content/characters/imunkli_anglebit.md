---
author: "UEFF"
title: "Imunkli Anglebit"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Imunkli Anglebit |
| Date of birth | 28.7.104 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Gnome](/races/gnome) |
| Position | Left Back |
| Current club | [1. SC Tinkerbury](/teams/1.sctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. SC Tinkerbury](/teams/1.sctinkerbury) | 20 | | |

## Honours

