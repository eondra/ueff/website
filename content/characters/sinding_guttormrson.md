---
author: "UEFF"
title: "Sinding Guttormrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sinding Guttormrson |
| Date of birth | 27.1.3 BLC |
| City of birth | [Esjuberg](/cities/esjuberg) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Left Back |
| Current club | [FC Esjuberg](/teams/fcesjuberg) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Esjuberg](/teams/fcesjuberg) | 20 | | |

## Honours

