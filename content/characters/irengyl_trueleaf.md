---
author: "UEFF"
title: "Irengyl Trueleaf"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Irengyl Trueleaf |
| Date of birth | 13.4.170 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Attacker |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours

