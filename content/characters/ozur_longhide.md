---
author: "UEFF"
title: "Ozur Longhide"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ozur Longhide |
| Date of birth | 12.10.10 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [FC Kinbadur](/teams/fckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kinbadur](/teams/fckinbadur) | 20 | | |

## Honours

