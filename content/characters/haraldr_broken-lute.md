---
author: "UEFF"
title: "Haraldr Broken-lute"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Haraldr Broken-lute |
| Date of birth | 4.9.6 BLC |
| City of birth | [Esjuberg](/cities/esjuberg) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [FS Dofrar](/teams/fsdofrar) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FS Dofrar](/teams/fsdofrar) | 20 | | |

## Honours

