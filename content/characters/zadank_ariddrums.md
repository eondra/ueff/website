---
author: "UEFF"
title: "Zadank Ariddrums"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Zadank Ariddrums |
| Date of birth | 21.10.1 LC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

