---
author: "UEFF"
title: "Hlore Clanwinds"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hlore Clanwinds |
| Date of birth | 29.5.2 BLC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

