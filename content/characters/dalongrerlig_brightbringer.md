---
author: "UEFF"
title: "Dalongrerlig Brightbringer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dalongrerlig Brightbringer |
| Date of birth | 31.12.84 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

