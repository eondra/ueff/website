---
author: "UEFF"
title: "Braellyn Shadowsword"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Braellyn Shadowsword |
| Date of birth | 2.7.14 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [FS Dofrar](/teams/fsdofrar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Dofrar](/teams/fsdofrar) | 20 | | |

## Honours

