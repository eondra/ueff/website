---
author: "UEFF"
title: "Krustham Grus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krustham Grus |
| Date of birth | 18.1.177 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

