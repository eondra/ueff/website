---
author: "UEFF"
title: "Ull Hakason"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ull Hakason |
| Date of birth | 16.7.13 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

