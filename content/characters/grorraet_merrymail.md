---
author: "UEFF"
title: "Grorraet Merrymail"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Grorraet Merrymail |
| Date of birth | 19.8.75 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

