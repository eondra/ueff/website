---
author: "UEFF"
title: "Incis Coilchin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Incis Coilchin |
| Date of birth | 21.8.81 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Back |
| Current club | [USC Theveluma](/teams/usctheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | 20 | | |

## Honours

