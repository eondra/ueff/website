---
author: "UEFF"
title: "Gnemkeefirn Mekkaballoon"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gnemkeefirn Mekkaballoon |
| Date of birth | 20.12.41 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Right Back |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

