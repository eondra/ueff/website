---
author: "UEFF"
title: "Whukdrith Solvon"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Whukdrith Solvon |
| Date of birth | 27.7.99 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

