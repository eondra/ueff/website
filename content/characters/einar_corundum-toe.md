---
author: "UEFF"
title: "Einar Corundum-toe"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Einar Corundum-toe |
| Date of birth | 8.7.9 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Nordic](/races/nordic) |
| Position | Left Back |
| Current club | [FC Kopa](/teams/fckopa) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kopa](/teams/fckopa) | 20 | | |

## Honours

