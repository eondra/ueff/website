---
author: "UEFF"
title: "Anam Fenkrana"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Anam Fenkrana |
| Date of birth | 11.10.162 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [SC Ullh Alari](/teams/scullhalari) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Ullh Alari](/teams/scullhalari) | 20 | | |

## Honours

