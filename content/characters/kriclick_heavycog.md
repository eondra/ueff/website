---
author: "UEFF"
title: "Kriclick Heavycog"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kriclick Heavycog |
| Date of birth | 3.3.99 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [GFC Chiselhaven](/teams/gfcchiselhaven) |

## Club career

| Club | Season | League position |
|-|-|-|
| [GFC Chiselhaven](/teams/gfcchiselhaven) | 20 | | |

## Honours

