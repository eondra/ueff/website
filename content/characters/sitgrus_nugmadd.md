---
author: "UEFF"
title: "Sitgrus Nugmadd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sitgrus Nugmadd |
| Date of birth | 15.1.98 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [Mount Kinbadur](/teams/mountkinbadur) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Mount Kinbadur](/teams/mountkinbadur) | 20 | | |

## Honours

