---
author: "UEFF"
title: "Thocki Flickercount"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thocki Flickercount |
| Date of birth | 3.9.54 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [GFC Chiselhaven](/teams/gfcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [GFC Chiselhaven](/teams/gfcchiselhaven) | 20 | | |

## Honours

