---
author: "UEFF"
title: "Efinuck Bizzwhistle"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Efinuck Bizzwhistle |
| Date of birth | 31.5.105 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Gnome](/races/gnome) |
| Position | Goalkeeper |
| Current club | [BB Chiselhaven](/teams/bbchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BB Chiselhaven](/teams/bbchiselhaven) | 20 | | |

## Honours

