---
author: "UEFF"
title: "Krorg Verrus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krorg Verrus |
| Date of birth | 19.4.2 LC |
| City of birth | [Gearingfort](/cities/gearingfort) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Back |
| Current club | [FC Gearingfort](/teams/fcgearingfort) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Gearingfort](/teams/fcgearingfort) | 20 | | |

## Honours

