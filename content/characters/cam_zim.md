---
author: "UEFF"
title: "Cam Zim"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cam Zim |
| Date of birth | 6.2.163 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Winger |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

