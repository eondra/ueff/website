---
author: "UEFF"
title: "Grerni Grimil"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Grerni Grimil |
| Date of birth | 2.2.3 LC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

