---
author: "UEFF"
title: "Krorngon-digg Vuut"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krorngon-digg Vuut |
| Date of birth | 12.4.1 LC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [ST Qvadgad](/teams/stqvadgad) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Qvadgad](/teams/stqvadgad) | 20 | | |

## Honours
