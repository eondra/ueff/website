---
author: "UEFF"
title: "Brukdarn Evnath"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Brukdarn Evnath |
| Date of birth | 20.10.1 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Orc](/races/orc) |
| Position | Goalkeeper |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

