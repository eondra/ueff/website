---
author: "UEFF"
title: "Agmaild Tiorvakin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Agmaild Tiorvakin |
| Date of birth | 26.3.5 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [FC Odeila](/teams/fcodeila) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Odeila](/teams/fcodeila) | 20 | | |

## Honours

