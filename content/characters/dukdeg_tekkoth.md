---
author: "UEFF"
title: "Dukdeg Tekkoth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dukdeg Tekkoth |
| Date of birth | 14.8.81 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [United Dim Garom](/teams/uniteddimgarom) |

## Club career

| Club | Season | League position |
|-|-|-|
| [United Dim Garom](/teams/uniteddimgarom) | 20 | | |

## Honours

