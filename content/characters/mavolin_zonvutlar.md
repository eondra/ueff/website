---
author: "UEFF"
title: "Mavolin Zonvutlar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Mavolin Zonvutlar |
| Date of birth | 14.7.169 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

