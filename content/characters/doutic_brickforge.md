---
author: "UEFF"
title: "Doutic Brickforge"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Doutic Brickforge |
| Date of birth | 16.5.100 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours

