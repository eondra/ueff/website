---
author: "UEFF"
title: "Vemdud Bristlearm"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Vemdud Bristlearm |
| Date of birth | 16.4.89 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [Mount Kinbadur](/teams/mountkinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Mount Kinbadur](/teams/mountkinbadur) | 20 | | |

## Honours

