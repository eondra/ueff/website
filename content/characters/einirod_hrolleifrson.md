---
author: "UEFF"
title: "Einirod Hrolleifrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Einirod Hrolleifrson |
| Date of birth | 4.4.11 BLC |
| City of birth | [Aurioa](/cities/aurioa) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Aurioa SC](/teams/arioasc) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Aurioa SC](/teams/arioasc) | 20 | | |

## Honours

