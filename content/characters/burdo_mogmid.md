---
author: "UEFF"
title: "Burdo Mogmid"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Burdo Mogmid |
| Date of birth | 27.5.2 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [FA Tinkerbury](/teams/fatinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FA Tinkerbury](/teams/fatinkerbury) | 20 | | |

## Honours

