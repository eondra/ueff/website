---
author: "UEFF"
title: "Barildrack Orebender"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Barildrack Orebender |
| Date of birth | 2.12.67 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [FC Kinbadur](/teams/fckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kinbadur](/teams/fckinbadur) | 20 | | |

## Honours

