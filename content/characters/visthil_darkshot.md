---
author: "UEFF"
title: "Visthil Darkshot"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Visthil Darkshot |
| Date of birth | 11.2.180 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours

