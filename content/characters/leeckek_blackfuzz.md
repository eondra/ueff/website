---
author: "UEFF"
title: "Leeckek Blackfuzz"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Leeckek Blackfuzz |
| Date of birth | 11.4.71 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [Umberwatch Guards](/teams/umberwatchguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Umberwatch Guards](/teams/umberwatchguards) | 20 | | |

## Honours

