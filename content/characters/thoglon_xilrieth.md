---
author: "UEFF"
title: "Thoglon Xilrieth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Thoglon Xilrieth |
| Date of birth | 31.8.177 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

