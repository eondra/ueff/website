---
author: "UEFF"
title: "Gurbed Battlehand"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gurbed Battlehand |
| Date of birth | 28.4.74 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Back |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

