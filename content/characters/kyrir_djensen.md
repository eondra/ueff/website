---
author: "UEFF"
title: "Kyrir Djensen"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kyrir Djensen |
| Date of birth | 15.4.3 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Right Back |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

