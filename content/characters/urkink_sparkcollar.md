---
author: "UEFF"
title: "Urkink Sparkcollar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Urkink Sparkcollar |
| Date of birth | 7.5.103 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [SC Redkug](/teams/scredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Redkug](/teams/scredkug) | 20 | | |

## Honours

