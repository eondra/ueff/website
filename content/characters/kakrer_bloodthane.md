---
author: "UEFF"
title: "Kakrer Bloodthane"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kakrer Bloodthane |
| Date of birth | 1.6.92 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [United Dim Garom](/teams/uniteddimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [United Dim Garom](/teams/uniteddimgarom) | 20 | | |

## Honours

