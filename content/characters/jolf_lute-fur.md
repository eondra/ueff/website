---
author: "UEFF"
title: "Jolf Lute-fur"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jolf Lute-fur |
| Date of birth | 21.2.6 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [FC Odeila](/teams/fcodeila) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Odeila](/teams/fcodeila) | 20 | | |

## Honours

