---
author: "UEFF"
title: "Tuhoss Jem"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tuhoss Jem |
| Date of birth | 17.10.168 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Elf](/races/elf) |
| Position | Goalkeeper |
| Current club | [USC Theveluma](/teams/usctheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | 20 | | |

## Honours

