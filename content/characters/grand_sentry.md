---
author: "UEFF"
title: "Grand Sentry"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Grand Sentry |
| Date of birth | 5.4.53 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

