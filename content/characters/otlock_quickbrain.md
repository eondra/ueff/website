---
author: "UEFF"
title: "Otlock Quickbrain"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Otlock Quickbrain |
| Date of birth | 8.10.91 BLC |
| City of birth | [Gearingfort](/cities/gearingfort) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [Gnupar Unit](/teams/gnuparunit) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gnupar Unit](/teams/gnuparunit) | 20 | | |

## Honours

