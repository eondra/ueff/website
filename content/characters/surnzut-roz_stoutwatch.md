---
author: "UEFF"
title: "Surnzut-roz Stoutwatch"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Surnzut-roz Stoutwatch |
| Date of birth | 10.1.0 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [ST Redkug](/teams/stredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Redkug](/teams/stredkug) | 20 | | |

## Honours
