---
author: "UEFF"
title: "Omnoud Flamebreath"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Omnoud Flamebreath |
| Date of birth | 3.4.62 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [Vin Daral Diamonds](/teams/vindaraldiamonds) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Diamonds](/teams/vindaraldiamonds) | 20 | | |

## Honours

