---
author: "UEFF"
title: "Sirnir Swiftroar"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sirnir Swiftroar |
| Date of birth | 22.7.5 BLC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

