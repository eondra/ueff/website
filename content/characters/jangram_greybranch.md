---
author: "UEFF"
title: "Jangram Greybranch"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jangram Greybranch |
| Date of birth | 10.11.98 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [SC Kinbadur](/teams/sckinbadur) |

## Club career

| Club | Season | League position |
|-|-|-|
| [SC Kinbadur](/teams/sckinbadur) | 20 | | |

## Honours

