---
author: "UEFF"
title: "Zatkon Sorgod"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Zatkon Sorgod |
| Date of birth | 26.7.92 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

