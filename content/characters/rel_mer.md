---
author: "UEFF"
title: "Rel Mer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rel Mer |
| Date of birth | 12.8.4 LC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [FC Naggar](/teams/fcnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Naggar](/teams/fcnaggar) | 20 | | |

## Honours

