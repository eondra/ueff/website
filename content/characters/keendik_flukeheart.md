---
author: "UEFF"
title: "Keendik Flukeheart"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Keendik Flukeheart |
| Date of birth | 14.8.83 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [Gearingfort Inc](/teams/gearingfortinc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gearingfort Inc](/teams/gearingfortinc) | 20 | | |

## Honours

