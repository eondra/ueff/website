---
author: "UEFF"
title: "Rihk Ret"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rihk Ret |
| Date of birth | 17.10.2 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [FT Vaz Ilkadh](/teams/ftvazilkadh) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Vaz Ilkadh](/teams/ftvazilkadh) | 20 | | |

## Honours

