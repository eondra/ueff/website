---
author: "UEFF"
title: "Ozar-toti Flamebeard"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ozar-toti Flamebeard |
| Date of birth | 6.6.7 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [FC Dim Garom](/teams/fcdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Dim Garom](/teams/fcdimgarom) | 20 | | |

## Honours

