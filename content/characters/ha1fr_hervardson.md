---
author: "UEFF"
title: "Ha1fr Hervardson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ha1fr Hervardson |
| Date of birth | 7.9.10 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [SC Kinbadur](/teams/sckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kinbadur](/teams/sckinbadur) | 20 | | |

## Honours

