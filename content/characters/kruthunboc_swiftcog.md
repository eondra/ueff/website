---
author: "UEFF"
title: "Kruthunboc Swiftcog"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kruthunboc Swiftcog |
| Date of birth | 5.6.64 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [SC Umberwatch](/teams/scumberwatch) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Umberwatch](/teams/scumberwatch) | 20 | | |

## Honours

