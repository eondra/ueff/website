---
author: "UEFF"
title: "Grust Mezohd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Grust Mezohd |
| Date of birth | 23.11.1 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

