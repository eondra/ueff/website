---
author: "UEFF"
title: "Ugus Glynven"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ugus Glynven |
| Date of birth | 16.2.175 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [Iri Serin Alchemists](/teams/iriserinalchemists) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Iri Serin Alchemists](/teams/iriserinalchemists) | 20 | | |

## Honours

