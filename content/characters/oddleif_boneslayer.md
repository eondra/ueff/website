---
author: "UEFF"
title: "Oddleif Boneslayer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Oddleif Boneslayer |
| Date of birth | 20.2.4 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

