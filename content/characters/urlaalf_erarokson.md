---
author: "UEFF"
title: "Urlaalf Erarokson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Urlaalf Erarokson |
| Date of birth | 20.4.13 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [FS Dofrar](/teams/fsdofrar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Dofrar](/teams/fsdofrar) | 20 | | |

## Honours

