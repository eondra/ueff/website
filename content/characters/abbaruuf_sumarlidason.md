---
author: "UEFF"
title: "Abbaruuf Sumarlidason"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Abbaruuf Sumarlidason |
| Date of birth | 9.1.14 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [FT Kruzaz](/teams/ftkruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Kruzaz](/teams/ftkruzaz) | 20 | | |

## Honours

