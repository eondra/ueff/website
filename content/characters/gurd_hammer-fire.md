---
author: "UEFF"
title: "Gurd Hammer-fire"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gurd Hammer-fire |
| Date of birth | 20.1.7 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [FS Gislavik](/teams/fsgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Gislavik](/teams/fsgislavik) | 20 | | |

## Honours

