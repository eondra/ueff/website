---
author: "UEFF"
title: "Strodaeck Giantbraid"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Strodaeck Giantbraid |
| Date of birth | 13.9.87 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [SC Kyonore](/teams/sckyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kyonore](/teams/sckyonore) | 20 | | |

## Honours

