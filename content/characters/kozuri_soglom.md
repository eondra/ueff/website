---
author: "UEFF"
title: "Kozuri Soglom"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kozuri Soglom |
| Date of birth | 11.11.93 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Defender |
| Current club | [Ullh Alari Guards](/teams/ullhalariguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Ullh Alari Guards](/teams/ullhalariguards) | 20 | | |

## Honours

