---
author: "UEFF"
title: "Gautr Hand-sot"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gautr Hand-sot |
| Date of birth | 27.7.3 BLC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Aurioa SC](/teams/arioasc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Aurioa SC](/teams/arioasc) | 20 | | |

## Honours

