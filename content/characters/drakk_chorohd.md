---
author: "UEFF"
title: "Drakk Chorohd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Drakk Chorohd |
| Date of birth | 29.9.1 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [FT Kruzaz](/teams/ftkruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Kruzaz](/teams/ftkruzaz) | 20 | | |

## Honours

