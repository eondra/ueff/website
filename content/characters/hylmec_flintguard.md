---
author: "UEFF"
title: "Hylmec Flintguard"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hylmec Flintguard |
| Date of birth | 19.8.87 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [Dim Garom Diggers](/teams/dimgaromdiggers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Dim Garom Diggers](/teams/dimgaromdiggers) | 20 | | |

## Honours

