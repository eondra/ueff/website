---
author: "UEFF"
title: "Odan Carnelis"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Odan Carnelis |
| Date of birth | 26.7.163 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Elf](/races/elf) |
| Position | Left Winger |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

