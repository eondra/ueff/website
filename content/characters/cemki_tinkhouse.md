---
author: "UEFF"
title: "Cemki Tinkhouse"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cemki Tinkhouse |
| Date of birth | 8.4.79 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Back |
| Current club | [Cogdale Scraps](/teams/cogdalescraps) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Cogdale Scraps](/teams/cogdalescraps) | 20 | | |

## Honours

