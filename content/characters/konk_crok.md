---
author: "UEFF"
title: "Konk Crok"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Konk Crok |
| Date of birth | 31.10.4 LC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [ST Qvadgad](/teams/stqvadgad) |

## Club career

| Club | Season | League position |
|-|-|-|
| [ST Qvadgad](/teams/stqvadgad) | 20 | | |

## Honours

