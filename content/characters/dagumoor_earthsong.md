---
author: "UEFF"
title: "Dagumoor Earthsong"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dagumoor Earthsong |
| Date of birth | 10.10.7 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [Kinbadur Rocks](/teams/kinbadurrocks) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kinbadur Rocks](/teams/kinbadurrocks) | 20 | | |

## Honours

