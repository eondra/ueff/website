---
author: "UEFF"
title: "Hrani Salt-hater"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hrani Salt-hater |
| Date of birth | 27.4.15 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [GFC Chiselhaven](/teams/gfcchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [GFC Chiselhaven](/teams/gfcchiselhaven) | 20 | | |

## Honours

