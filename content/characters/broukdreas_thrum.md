---
author: "UEFF"
title: "Broukdreas Thrum"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Broukdreas Thrum |
| Date of birth | 3.9.71 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [Mount Kinbadur](/teams/mountkinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Mount Kinbadur](/teams/mountkinbadur) | 20 | | |

## Honours

