---
author: "UEFF"
title: "Dan Drakespine"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Dan Drakespine |
| Date of birth | 10.6.84 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [Iri Serin Alchemists](/teams/iriserinalchemists) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Iri Serin Alchemists](/teams/iriserinalchemists) | 20 | | |

## Honours

