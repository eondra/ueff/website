---
author: "UEFF"
title: "Arnvid Thorolfrkin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Arnvid Thorolfrkin |
| Date of birth | 5.5.3 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

