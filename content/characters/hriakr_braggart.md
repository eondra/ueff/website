---
author: "UEFF"
title: "Hriakr Braggart"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hriakr Braggart |
| Date of birth | 4.2.8 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [1. FC Gislavik](/teams/1.fcgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. FC Gislavik](/teams/1.fcgislavik) | 20 | | |

## Honours

