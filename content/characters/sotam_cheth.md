---
author: "UEFF"
title: "Sotam Cheth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sotam Cheth |
| Date of birth | 1.3.1 BLC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Goalkeeper |
| Current club | [Qvadgad Wolves](/teams/qvadgadwolves) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Qvadgad Wolves](/teams/qvadgadwolves) | 20 | | |

## Honours

