---
author: "UEFF"
title: "Kzizcish Thoveth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kzizcish Thoveth |
| Date of birth | 9.5.1 BLC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) | 20 | | |

## Honours

