---
author: "UEFF"
title: "Elephon Phixina"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Elephon Phixina |
| Date of birth | 17.3.173 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

