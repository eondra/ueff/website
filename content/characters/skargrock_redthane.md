---
author: "UEFF"
title: "Skargrock Redthane"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Skargrock Redthane |
| Date of birth | 1.10.99 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [FC Vin Daral](/teams/fcvindaral) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Vin Daral](/teams/fcvindaral) | 20 | | |

## Honours

