---
author: "UEFF"
title: "Mogoth Isnoth"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Mogoth Isnoth |
| Date of birth | 1.11.3 LC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [Vin Daral Diamonds](/teams/vindaraldiamonds) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Diamonds](/teams/vindaraldiamonds) | 20 | | |

## Honours

