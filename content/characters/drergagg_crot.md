---
author: "UEFF"
title: "Drergagg Crot"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Drergagg Crot |
| Date of birth | 25.10.3 LC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Left Back |
| Current club | [SC Redkug](/teams/scredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Redkug](/teams/scredkug) | 20 | | |

## Honours

