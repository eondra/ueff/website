---
author: "UEFF"
title: "Haim Treasurejaw"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Haim Treasurejaw |
| Date of birth | 17.8.79 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [Kinbadur Rocks](/teams/kinbadurrocks) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Kinbadur Rocks](/teams/kinbadurrocks) | 20 | | |

## Honours

