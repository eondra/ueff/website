---
author: "UEFF"
title: "Strad Sror"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Strad Sror |
| Date of birth | 7.9.66 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [United Dim Garom](/teams/uniteddimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [United Dim Garom](/teams/uniteddimgarom) | 20 | | |

## Honours

