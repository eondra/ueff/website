---
author: "UEFF"
title: "Cesern Laughingtale"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cesern Laughingtale |
| Date of birth | 5.8.2 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | League position |
|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours

