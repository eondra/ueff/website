---
author: "UEFF"
title: "Binkis Wrenchgrinder"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Binkis Wrenchgrinder |
| Date of birth | 10.4.64 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [FA Tinkerbury](/teams/fatinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FA Tinkerbury](/teams/fatinkerbury) | 20 | | |

## Honours

