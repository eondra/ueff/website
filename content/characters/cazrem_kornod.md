---
author: "UEFF"
title: "Cazrem Kornod"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cazrem Kornod |
| Date of birth | 18.2.95 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | League position |
|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

