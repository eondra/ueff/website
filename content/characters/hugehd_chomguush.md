---
author: "UEFF"
title: "Hugehd Chomguush"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hugehd Chomguush |
| Date of birth | 11.10.2 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vaz Ilkadh Warriors](/teams/vazilkadhwarriors) | 20 | | |

## Honours

