---
author: "UEFF"
title: "Cemkin Berrydock"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cemkin Berrydock |
| Date of birth | 5.3.72 BLC |
| City of birth | [Chiselhaven](/cities/chiselhaven) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [BB Chiselhaven](/teams/bbchiselhaven) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BB Chiselhaven](/teams/bbchiselhaven) | 20 | | |

## Honours

