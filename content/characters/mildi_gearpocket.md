---
author: "UEFF"
title: "Mildi Gearpocket"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Mildi Gearpocket |
| Date of birth | 19.6.54 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [SC Umberwatch](/teams/scumberwatch) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Umberwatch](/teams/scumberwatch) | 20 | | |

## Honours

