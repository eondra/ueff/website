---
author: "UEFF"
title: "Filis Sparkgear"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Filis Sparkgear |
| Date of birth | 31.7.92 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Attacker |
| Current club | [Umberwatch Guards](/teams/umberwatchguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Umberwatch Guards](/teams/umberwatchguards) | 20 | | |

## Honours

