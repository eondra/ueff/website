---
author: "UEFF"
title: "Sun Rigmuak"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sun Rigmuak |
| Date of birth | 15.2.1 BLC |
| City of birth | [Redkug](/cities/redkug) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [Warcry Redkug](/teams/warcryredkug) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Warcry Redkug](/teams/warcryredkug) | 20 | | |

## Honours

