---
author: "UEFF"
title: "Hori Grimeyhouse"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hori Grimeyhouse |
| Date of birth | 28.10.98 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [FC Tinkerbury](/teams/fctinkerbury) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Tinkerbury](/teams/fctinkerbury) | 20 | | |

## Honours

