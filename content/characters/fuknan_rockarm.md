---
author: "UEFF"
title: "Fuknan Rockarm"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fuknan Rockarm |
| Date of birth | 22.7.85 BLC |
| City of birth | [Aurioa](/cities/aurioa) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [Aurioa SC](/teams/arioasc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Aurioa SC](/teams/arioasc) | 20 | | |

## Honours

