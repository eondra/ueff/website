---
author: "UEFF"
title: "Broutik Gomn"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Broutik Gomn |
| Date of birth | 17.1.79 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [FC Kinbadur](/teams/fckinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kinbadur](/teams/fckinbadur) | 20 | | |

## Honours

