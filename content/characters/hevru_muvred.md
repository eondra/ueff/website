---
author: "UEFF"
title: "Hevru Muvred"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hevru Muvred |
| Date of birth | 10.11.4 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Left Back |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

