---
author: "UEFF"
title: "Rulkurn Okihd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rulkurn Okihd |
| Date of birth | 15.4.3 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Goalkeeper |
| Current club | [FT Vaz Ilkadh](/teams/ftvazilkadh) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FT Vaz Ilkadh](/teams/ftvazilkadh) | 20 | | |

## Honours

