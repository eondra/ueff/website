---
author: "UEFF"
title: "Hunass Herdithas"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hunass Herdithas |
| Date of birth | 8.9.174 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [Ullh Alari Guards](/teams/ullhalariguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Ullh Alari Guards](/teams/ullhalariguards) | 20 | | |

## Honours

