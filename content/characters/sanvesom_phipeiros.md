---
author: "UEFF"
title: "Sanvesom Phipeiros"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Sanvesom Phipeiros |
| Date of birth | 7.3.167 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [Ullh Alari Guards](/teams/ullhalariguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Ullh Alari Guards](/teams/ullhalariguards) | 20 | | |

## Honours

