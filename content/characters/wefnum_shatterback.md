---
author: "UEFF"
title: "Wefnum Shatterback"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Wefnum Shatterback |
| Date of birth | 18.1.82 BLC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Back |
| Current club | [FC Kruzaz](/teams/fckruzaz) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Kruzaz](/teams/fckruzaz) | 20 | | |

## Honours

