---
author: "UEFF"
title: "Razron Narsadd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Razron Narsadd |
| Date of birth | 12.5.85 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [Vin Daral Grounders](/teams/vindaralgrounders) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Grounders](/teams/vindaralgrounders) | 20 | | |

## Honours

