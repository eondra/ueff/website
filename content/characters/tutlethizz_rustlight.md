---
author: "UEFF"
title: "Tutlethizz Rustlight"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Tutlethizz Rustlight |
| Date of birth | 5.4.80 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [Cogdale United](/teams/cogdaleunited) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Cogdale United](/teams/cogdaleunited) | 20 | | |

## Honours

