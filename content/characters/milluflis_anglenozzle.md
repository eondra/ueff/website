---
author: "UEFF"
title: "Milluflis Anglenozzle"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Milluflis Anglenozzle |
| Date of birth | 8.11.75 BLC |
| City of birth | [Cogdale](/cities/cogdale) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Coach |
| Current club | [Cogdale Scraps](/teams/cogdalescraps) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Cogdale Scraps](/teams/cogdalescraps) | 20 | | |

## Honours

