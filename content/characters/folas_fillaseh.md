---
author: "UEFF"
title: "Folas Fillaseh"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Folas Fillaseh |
| Date of birth | 9.1.170 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Elf](/races/elf) |
| Position | Attacker |
| Current club | [BA Kyonore](/teams/bakyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [BA Kyonore](/teams/bakyonore) | 20 | | |

## Honours

