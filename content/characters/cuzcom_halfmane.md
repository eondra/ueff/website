---
author: "UEFF"
title: "Cuzcom Halfmane"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cuzcom Halfmane |
| Date of birth | 2.9.1 LC |
| City of birth | [Vaz Ilkadh](/cities/vazilkadh) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours

