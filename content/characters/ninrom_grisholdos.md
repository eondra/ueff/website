---
author: "UEFF"
title: "Ninrom Grisholdos"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ninrom Grisholdos |
| Date of birth | 24.7.179 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Defender |
| Current club | [USC Theveluma](/teams/usctheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | 20 | | |

## Honours

