---
author: "UEFF"
title: "Chrimdall Jolgeirson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Chrimdall Jolgeirson |
| Date of birth | 1.5.12 BLC |
| City of birth | [Gautland](/cities/gautland) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [Vikur Gautland](/teams/vikurgautland) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Gautland](/teams/vikurgautland) | 20 | | |

## Honours

