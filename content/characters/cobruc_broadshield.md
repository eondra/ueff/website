---
author: "UEFF"
title: "Cobruc Broadshield"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cobruc Broadshield |
| Date of birth | 28.3.83 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [Vin Daral Grounders](/teams/vindaralgrounders) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vin Daral Grounders](/teams/vindaralgrounders) | 20 | | |

## Honours

