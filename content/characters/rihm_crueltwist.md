---
author: "UEFF"
title: "Rihm Crueltwist"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Rihm Crueltwist |
| Date of birth | 11.4.1 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [FS Illa Ennore](/teams/fsillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Illa Ennore](/teams/fsillaennore) | 20 | | |

## Honours

