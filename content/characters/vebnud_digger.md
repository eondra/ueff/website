---
author: "UEFF"
title: "Vebnud Digger"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Vebnud Digger |
| Date of birth | 8.2.64 BLC |
| City of birth | [Haell](/cities/haell) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Left Winger |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

