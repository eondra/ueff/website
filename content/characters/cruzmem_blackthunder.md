---
author: "UEFF"
title: "Cruzmem Blackthunder"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cruzmem Blackthunder |
| Date of birth | 10.4.1 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Winger |
| Current club | [SC Dim Garom](/teams/scdimgarom) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Dim Garom](/teams/scdimgarom) | 20 | | |

## Honours

