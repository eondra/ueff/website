---
author: "UEFF"
title: "Ogtern Dhuvnid"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ogtern Dhuvnid |
| Date of birth | 7.7.2 LC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

