---
author: "UEFF"
title: "Vungarth Boulderchewer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Vungarth Boulderchewer |
| Date of birth | 4.10.7 BLC |
| City of birth | [Fjall](/cities/fjall) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Nordic](/races/nordic) |
| Position | Left Winger |
| Current club | [Vikur Fjall](/teams/vikurfjall) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Vikur Fjall](/teams/vikurfjall) | 20 | | |

## Honours

