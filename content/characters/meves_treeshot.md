---
author: "UEFF"
title: "Meves Treeshot"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Meves Treeshot |
| Date of birth | 11.12.179 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

