---
author: "UEFF"
title: "Jokul Vandilson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Jokul Vandilson |
| Date of birth | 3.2.8 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [FS Dofrar](/teams/fsdofrar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Dofrar](/teams/fsdofrar) | 20 | | |

## Honours

