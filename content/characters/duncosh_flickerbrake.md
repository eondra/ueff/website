---
author: "UEFF"
title: "Duncosh Flickerbrake"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Duncosh Flickerbrake |
| Date of birth | 2.8.92 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Midfielder |
| Current club | [FC Vin Daral](/teams/fcvindaral) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Vin Daral](/teams/fcvindaral) | 20 | | |

## Honours

