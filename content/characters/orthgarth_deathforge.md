---
author: "UEFF"
title: "Orthgarth Deathforge"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Orthgarth Deathforge |
| Date of birth | 27.1.15 BLC |
| City of birth | [Aurioa](/cities/aurioa) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Right Back |
| Current club | [ST Qvadgad](/teams/stqvadgad) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Qvadgad](/teams/stqvadgad) | 20 | | |

## Honours

