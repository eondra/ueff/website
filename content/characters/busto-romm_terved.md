---
author: "UEFF"
title: "Busto-romm Terved"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Busto-romm Terved |
| Date of birth | 11.10.1 LC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Right Back |
| Current club | [ST Naggar](/teams/stnaggar) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Naggar](/teams/stnaggar) | 20 | | |

## Honours
