---
author: "UEFF"
title: "Bengrorlum Grandgem"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Bengrorlum Grandgem |
| Date of birth | 21.2.86 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [FC Kinbadur](/teams/fckinbadur) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Kinbadur](/teams/fckinbadur) | 20 | | |

## Honours

