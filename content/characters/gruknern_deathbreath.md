---
author: "UEFF"
title: "Gruknern Deathbreath"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gruknern Deathbreath |
| Date of birth | 3.4.1 BLC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [SC Redkug](/teams/scredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Redkug](/teams/scredkug) | 20 | | |

## Honours

