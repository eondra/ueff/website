---
author: "UEFF"
title: "Strul Sahudd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Strul Sahudd |
| Date of birth | 4.4.58 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Midfielder |
| Current club | [Kinbadur Rocks](/teams/kinbadurrocks) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kinbadur Rocks](/teams/kinbadurrocks) | 20 | | |

## Honours

