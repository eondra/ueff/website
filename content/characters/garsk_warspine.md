---
author: "UEFF"
title: "Garsk Warspine"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Garsk Warspine |
| Date of birth | 29.8.83 BLC |
| City of birth | [Vin Daral](/cities/vindaral) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Coach |
| Current club | [Vin Daral Fighters](/teams/vindaralfighters) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Vin Daral Fighters](/teams/vindaralfighters) | 20 | | |

## Honours

