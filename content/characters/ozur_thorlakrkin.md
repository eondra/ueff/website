---
author: "UEFF"
title: "Ozur Thorlakrkin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ozur Thorlakrkin |
| Date of birth | 13.1.5 BLC |
| City of birth | [Barosvik](/cities/barosvik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Attacker |
| Current club | [Barosvik Hammers](/teams/barosvikhammers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Barosvik Hammers](/teams/barosvikhammers) | 20 | | |

## Honours

