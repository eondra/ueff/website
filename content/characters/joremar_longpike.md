---
author: "UEFF"
title: "Joremar Longpike"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Joremar Longpike |
| Date of birth | 22.11.8 BLC |
| City of birth | [Dofrar](/cities/dofrar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [1. FC Gislavik](/teams/1.fcgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. FC Gislavik](/teams/1.fcgislavik) | 20 | | |

## Honours

