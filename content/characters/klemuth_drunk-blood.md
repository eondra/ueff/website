---
author: "UEFF"
title: "Klemuth Drunk-blood"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Klemuth Drunk-blood |
| Date of birth | 9.5.9 BLC |
| City of birth | [Gislavik](/cities/gislavik) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Right Back |
| Current club | [FS Gislavik](/teams/fsgislavik) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FS Gislavik](/teams/fsgislavik) | 20 | | |

## Honours

