---
author: "UEFF"
title: "Krigust Shadowheart"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krigust Shadowheart |
| Date of birth | 2.4.2 BLC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Midfielder |
| Current club | [Umberwatch Guards](/teams/umberwatchguards) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Umberwatch Guards](/teams/umberwatchguards) | 20 | | |

## Honours

