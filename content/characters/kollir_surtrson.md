---
author: "UEFF"
title: "Kollir Surtrson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kollir Surtrson |
| Date of birth | 20.5.10 BLC |
| City of birth | [Tinkerbury](/cities/tinkerbury) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Right Back |
| Current club | [1. SC Tinkerbury](/teams/1.sctinkerbury) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [1. SC Tinkerbury](/teams/1.sctinkerbury) | 20 | | |

## Honours

