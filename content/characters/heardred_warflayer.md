---
author: "UEFF"
title: "Heardred Warflayer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Heardred Warflayer |
| Date of birth | 15.6.2 BLC |
| City of birth | [Iri Serin](/cities/iriserin) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [FC Iri Serin](/teams/fciriserin) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Iri Serin](/teams/fciriserin) | 20 | | |

## Honours

