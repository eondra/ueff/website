---
author: "UEFF"
title: "Krakliwuk Pullboss"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Krakliwuk Pullboss |
| Date of birth | 23.7.99 BLC |
| City of birth | [Illa Ennore](/cities/illaennore) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Left Winger |
| Current club | [FC Illa Ennore](/teams/fcillaennore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [FC Illa Ennore](/teams/fcillaennore) | 20 | | |

## Honours

