---
author: "UEFF"
title: "Cal Tus"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Cal Tus |
| Date of birth | 17.1.163 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [Nfelin Druids](/teams/nfelindruids) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Nfelin Druids](/teams/nfelindruids) | 20 | | |

## Honours

