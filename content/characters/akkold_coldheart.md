---
author: "UEFF"
title: "Akkold Coldheart"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Akkold Coldheart |
| Date of birth | 8.1.3 LC |
| City of birth | [Kruzaz](/cities/kruzaz) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

