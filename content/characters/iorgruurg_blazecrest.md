---
author: "UEFF"
title: "Iorgruurg Blazecrest"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Iorgruurg Blazecrest |
| Date of birth | 17.7.93 BLC |
| City of birth | [Dim Garom](/cities/dimgarom) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Attacker |
| Current club | [Dim Garom Diggers](/teams/dimgaromdiggers) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Dim Garom Diggers](/teams/dimgaromdiggers) | 20 | | |

## Honours

