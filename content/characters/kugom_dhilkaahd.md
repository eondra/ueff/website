---
author: "UEFF"
title: "Kugom Dhilkaahd"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kugom Dhilkaahd |
| Date of birth | 6.5.3 LC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Attacker |
| Current club | [USC Theveluma](/teams/usctheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | 20 | | |

## Honours

