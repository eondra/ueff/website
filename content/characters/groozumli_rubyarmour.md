---
author: "UEFF"
title: "Groozumli Rubyarmour"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Groozumli Rubyarmour |
| Date of birth | 24.11.60 BLC |
| City of birth | [Kinbadur](/cities/kinbadur) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Back |
| Current club | [Mount Kinbadur](/teams/mountkinbadur) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Mount Kinbadur](/teams/mountkinbadur) | 20 | | |

## Honours

