---
author: "UEFF"
title: "Kyg Fullshade"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Kyg Fullshade |
| Date of birth | 24.5.58 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Vongram](/images/flags/vongram_small_md.png) [Vongram](/nations/vongram) |
| Race | [Dwarf](/races/dwarf) |
| Position | Right Winger |
| Current club | [SC Ullh Alari](/teams/scullhalari) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Ullh Alari](/teams/scullhalari) | 20 | | |

## Honours

