---
author: "UEFF"
title: "Ovrirer Ambergrove"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Ovrirer Ambergrove |
| Date of birth | 30.9.178 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Midfielder |
| Current club | [USC Theveluma](/teams/usctheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [USC Theveluma](/teams/usctheveluma) | 20 | | |

## Honours

