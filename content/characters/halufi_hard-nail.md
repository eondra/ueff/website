---
author: "UEFF"
title: "Halufi Hard-nail"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Halufi Hard-nail |
| Date of birth | 22.8.14 BLC |
| City of birth | [Kopareykir](/cities/kopareykir) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [FC Kopa](/teams/fckopa) |

## Club career

| Club | Season | League position |
|-|-|-|
| [FC Kopa](/teams/fckopa) | 20 | | |

## Honours

