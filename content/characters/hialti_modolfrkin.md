---
author: "UEFF"
title: "Hialti Modolfrkin"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Hialti Modolfrkin |
| Date of birth | 29.8.12 BLC |
| City of birth | [Gnupar](/cities/gnupar) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Defender |
| Current club | [Gnupar Unit](/teams/gnuparunit) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Gnupar Unit](/teams/gnuparunit) | 20 | | |

## Honours

