---
author: "UEFF"
title: "Gudbrand Viga-glumson"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Gudbrand Viga-glumson |
| Date of birth | 10.4.11 BLC |
| City of birth | [Odeila](/cities/odeila) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Strig Haell](/teams/strighaell) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Strig Haell](/teams/strighaell) | 20 | | |

## Honours

