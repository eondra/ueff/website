---
author: "UEFF"
title: "Vungarth Milk-healer"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Vungarth Milk-healer |
| Date of birth | 21.9.11 BLC |
| City of birth | [Theveluma](/cities/theveluma) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Goalkeeper |
| Current club | [MA Theveluma](/teams/matheveluma) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [MA Theveluma](/teams/matheveluma) | 20 | | |

## Honours

