---
author: "UEFF"
title: "Lokihor Lightshield"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Lokihor Lightshield |
| Date of birth | 27.9.4 BLC |
| City of birth | [Esjuberg](/cities/esjuberg) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Coach |
| Current club | [Qvadgad Wolves](/teams/qvadgadwolves) |

## Club career

| Club | Season | League position |
|-|-|-|
| [Qvadgad Wolves](/teams/qvadgadwolves) | 20 | | |

## Honours

