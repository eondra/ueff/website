---
author: "UEFF"
title: "Theodemar Ravaxalim"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Theodemar Ravaxalim |
| Date of birth | 13.2.175 BLC |
| City of birth | [Ullh Alari](/cities/ullhalari) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Left Back |
| Current club | [SC Kyonore](/teams/sckyonore) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [SC Kyonore](/teams/sckyonore) | 20 | | |

## Honours

