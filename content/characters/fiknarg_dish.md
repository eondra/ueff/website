---
author: "UEFF"
title: "Fiknarg Dish"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fiknarg Dish |
| Date of birth | 25.7.1 BLC |
| City of birth | [Qvadgad](/cities/qvadgad) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Defender |
| Current club | [Kruzaz Spears](/teams/kruzazspears) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Kruzaz Spears](/teams/kruzazspears) | 20 | | |

## Honours

