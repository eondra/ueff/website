---
author: "UEFF"
title: "Samm Thul"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Samm Thul |
| Date of birth | 8.1.1 LC |
| City of birth | [Umberwatch](/cities/umberwatch) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Coach |
| Current club | [SC Umberwatch](/teams/scumberwatch) |

## Club career

| Club | Season | League position |
|-|-|-|
| [SC Umberwatch](/teams/scumberwatch) | 20 | | |

## Honours

