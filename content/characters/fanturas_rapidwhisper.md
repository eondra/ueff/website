---
author: "UEFF"
title: "Fanturas Rapidwhisper"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fanturas Rapidwhisper |
| Date of birth | 13.10.173 BLC |
| City of birth | [Kyonore](/cities/kyonore) |
| Nationality | ![Yllin](/images/flags/yllin_small_md.png) [Yllin](/nations/yllin) |
| Race | [Elf](/races/elf) |
| Position | Coach |
| Current club | [BA Kyonore](/teams/bakyonore) |

## Club career

| Club | Season | League position |
|-|-|-|
| [BA Kyonore](/teams/bakyonore) | 20 | | |

## Honours

