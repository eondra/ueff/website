---
author: "UEFF"
title: "Fiwilleck Slipneedle"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Fiwilleck Slipneedle |
| Date of birth | 26.1.57 BLC |
| City of birth | [Nfelin](/cities/nfelin) |
| Nationality | ![Katargo](/images/flags/katargo_small_md.png) [Katargo](/nations/katargo) |
| Race | [Gnome](/races/gnome) |
| Position | Defender |
| Current club | [Nfelin United](/teams/nfelinunited) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Nfelin United](/teams/nfelinunited) | 20 | | |

## Honours

