---
author: "UEFF"
title: "Marntuch Angersteel"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Marntuch Angersteel |
| Date of birth | 21.12.4 LC |
| City of birth | [Naggar](/cities/naggar) |
| Nationality | ![Urotha](/images/flags/urotha_small_md.png) [Urotha](/nations/urotha) |
| Race | [Orc](/races/orc) |
| Position | Goalkeeper |
| Current club | [ST Redkug](/teams/stredkug) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [ST Redkug](/teams/stredkug) | 20 | | |

## Honours

