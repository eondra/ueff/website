---
author: "UEFF"
title: "Assuke Hard-nail"
date: 2017-04-02T12:00:06+01:00
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | Assuke Hard-nail |
| Date of birth | 30.5.5 BLC |
| City of birth | [Aurioa](/cities/aurioa) |
| Nationality | ![Jamta](/images/flags/jamta_small_md.png) [Jamta](/nations/jamta) |
| Race | [Nordic](/races/nordic) |
| Position | Midfielder |
| Current club | [Aurioa SC](/teams/arioasc) |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [Aurioa SC](/teams/arioasc) | 20 | | |

## Honours

