---
author: "UEFF"
title: "UEFF Rules"
date: 2017-05-22T13:00:06+01:00
draft: false
type: "showcase"
---

## UEFF Board and national board

| Nation | Name |
|-|-|
| ![Jamta](/images/flags/jamta_small_md.png) | Gogrsgar Kjorinsson |
| ![Katargo](/images/flags/katargo_small_md.png) | Kletleek Copperbells |
| ![Urotha](/images/flags/urotha_small_md.png) | Dusdern Vullil |
| ![Vongram](/images/flags/vongram_small_md.png) | Nedman Woldbrand |
| ![Yllin](/images/flags/yllin_small_md.png) | Cerelrol Kelrieth |

## Joining the UEFF as a team
Every team that wants to join the UEFF, must meet the following requirements
- A coach
- A minimum of 15 and maximum of 25 registered players
- A Footorb field that can be played on regularly
- Funds to cover the travel cost to away games (Guards, Transport, ...)
- Willingness to travel to other nations

## Footorb Rules
- Footorb is played in 2 halves, being 45 minutes per half
  - 15 minutes of break are allowed between halves
  - Teams change their playing sides between halves
- If the Footorb crosses the goal-line between the goal-post and the crossbar, it counts as a goal
- The Footorb is not allowed to contain any kind of magic
- No player is allowed to use any kind of magic that interfere with the game
- The maximum number of players on the field per team is 11
- The Footorb is not allowed to be played with the use of hands (except for the goalkeeper)
- The goalkeeper is only allowed to use his hands inside the 16 branches long zone
- If the ball leaves the side-line of the field, a player has to throw the Footorb into the field with his hands
- If the ball leaves the goal-line of the field, a player has to kick the Footorb into the field with his foot
  - Before the Footorb is kicked into the field again, no player is allowed to be/enter the 16 branches zone
- If a player actively tries to hurt another player in any way, the player has to be sent off
- Only the actual players are allowed on the playing field

## Professional status
- Players are required to absolve at least 1 year in a youth club and 3 years at most
- Players are only allowed to play 15 years as a professional. Beginning from the year they joined a club outside the youth system
- If a player was a professional before, the years after still count towards the 15 years.
- All years before the start of the first official league don't count.

## Leagues
The leagues are governed by their national organization.
They are also responsible for enforcing the rules set by the UEFF.

### System
A national league must contain a maximum of 8 teams.
The winner is determined by:
- most points
  - goal difference
    - direct games against each other (wins)
      - direct games against each other (goals)
        - a game against each other

### Size
Every national organization may determine the size of their leagues under the allowed maximum.
Refer to [system](/ueff/rules/#system).

### Point Systems
Leagues are allowed to use the following point systems.

#### 3-Points-System
For a win: 3 Points
For a draw: 1 Point
For a defeat: 0 Points

#### 2-Points-System
For a win: 2 Points
For a draw: 1 Point
For a defeat: 0 Points

#### Narkath-System
For a win: 2 Points
For a win after shootout: 2 Points
For a defeat after shootout: 1 Point
For a defeat: 0 Points

## Goblet Tournaments
The goblets are governed by their national organization.
They are also responsible to enforce the rules set by the UEFF.

### System
A national goblet must contain a minimum of 8 teams.

There are 2 eligible systems.

#### Round-rudi
Teams are drawn against each other.
The drawing process must be public.
The winning team stays in goblet run, the loser is out.
This goes on until 2 teams are playing against each other.
The winner takes the goblet.

#### Groups + Round-rudi
Same as [Round-rudi](/ueff/rules/#system/round-rudi) but with a league system before the actual Round-rudi.

## Transfer of players
Teams are not allowed to trade players.
Teams are not allowed to buy a player with money or items of any kind.
Teams are not allowed to force a player to play for them or another team.
If a player starts the season by a club, they are only allowed to change the team in the determined transfer period.
