---
author: "UEFF"
title: "History of Footorb"
date: 2020-02-29T12:00:00+01:00
type: "showcase"
draft: false
---

Footorb has a long tradition in Eondra.

Footorb was mentioned for the first time in the 412 BLC book by the author 1BLA 2BLA "Culture of Jamta".
According to "Culture of Jamta", Footorb was played with an animal stomach that was filled with various materials.

After TBD saw this, the elf presented an orb to the playing children of Jamta, which he magically filled with air.
This inspired the father of three Footorb-playing children of his own to make these orbs in his smithy. He did not add the air magically, but with his bellows.
TBD is considered the forefather of today's Footorbs.

After the publication of the book, the game became more and more popular within Yllin and over the years has spread throughout Eondra.

The game was initially only popular with children, but later became increasingly popular among adults.

Smaller local tournaments were held regularly throughout Eondra.

After the fall of Larvik in the fourth great war, the five major nations decided that Eondra needed a fresh start.
They were fed up with going to war over recurring, minute problems.

Four years after Larvik's fall, the nations of Yllin and Jamta decided that the population would need a distraction and tried to increase the mood by promoting the creation of Footorb clubs.

The first official club was founded in 03.03.04 LC was named "1. Footorb Club Gislavik", in Gislavik, Jamta.

Several Footorb clubs were founded in both nations.
Those tournaments were held inter-regional and more and more residents came to watch.

Word got around in Eondra and more and more clubs outside Jamta and Yllin were founded.
Satisfaction in every segment of the population increased, crime decreased and work performance also increased across Eondra.

In 16 LC the leaders of the five major nations started negotiations to establish a joint Footorb league.

The UEFF (United Eondra Footorb Federation) was founded on 02.04.17 LC. With it, every nation founded an umbrella organization + their own nationwide leagues. The first leagues are planned to start in 20 LC.
