---
author: "UEFF"
title: "Friendly | Dim Garom Diggers - Kinbadur Rocks"
date: 2020-02-22T13:00:06+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [Dim Garom Diggers](/teams/dimgaromdiggers) - [Kinbadur Rocks](/teams/kinbadurrocks) 2:0 (0:0)

#### Lineup

| # | [Dim Garom Diggers](/teams/dimgaromdiggers) | # | [Kinbadur Rocks](/teams/kinbadurrocks) |
|:-:|:-:|:-:|:-:|
| 2 | [Flintstream](/characters/graadd_flintstream) | 2 | [Warmmantle](/characters/vyls_warmmantle) |
| 5 | [Twilightstone](/characters/darg_twilightstone) | 6 | [Khas](/characters/lurgrus_khas) |
| 8 | [Softsnow](/characters/rakrulim_softsnow) | 9 | [Greatchest](/characters/helerlug_greatchest) |
| 10 | [Deeparm](/characters/gragroc_deeparm) | 4 | [Bloodthane](/characters/wefnum_bloodthane) |
| 7 | [Leatherborn](/characters/acmurd_leatherborn) | 14 | [Flaskshield](/characters/ruudd_flaskshield) |
| 3 | [Bloodbrow](/characters/gurgeg_bloodbrow) | 18 | [Warback](/characters/zersuc_warback) |
| 18 | [Jadehelm](/characters/ak_jadehelm) | 19 | [Sahudd](/characters/strul_sahudd) |
| 20 | [Darkridge](/characters/skargrock_darkridge) | 20 | [Graybane](/characters/sandrul_graybane) |
| 14 | [Srorgwak](/characters/magnull_srorgwak) | 13 | [Gravelarm](/characters/sneaky_gravelarm) |
| 23 | [Stonestrike](/characters/braddumi_stonestrike) | 24 | [Coalspine](/characters/zabmiodd_coalspine) |
| 13 | [Assailant](/characters/uulgroor_assailant) | 21 | [Earthsong](/characters/dagumoor_earthsong) |
| C | [Ghuls](/characters/loznerlug_ghuls) | C | [Treasurejaw](/characters/halm_treasurejaw) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 63 | [Srorgwak](/characters/magnull_srorgwak) | [Dim Garom Diggers](/teams/dimgaromdiggers) |
| 84 | [Assailant](/characters/uulgroor_assailant) | [Dim Garom Diggers](/teams/dimgaromdiggers) |

#### Statistics

| [Dim Garom Diggers](/teams/dimgaromdiggers) | | [Kinbadur Rocks](/teams/kinbadurrocks) |
|:-:|:-:|:-:|
| 2 | Goals | 0 |
| 44% | Possession | 56% |
| 17 | Goal Attempts | 12 |
