---
author: "UEFF"
title: "Friendly | Gearingfort Inc. - Cogdale Scraps"
date: 2020-03-01T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [Gearingfort Inc.](/teams/gearingfortinc) - [Cogdale Scraps](/teams/cogdalescraps) 1:1 (0:1)

#### Lineup

| # | [Gearingfort Inc.](/teams/gearingfortinc) | # | [Cogdale Scraps](/teams/cogdalescraps) |
|:-:|:-:|:-:|:-:|
| 1 | [Steamtorque](/characters/itirn_steamtorque) | 1 | [Cogsignal](/characters/gniles_cogsignal) |
| 6 | [Bizzheart](/characters/itlazz_bizzheart) | 5 | [Sprytwist](/characters/dollik_sprytwist) |
| 8 | [Luckcable](/characters/icisirn_luckcable) | 7 | [Fuzzyspinner](/characters/tenlizz_fuzzyspinner) |
| 10 | [Portersingal](/characters/gnemkeefirn_portersignal) | 11 | [Wheelclick](/characters/pimack_wheelclick) |
| 9 | [Shiftsteel](/characters/peetki_shiftsteel) | 3 | [Tinkhouse](/characters/cemki_tinkhouse) |
| 11 | [Tinksteel](/characters/gaklotkirn_tinksteel) | 18 | [Castclock](/characters/irkok_castclock) |
| 4 | [Wobbledock](/characters/pinki_wobbledock) | 17 | [Acercollar](/characters/piclis_acercollar) |
| 15 | [Buzzinkettle](/characters/beclimirn_buzzinkettle) | 14 | [Togglescheme](/characters/inciblick_togglescheme) |
| 18 | [Draxlebrick](/characters/fukugus_draxlebrick) | 23 | [Strikebrake](/characters/klilkis_strikebrake) |
| 13 | [Railstrip](/characters/hithkozz_railstrip) | 25 | [Draxletorque](/characters/cunkack_draxletorque) |
| 25 | [Trickyblast](/characters/thetlick_trickyblast) | 12 | [Buzzinchart](/characters/finethizz_buzzinchart) |
| C | [Bizzpitch](/characters/ilirn_bizzpitch) | C | [Anglenozzle](/characters/milluflis_anglenozzle) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 16 | [Draxletorque](/characters/cunkack_draxletorque) | [Cogdale Scraps](/teams/cogdalescraps) |
| 57 | [Trickyblast](/characters/thetlick_trickyblast) | [Gearingfort Inc.](/teams/gearingfortinc) |

#### Statistics

| [Gearingfort Inc.](/teams/gearingfortinc) | | [Cogdale Scraps](/teams/cogdalescraps) |
|:-:|:-:|:-:|
| 1 | Goals | 1 |
| 47% | Possession | 53% |
| 19 | Goal Attempts | 18 |
