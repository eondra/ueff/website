---
author: "UEFF"
title: "Friendly | FT Kruzaz - Qvadgad Wolves"
date: 2020-02-22T13:00:06+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [FT Kruzaz](/teams/ftkruzaz) - [Qvadgad Wolves](/teams/qvadgadwolves) 1:0 (1:0)

#### Lineup

| # | [FT Kruzaz](/teams/ftkruzaz) | # | [Qvadgad Wolves](/teams/qvadgadwolves) |
|:-:|:-:|:-:|:-:|
| 2 | [Lowtooth](/characters/trivrold_lowtooth) | 1 | [Cheth](/characters/sotam_cheth) |
| 5 | [Zikuk](/characters/kranefe_zikuk) | 5 | [Hrolleifrson](/characters/gromvaror_hrolleifrson) |
| 8 | [Donnin](/characters/drash_donnin) | 9 | [Thufrahn](/characters/gagzukk_thufrahn) |
| 4 | [Zaam](/characters/borm_zaam) | 8 | [Thusgash](/characters/roresk_thusgash) |
| 15 | [Gearbang](/characters/eeckak_gearbang) | 4 | [Brehjosh](/characters/thrikar_brehjosh) |
| 19 | [Chikkud](/characters/kuk_chikkud) | 14 | [Velkihn](/characters/trork_velkihn) |
| 17 | [Koboldblade](/characters/tioc_koboldblade) | 17 | [Kirkon](/characters/murn_kirkon) |
| 20 | [Brirnem](/characters/kratomm_brirnem) | 16 | [Dhus](/characters/duzathe_dhus) |
| 13 | [Deaddrums](/characters/adzokk_deaddrums) | 19 | [Sharpmight](/characters/moch_sharpmight) |
| 25 | [Stoutwatch](/characters/uklilvoz_stoutwatch) | 13 | [Ravenscream](/characters/torrfyg_ravenscream) |
| 24 | [Chisreth](/characters/vrokk_chisreth) | 24 | [Cravenchest](/characters/grik_cravenchest) |
| C | [Copperbit](/characters/finki_copperbit) | C | [Lightshield](/characters/lokihor_lightshield) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 5 | [Stoutwatch](/characters/uklilvoz_stoutwatch) | [FT Kruzaz](/teams/ftkruzaz) |

#### Statistics

| [FT Kruzaz](/teams/ftkruzaz) | | [Qvadgad Wolves](/teams/qvadgadwolves) |
|:-:|:-:|:-:|
| 1 | Goals | 0 |
| 53% | Possession | 47% |
| 13 | Goal Attempts | 13 |
