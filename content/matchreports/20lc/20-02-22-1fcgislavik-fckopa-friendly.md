---
author: "UEFF"
title: "Friendly | 1. FC Gislavik - FC Kopa"
date: 2020-02-22T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [1. FC Gislavik](/teams/1fcgislavik) - [FC Kopa](/teams/fckopa) 1:2 (1:2)

#### Lineup

| # | [1. FC Gislavik](/teams/1fcgislavik) | # | [FC Kopa](/teams/fckopa) |
|:-:|:-:|:-:|:-:|
| 2 | [Gunnulvson](/characters/maearke_gunnulvson) | 1 | [Mjisssen](/characters/hridarik_mjisssen) |
| 6 | [Warlock](/characters/koll_warlock) | 5 | [Kylanson](/characters/kvistr_kylanson) |
| 11 | [Valgardson](/characters/vebiorn_valgardson) | 9 | [Kolson](/characters/akaoknolf_kolson) |
| 9 | [Brownhand](/characters/wedrud_brownhand) | 10 | [Short-maiden](/characters/vermundr_short-maiden) |
| 3 | [Fog-lute](/characters/lundthor_fog) | 4 | [Sarervonson](/characters/metinir_sarervonsen) |
| 15 | [Frosthide](/characters/jeran_frosthide) | 14 | [Bloodmouth](/characters/vignis_bloodmouth) |
| 20 | [Haraeldsen](/characters/rhofur_haraeldsen) | 16 | [Haukrson](/characters/loatnjolf_haukrson) |
| 17 | [Firerider](/characters/tulrod_firerider) | 17 | [Fjorinssen](/characters/hastein_fjorinssen) |
| 13 | [Riversword](/characters/stenrmoor_riversword) | 12 | [Mojarkesson](/characters/soxolfr_mojarkesson) |
| 23 | [Ghik](/characters/borekk_ghik) | 25 | [Erensson](/characters/gutng_erensson) |
| 25 | [Cabbage-skinner](/characters/freki_cabbage-skinner) | 22 | [Heavyshield](/characters/hakon_heavyshield) |
| C | [Fridmundrson](/characters/thormodr_fridmundrson) | C | [Hard-nail](/characters/halufi_hard-nail) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 10 | [Fjorinssen](/characters/hastein_fjorinssen) | [FC Kopa](/teams/fckopa) |
| 34 | [Heavyshield](/characters/hakon_heavyshield) | [FC Kopa](/teams/fckopa) |
| 37 | [Cabbage-skinner](/characters/freki_cabbage-skinner) | [1. FC Gislavik](/teams/1fcgislavik) |

#### Statistics

| [1. FC Gislavik](/teams/1fcgislavik) | | [FC Kopa](/teams/fckopa) |
|:-:|:-:|:-:|
| 1 | Goals | 2 |
| 49% | Possession | 51% |
| 13 | Goal Attempts | 19 |
