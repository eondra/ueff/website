---
author: "UEFF"
title: "Friendly | MA Theveluma - Nfelin United"
date: 2020-02-22T13:00:06+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [MA Theveluma](/teams/matheveluma) - [Nfelin United](/teams/nfelinunited) 0:0 (0:0)

#### Lineup

| # | [MA Theveluma](/teams/matheveluma) | # | [Nfelin United](/teams/nfelinunited) |
|:-:|:-:|:-:|:-:|
| 1 | [Foreststriker](/characters/thias_foreststriker) | 1 | [Grus](/characters/krustham_grus) |
| 5 | [Kornuhn](/characters/tugish_kornuhn) | 6 | [Blackarrow](/characters/thusess_blackarrow) |
| 7 | [Fensys](/characters/kraenithodvioss_fensys) | 10 | [Swiftgrove](/characters/balfran_swiftgrove) |
| 11 | [Saryarus](/characters/ayduin_saryarus) | 7 | [Ravenwalker](/characters/iri_ravenwalker) |
| 4 | [Erzeiros](/characters/saelethil_erzeiros) | 3 | [Forestleaf](/characters/thiler_forestleaf) |
| 14 | [Zonvutlar](/characters/sylvar_zonvutlar) | 20 | [Yllapeiros](/characters/nuvian_yllapeiros) |
| 17 | [Cailynn](/characters/zol_cailynn) | 16 | [Flukecog](/characters/imlik_flukecog) |
| 19 | [Urigolor](/characters/halflar_urigolor) | 18 | [Kubhog](/characters/doylan_kubhug) |
| 12 | [Carnelis](/characters/odan_carnelis) | 24 | [Ravajor](/characters/lydent_ravajor) |
| 23 | [Thebella](/characters/jithae_thebella) | 25 | [Adleth](/characters/mavolaar_adleth) |
| 22 | [Nas](/characters/ven_nas) | 12 | [Steelwinds](/characters/braellyn_steelwinds) |
| C | [Glynnelis](/characters/cethidan_glynnelis) | C | [Brightbringer](/characters/dalongrerlig_brightbringer) |

#### Statistics

| [MA Theveluma](/teams/matheveluma) | | [Nfelin United](/teams/nfelinunited) |
|:-:|:-:|:-:|
| 0 | Goals | 0 |
| 51% | Possession | 49% |
| 13 | Goal Attempts | 8 |
