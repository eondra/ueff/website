---
author: "UEFF"
title: "Friendly | FC Esjuberg - SC Aurioa"
date: 2020-03-01T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [FC Esjuberg](/teams/fcesjuberg) - [SC Aurioa](/teams/scaurioa) 1:0 (1:0)

#### Lineup

| # | [FC Esjuberg](/teams/fcesjuberg) | # | [SC Aurioa](/teams/scaurioa) |
|:-:|:-:|:-:|:-:|
| 1 | [Jorgenrsson](/characters/haehjolf_jorgenrsson) | 1 | [Bristlebraids](/characters/trorgrorg_bristlebraids) |
| 6 | [Dawnmaul](/characters/bronhorgh_dawnmaul) | 5 | [Flickerspell](/characters/tindick_flickerspell) |
| 7 | [Ermon](/characters/kastuth_ermon) | 8 | [Gusirson](/characters/holti_gusirson) |
| 4 | [Fiery-Hair](/characters/saesim_fiery-hair) | 7 | [Torberson](/characters/lodin_torberson) |
| 14 | [Lightningslayer](/characters/baslrir_lightningslayer) | 3 | [Short-Hammers](/characters/hrahar_short-hammers) |
| 20 | [Knottrson](/characters/rustlam_knottrson) | 18 | [Ulrerssen](/characters/hisas_ulrerssen) |
| 16 | [Milk-Hammers](/characters/kodran_milk-hammers) | 17 | [Hard-Nail](/characters/assuke_hard-nail) |
| 19 | [Sprycollar](/characters/titku_sprycollar) | 14 | [Ember-Loom](/characters/hegbjorn_ember-loom) |
| 13 | [Strongbringer](/characters/bjntus_strongbringer) | 22 | [Herornsen](/characters/hrorvur_herornsen) |
| 22 | [Ahlornson](/characters/eldjmoor_ahlornson) | 21 | [Frostbreath](/characters/avuit_frostbreath) |
| 25 | [Goreslayer](/characters/thormodr_goreslayer) | 12 | [Rockarm](/characters/fuknan_rockarm) |
| C | [Two-Lute](/characters/sniolf_two-lute) | C | [Hrolleifrson](/characters/einirod_hrolleifrson) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 90 | [Sprycollar](/characters/titku_sprycollar) | [FC Esjuberg](/teams/fcesjuberg) |

#### Statistics

| [FC Esjuberg](/teams/fcesjuberg) | | [SC Aurioa](/teams/scaurioa) |
|:-:|:-:|:-:|
| 1 | Goals | 0 |
| 52% | Possession | 48% |
| 13 | Goal Attempts | 15 |
