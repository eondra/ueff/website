---
author: "UEFF"
title: "Friendly | BA Kyonore - Iri Serin Alchemists"
date: 2020-03-01T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [BA Kyonore](/teams/bakyonore) - [Iri Serin Alchemists](/teams/iriserinalchemists) 1:0 (1:0)

#### Lineup

| # | [BA Kyonore](/teams/bakyonore) | # | [Iri Serin Alchemists](/teams/iriserinalchemists) |
|:-:|:-:|:-:|:-:|
| 1 | [Darkforest](/characters/krur_darkforest) | 1 | [Wem](/characters/sel_wem) |
| 5 | [Bearscribe](/characters/helnim_bearscribe) | 6 | [Shadeshot](/characters/leleath_shadeshot) |
| 7 | [Glynven](/characters/iliphar_glynven) | 11 | [Deadrage](/characters/nirn_deadrage) |
| 9 | [Zhirom](/characters/nadraen_zhirom) | 8 | [Gastamras](/characters/nelsil_gastamras) |
| 4 | [Aethana](/characters/aladaen_aethana) | 10 | [Teenygauge](/characters/dutlut_teenygauge) |
| 16 | [Nem](/characters/lathlaeril_nem) | 7 | [Starblower](/characters/orrian_starblower) |
| 17 | [Saeyudim](/characters/thulsom_saeyudim) | 4 | [Shadelight](/characters/krar_shadelight) |
| 15 | [Forestbreath](/characters/ostess_forestbreath) | 15 | [Fjorinssen](/characters/zarengyl_solsteh) |
| 24 | [Eilwarin](/characters/aubron_eilwarin) | 17 | [Forestflower](/characters/rylaeth_forestflower) |
| 23 | [Yelphyra](/characters/garril_yelphyra) | 13 | [Gelai](/characters/madaan_gelai) |
| 13 | [Jilvinral](/characters/terendrannul_jilvinral) | 23 | [Farieth](/characters/elren_farieth) |
| C | [Rapidwhisper](/characters/fanturas_rapidwhisper) | C | [Diolliadu](/characters/bardiant_diolliadu) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 10 (OG) | [Starblower](/characters/orrian_starblower) | [Iri Serin Alchemists](/teams/iriserinalchemists) |

#### Statistics

| [BA Kyonore](/teams/bakyonore) | | [Iri Serin Alchemists](/teams/iriserinalchemists) |
|:-:|:-:|:-:|
| 1 | Goals | 0 |
| 54% | Possession | 46% |
| 11 | Goal Attempts | 17 |
