---
author: "UEFF"
title: "Friendly | 1. SC Tinkerbury - BB Chiselhaven"
date: 2020-02-22T13:00:06+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [1. SC Tinkerbury](/teams/1sctinkerbury) - [BB Chiselhaven](/teams/bbchiselhaven) 2:2 (0:1)

#### Lineup

| # | [1. SC Tinkebury](/teams/1sctinkerbury) | # | [BB Chiselhaven](/teams/bbchiselhaven) |
|:-:|:-:|:-:|:-:|
| 2 | [Wheelcrown](/characters/urkink_wheelcrown) | 2 | [Bizzwhistle](/characters/efinuck_bizzwhistle) |
| 6 | [Silverspanner](/characters/futhkic_silverspanner) | 6 | [Lockfuse](/characters/kirlis_lockfuse) |
| 10 | [Forehorn](/characters/crumzit_forehorn) | 10 | [Fizzlecable](/characters/gnirkabrik_fizzlecable) |
| 11 | [Quickpickle](/characters/kletimoc_quickpickle) | 7 | [Grimeyhouse](/characters/hinarn_grimeyhouse) |
| 4 | [Anglebit](/characters/imunkil_anglebit) | 3 | [Stitchbus](/characters/lisencec_stitchbus) |
| 15 | [Pitchbang](/characters/morkizz_pitchbang) | 14 | [Berrysprocket](/characters/cunkack_berrysprocket) |
| 17 | [Fixcub](/characters/deenbo_fixcub) | 18 | [Gripclick](/characters/lathkigeesh_gripclick) |
| 20 | [Singlebus](/characters/muvorkek_singlebus) | 17 | [Berrysprocket](/characters/ariklik_berrysprocket) |
| 12 | [Ghikal](/characters/bosh_ghikal) | 12 | [Pipepatch](/characters/eeklareck_pipepatch) |
| 24 | [Anglechin](/characters/itkon_anglechin) | 24 | [Acerkettle](/characters/ditec_acerkettle) |
| 22 | [Swiftcrown](/characters/linduzz_swiftcrown) | 25 | [Rustcable](/characters/kruthkik_rustcable) |
| C | [Switchblock](/characters/tutlethizz_switchblock) | C | [Battlelaugh](/characters/klefilkonk_battlelaugh) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 18 | [Acerkettle](/characters/ditec_acerkettle) | [BB Chiselhaven](/teams/bbchiselhaven) |
| 49 | [Swiftcrown](/characters/linduzz_swiftcrown) | [1. SC Tinkebury](/teams/1sctinkerbury) |
| 83 | [Swiftcrown](/characters/linduzz_swiftcrown) | [1. SC Tinkebury](/teams/1sctinkerbury) |
| 89 | [Rustcable](/characters/kruthkik_rustcable) | [BB Chiselhaven](/teams/bbchiselhaven) |

#### Statistics

| [1. SC Tinkebury](/teams/1sctinkerbury) | | [BB Chiselhaven](/teams/bbchiselhaven) |
|:-:|:-:|:-:|
| 2 | Goals | 2 |
| 53% | Possession | 47% |
| 12 | Goal Attempts | 17 |
