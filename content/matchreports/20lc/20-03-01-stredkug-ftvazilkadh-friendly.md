---
author: "UEFF"
title: "Friendly | ST Redkug - FT Vaz Ilkadh"
date: 2020-03-01T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [ST Redkug](/teams/stredkug) - [FT Vaz Ilkadh](/teams/ftvazilkadh) 0:2 (0:1)

#### Lineup

| # | [ST Redkug](/teams/stredkug) | # | [FT Vaz Ilkadh](/teams/ftvazilkadh) |
|:-:|:-:|:-:|:-:|
| 2 | [Angersteel](/characters/marntuch_angersteel) | 2 | [Silentcrusher](/characters/dald_silentcrusher) |
| 5 | [Saurdrum](/characters/rimimm_saurdrum) | 5 | [Crik](/characters/krume_crik) |
| 8 | [Burningkill](/characters/usram_burningkill) | 11 | [Blindspite](/characters/grahd_blindspite) |
| 7 | [Warptwist](/characters/citald_warptwist) | 4 | [Brightdrums](/characters/kzangosk_brightdrums) |
| 4 | [Thoveth](/characters/sudorm_thoveth) | 15 | [Uazot](/characters/muld_uazot) |
| 15 | [Kremgam](/characters/vrelok_kremgam | 19 | [Laughningnight](/characters/gremrahn_laughingnight) |
| 17 | [Doomwatch](/characters/muzcag_doomwatch) | 18 | [Hollowstriker](/characters/grarn_hollowstriker) |
| 16 | [Olafurkin](/characters/leifae_olafurkin) | 20 | [Saurthunder](/characters/bonvoltal_saurthunder) |
| 12 | [Boradstriker](/characters/kzemgig_broadstriker) | 12 | [Temperscheme](/characters/orkink_temperscheme) |
| 22 | [Donnin](/characters/adzokk_donnin) | 22 | [Roth](/characters/draldam_roth) |
| 25 | [Shiftcup](/characters/abrinlin_shiftcup) | 23 | [Bad](/characters/kold_bad) |
| C | [Laughinghand](/characters/brarri_laughinghand) | C | [Ket](/characters/cralzech_ket) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|
| 19 | [Roth](/characters/draldam_roth) | [FT Vaz Ilkadh](/teams/ftvazilkadh) |
| 84 | [Roth](/characters/draldam_roth) | [FT Vaz Ilkadh](/teams/ftvazilkadh) |

#### Statistics

| [ST Redkug](/teams/stredkug) | | [FT Vaz Ilkadh](/teams/ftvazilkadh) |
|:-:|:-:|:-:|
| 0 | Goals | 2 |
| 44% | Possession | 56% |
| 3 | Goal Attempts | 16 |
