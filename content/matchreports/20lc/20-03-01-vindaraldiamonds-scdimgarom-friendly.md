---
author: "UEFF"
title: "Friendly | Vin Daral Diamonds - SC Dim Garom"
date: 2020-03-01T13:00:00+01:00
draft: false
tags: ["friendly","matchreport"]
type: "showcase"
---
### [Vin Daral Diamonds](/teams/vindaraldiamonds) - [SC Dim Garom](/teams/scdimgarom) 0:0 (0:0)

#### Lineup

| # | [Vin Daral Diamonds](/teams/vindaraldiamonds) | # | [SC Dim Garom](/teams/scdimgarom) |
|:-:|:-:|:-:|:-:|
| 1 | [Coalborn](/characters/hamoic_coalborn) | 1 | [Beastcloak](/characters/sighoud_beastcloak) |
| 6 | [Browngrip](/characters/kabek_browngrip) | 6 | [Kyrg](/characters/grusgraic_kyrg) |
| 9 | [Rilrin](/characters/fofaek_rilrin) | 8 | [Jondus](/characters/khutheack_jondus) |
| 10 | [Tresat](/characters/whughean_tresat) | 7 | [Seck](/characters/throthead_seck) |
| 11 | [Coalheart](/characters/khouvraer_coalheart) | 11 | [Grodd](/characters/hock_grodd) |
| 8 | [Flamebreath](/characters/omnoud_flamebreath) | 9 | [Yoglet](/characters/bryknyn_yoglet) |
| 4 | [Solidhide](/characters/gocmirg_soldhide) | 3 | [Tatlan](/characters/bomkag_tatlan) |
| 19 | [Mithrilbraids](/characters/durikdromri_mithrilbraids) | 16 | [Forechewer](/characters/drohn_forechewer) |
| 16 | [Isnoth](/characters/mogth_isnoth) | 20 | [Deepcrest](/characters/ulgursk_deepcrest) |
| 20 | [Flatbraids](/characters/rols_flatbraids) | 19 | [Thundergut](/characters/jal_thundergut) |
| 23 | [Fairward](/characters/dur_fairward) | 25 | [Amberfall](/characters/throthead_amberfall) |
| C | [Ningid](/characters/srod_ningrid) | C | [Kornod](/characters/cazrem_kornod) |

#### Goals

| Minute | Name | Team |
|:-:|:-:|:-:|

#### Statistics

| [Vin Daral Diamonds](/teams/vindaraldiamonds) | | [SC Dim Garom](/teams/scdimgarom) |
|:-:|:-:|:-:|
| 0 | Goals | 0 |
| 52% | Possession | 48% |
| 20 | Goal Attempts | 14 |
